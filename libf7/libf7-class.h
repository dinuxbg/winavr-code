/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#ifndef LIBF7_CLASS_H
#define LIBF7_CLASS_H

#define IN_LIBF7_CLASS_H

#undef abs // Annoyance from avr-libc's stdlib.h.

#define F7_INLINE_WRAPPERS

#ifdef F7_INLINE_WRAPPERS
    #define F7_IW           F7_INLINE
    #define F7_static_IW    static F7_INLINE
#else
    #define F7_IW           // empty
    #define F7_static_IW    // empty
#endif

class f7_double;

class f7
{
    f7_t mm;

    friend f7_double;
public:
    class value;

    f7()
    {
        mm.flags = F7_FLAG_nan;
    }

    f7 (const f7& yy)
    {
        f7_copy (& mm, & yy.mm);
    }

    F7_INLINE f7 (const f7_double&);
    F7_INLINE f7 (const f7_double*);

    constexpr f7 (const f7_t &xx) : mm(xx) {}

    const f7& operator = (const f7 &yy)
    {
        f7_copy (& mm, & yy.mm);
        return *this;
    }

    const f7_t* praw () const
    {
        return & mm;
    }

    f7_t* praw ()
    {
        return & mm;
    }

    F7_INLINE
    f7 (int16_t s16)     { __asm (";; XXXsS16"); f7_set_s16 (& mm, s16); }
    f7 (int32_t s32)     { f7_set_s32 (& mm, s32); }
    f7 (int64_t s64)     { f7_set_s64 (& mm, s64); }

    F7_INLINE
    f7 (uint16_t u16)     { __asm (";; XXXu16"); f7_set_u16 (& mm, u16); }
    f7 (uint32_t u32)    { f7_set_u32 (& mm, u32); }
    f7 (uint64_t u64)    { f7_set_u64 (& mm, u64); }

    f7 (float sf)        { f7_set_float  (& mm, sf); }
    f7 (double sf)       { f7_set_float  (& mm, (float) sf); }

    operator int16_t()  { return f7_get_s16 (& mm); }
    operator int32_t()  { return f7_get_s32 (& mm); }
    operator int64_t()  { return f7_get_s64 (& mm); }

    operator float() { return f7_get_float (& mm); }

    F7_IW f7 dump () const;

    F7_IW f7 operator - () const;

    F7_IW f7 operator + (const f7 &yy) const;
    F7_IW f7 operator - (const f7 &yy) const;
    F7_IW f7 operator * (const f7 &yy) const;
    F7_IW f7 operator / (const f7 &yy) const;

    F7_IW const f7& operator += (const f7 &yy);
    F7_IW const f7& operator -= (const f7 &yy);
    F7_IW const f7& operator *= (const f7 &yy);
    F7_IW const f7& operator /= (const f7 &yy);

    F7_IW f7 operator + (int s16) const;
    F7_IW f7 operator - (int s16) const;
    F7_IW f7 operator * (int s16) const;
    F7_IW f7 operator / (int s16) const;

    F7_IW friend f7 operator + (int, const f7&);
    F7_IW friend f7 operator - (int, const f7&);
    F7_IW friend f7 operator * (int, const f7&);
    F7_IW friend f7 operator / (int, const f7&);

    F7_IW bool operator >  (const f7&) const;
    F7_IW bool operator <  (const f7&) const;
    F7_IW bool operator >= (const f7&) const;
    F7_IW bool operator <= (const f7&) const;
    F7_IW bool operator == (const f7&) const;
    F7_IW bool operator != (const f7&) const;

    F7_IW bool operator >  (int) const;
    F7_IW bool operator <  (int) const;
    F7_IW bool operator >= (int) const;
    F7_IW bool operator <= (int) const;
    F7_IW bool operator == (int) const;
    F7_IW bool operator != (int) const;

    F7_IW friend bool operator >  (int, const f7&);
    F7_IW friend bool operator <  (int, const f7&);
    F7_IW friend bool operator >= (int, const f7&);
    F7_IW friend bool operator <= (int, const f7&);
    F7_IW friend bool operator == (int, const f7&);
    F7_IW friend bool operator != (int, const f7&);

    F7_IW const f7& operator <<= (int);
    F7_IW const f7& operator >>= (int);

    F7_IW f7 operator << (int) const;
    F7_IW f7 operator >> (int) const;

    F7_IW f7 abs () const;
    F7_IW f7 exp () const;
    F7_IW f7 log () const;
    F7_IW f7 log2 () const;
    F7_IW f7 log10 () const;
    F7_IW f7 sqrt () const;
    F7_IW f7 ceil () const;
    F7_IW f7 floor () const;
    F7_IW f7 round () const;
    F7_IW f7 trunc () const;
    F7_IW f7 cotan () const;
    F7_IW f7 sin () const;
    F7_IW f7 cos () const;
    F7_IW f7 tan () const;
    F7_IW f7 asin () const;
    F7_IW f7 acos () const;
    F7_IW f7 atan () const;
    F7_IW f7 min (const f7&) const;
    F7_IW f7 max (const f7&) const;
    F7_IW f7 pow (const f7&) const;
    F7_IW f7 pow10 () const;
    F7_IW f7 exp10 () const;
    F7_IW f7 atan2 (const f7&) const;
    F7_IW f7 powi (int) const;
};

F7_static_IW const f7& dump (const f7&);

F7_static_IW f7 abs (const f7&);
F7_static_IW f7 exp (const f7&);
F7_static_IW f7 log (const f7&);
F7_static_IW f7 log10 (const f7&);
F7_static_IW f7 log2 (const f7&);
F7_static_IW f7 sqrt (const f7&);
F7_static_IW f7 ceil (const f7&);
F7_static_IW f7 floor (const f7&);
F7_static_IW f7 round (const f7&);
F7_static_IW f7 trunc (const f7&);
F7_static_IW f7 cotan (const f7&);
F7_static_IW f7 sin (const f7&);
F7_static_IW f7 cos (const f7&);
F7_static_IW f7 tan (const f7&);
F7_static_IW f7 asin (const f7&);
F7_static_IW f7 acos (const f7&);
F7_static_IW f7 atan (const f7&);
F7_static_IW f7 min (const f7&, const f7&);
F7_static_IW f7 max (const f7&, const f7&);
F7_static_IW f7 pow (const f7&, const f7&);
F7_static_IW f7 pow10 (const f7&);
F7_static_IW f7 exp10 (const f7&);
F7_static_IW f7 atan2 (const f7&, const f7&);
F7_static_IW f7 min (const f7&, const f7&);
F7_static_IW f7 max (const f7&, const f7&);
F7_static_IW f7 powi (const f7&, int);

#if defined (F7_INLINE_WRAPPERS) && !defined (F7MOD_wrappers_)
    #include "libf7.cpp"
#endif


struct f7::value
{
    #define F7_CONST_DEF(NAME, FLAGS, M6, M5, M4, M3, M2, M1, M0, EXPO) \
        static constexpr f7 NAME =                                      \
        (f7_t) { .flags = FLAGS, .mant = { M0,M1,M2,M3,M4,M5,M6 }, .expo = EXPO };
    #define ONLY_CONST_WITH_ID
        #include "libf7-const.def"
    #undef ONLY_CONST_WITH_ID
    #undef F7_CONST_DEF
};

extern const f7& dump   (const char*, const f7&);
extern const f7& dump_P (const char*, const f7&);

extern f7 absdiff (const f7&, const f7&);


class f7_double
{
    uint64_t dd;
public:
    f7_double() { f7 xx; set (xx); }
    f7_double (int16_t s16)  { f7 xx; f7_set_s16 (xx.praw(), s16); set (xx); }
    f7_double (int32_t s32)  { f7 xx; f7_set_s32 (xx.praw(), s32); set (xx); }
    f7_double (int64_t s64)  { f7 xx; f7_set_s64 (xx.praw(), s64); set (xx); }

    f7_double (uint16_t u16) { f7 xx; f7_set_u16 (xx.praw(), u16); set (xx); }
    f7_double (uint32_t u32) { f7 xx; f7_set_u32 (xx.praw(), u32); set (xx); }
    f7_double (uint64_t u64) { f7 xx; f7_set_u64 (xx.praw(), u64); set (xx); }

    f7_double (float sf)  { f7 xx; f7_set_float (xx.praw(), sf); set (xx); }
    f7_double (double sf) { f7 xx; f7_set_float (xx.praw(), sf); set (xx); }

    void set (const f7& xx)
    {
        *this = (f7_double) xx;
    }

    f7_double (const f7 &xx)
    {
        dd = f7_get_double (& xx.mm);
    }

    operator f7() const
    {
        return (f7) (*this);
    }

    f7 to_f7 () const
    {
        return (f7) (*this);
    }

    uint64_t raw() const
    {
        return dd;
    }

    const uint64_t* praw() const
    {
        return & dd;
    }

    uint64_t* praw()
    {
        return & dd;
    }

    f7_double operator + (const f7_double&) const;
    f7_double operator - (const f7_double&) const;
    f7_double operator * (const f7_double&) const;
    f7_double operator / (const f7_double&) const;

    bool operator >  (const f7_double&) const;
    bool operator >= (const f7_double&) const;
    bool operator <  (const f7_double&) const;
    bool operator <= (const f7_double&) const;
    bool operator == (const f7_double&) const;
    bool operator != (const f7_double&) const;
};



f7::f7 (const f7_double &yy)
{
    f7_set_double (& mm, yy.raw());
}

f7::f7 (const f7_double *yy)
{
    f7_set_pdouble (& mm, yy->praw());
}

#undef IN_LIBF7_CLASS_H
#endif // LIBF7_CLASS_H
