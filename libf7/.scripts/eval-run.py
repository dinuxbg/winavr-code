from __future__ import print_function
from decimal import *

import sys, re
from decimal import Decimal, ROUND_HALF_UP
from calc import *
from mymathutils import * # ld, cdef

fin = sys.stdin

def dmp (str, x):
	print ("%s = %s\n%s = %s" % (str, x, str, cdef(x)))

if 0 == 1:
	bump_precision(400)
	d = Decimal (-142352684017816320) / 2 ** 72
	print (d)
	d = Decimal (0xf81bc845c81bf8) / 2**56
	print (cdef (d, n_bytes=9))
	print (cdef (d*d, n_bytes=9))
	print ("")
	d = Decimal (255) / 256
	for i in range(15):
		d *= d
		print (cdef (d, n_bytes=9))
	exit (0)
	pi = get_pi(200)
	bump_precision(200)
	
	x = dec_from_f7 ("F7_CONST_DEF (big_acos_f7t, 0, 0xfe,0x70,0x6a,0x9b,0x9f,0xf2,0x29, -1)")
	dmp ("x", x)
	dmp ("acos", get_acos (x, 80))
	dmp ("2-2x", 2-2*x)
	dmp ("sqrt(2-2x)", (2-2*x).sqrt())
	
	exit(44)
	x = Decimal(355)/2
	y = Decimal(113)/2 * pi
	pi_low = pi - Decimal (0xc90fdaa22168c2) / 2 ** 54
	print ("--- %s, %s" % (y, cdef(y)))
	print ("--- %s, %s" % (x-y, cdef(x-y)))
	print ("low %s, %s" % (pi_low, cdef(pi_low)))
	print ("low %s, %s" % (pi_low*Decimal(113)/2, cdef(pi_low*Decimal(113)/2)))

	s_pi = "F7_CONST_DEF (pi,	0,	0xc9,0x0f,0xda,0xa2,0x21,0x68,0xc2,	1)"
	d_pi = dec_from_f7 (s_pi)
	print ("pi            = %s" % cdef(d_pi))
	d_pi *= Decimal(113)/2
	print ("pi *113/2     = %s" % cdef(d_pi))
	d_pi = Decimal(355)/2 - d_pi
	print ("(355-133pi)/2 = %s" % cdef(d_pi))
	exit(66)

class Value (object):
	""" A real number as encoded like
			F<digs>(<sign>,<mant>,<expo>)
		with
			digs: Number of bits in the mantissa (including amn implicit '1'.
			sign: Number if <  0 if sign == 1, 
				  Number is >= 0 if sign == 0.
			mant: Natural number, at most 64 bits, encoded as "0x..." style hex.
			expo: Integer
		Represented value is Number = (sign == 1 ? -1 : 1) * mant * 2 ** expo.
	"""
	pat_val = re.compile (r"(0|1),(0x.+),(-?\d+)")
	def __init__ (self, n_bits, s_value):
		self.n_bits = n_bits
		self.s_value = s_value
		self.isNaN  = s_value == "NaN"
		self.ispInf = s_value == "pInf"
		self.ismInf = s_value == "mInf"
		self.isInf = self.ismInf or self.ispInf
		self.isNumber = not (self.isInf or self.isNaN)
		self.val = None
		self.mant = None
		self.sign = None
		self.expo = None
		self.pow2 = None
		if self.isNumber:
			m = self.pat_val.match (s_value)
			if not m:
				raise ValueError ("unrecognized value representation: ", s_value)
			(s_sign,s_mant,s_expo) = (m.group(1),m.group(2),m.group(3))
			self.mant = mant =long(s_mant,base=16)
			self.expo = expo = int(s_expo)
			self.sign = sign = -1 if s_sign == "1" else 1
			self.val = sign * Decimal (mant)
			if expo >= 0:
				self.val *= Decimal(2) ** expo
			else:
				self.val /=  Decimal(2) ** -expo
			if self.val != 0:
				# .val = m * 2 ** pow2  with m in [1,2).
				self.pow2 = mant.bit_length()-1 + self.expo
			#print ("val64 = %s = %s, %x, %s, %s" % (s_value, s_mant, mant, self.val,
			#		self.val * Decimal(2)**-expo))

	def rounded (self, n_bits):
		if not self.isNumber:
			if self.isNaN:  return Decimal ("NaN")
			if self.ispInf: return Decimal ("+Inf")
			if self.ismInf: return Decimal ("-Inf")
			raise ValueError ("todo")
		if self.pow2 is None:
			assert self.val == 0
			return Decimal(0)

		val = self.val * Decimal(2) ** (n_bits-1 - self.pow2)
		val = val.to_integral_value(ROUND_HALF_UP)
		val /= Decimal(2) ** (n_bits-1 - self.pow2)
		return val

	def __repr__ (self):
		if self.val is None:
			assert not self.isNumber
		if self.isNaN:  return "NaN"
		if self.ispInf: return "+Inf"
		if self.ismInf: return "-Inf"
		return "%s" % self.val

mant_to_expo_bits = { 24:8, 53:11, 56:15 }

def is_Inf (x, n_bits):
	""" A big Decimal might actually by equal to +/-Inf if only n_bits mantissa
		is in effect."""
	if x.is_infinite():
		return True

	# Maximal unbiased exponent for the type.
	max_expo = 2 ** (mant_to_expo_bits[n_bits] - 1)
	return x.copy_abs() >= 2 ** max_expo

def is_Zero (x, n_bits):
	""" A big Decimal might actually by equal to 0 if only n_bits mantissa is
		in effect."""
	if x == 0:
		return True

	# Maximal unbiased exponent for the type.
	min_expo = 1 - 2 ** (mant_to_expo_bits[n_bits] - 1)
	return x.copy_abs() < 2 ** (1 + min_expo - n_bits)


class Result (object):
	""" m is a match for one line of input that looks like

			avr-libc  asin 5   2401 F24(1,0xdc3d5a,-24) F24(1,0x8497b5,-23)

		#1: Name of the "lib"
		#2: The function for which name
		#3: Natural number, an ID that groups this result with other results
		    for different LIBs but the same function and input.
		#4: Number of consumed ticks
		#5,#6: Input.
		#7,#8: Result: of #8 = function (#6).

		Assertion is that #5 == #7 is the number of bits of precision of the
		computation. """

	def __init__ (self, m):
		(self.lib,
		 self.func,
		 self.s_id,
		 self.s_ticks,
		 self.s_bits_x, self.s_x,
		 self.s_bits_y, self.s_y) = (m.group(i) for i in range(1,9))

		self.id = int(self.s_id)
		self.ticks = int(self.s_ticks)
		if self.s_bits_x != self.s_bits_y:
			raise ValueError ("inconsistent representation: ", m.group(0))
		self.n_bits = int(self.s_bits_x)
		self.x = Value (self.n_bits, self.s_x)
		self.y = Value (self.n_bits, self.s_y)

	def acc (self):
		fun = funcs[self.func]
		self.exact = fx = fun (self.x.val)
		y = self.y
		n_bits = self.y.n_bits
		best_acc =  -n_bits - 1.0
		if y.isNaN and fx.is_nan():
			return best_acc
		elif y.ispInf and is_Inf(fx,n_bits) and fx > 0:
			return best_acc
		elif y.ismInf and is_Inf(fx,n_bits) and fx < 0:
			return best_acc
		elif y.val == 0 and is_Zero(fx,n_bits):
			return best_acc

		if y.isNumber and y.val != 0 and fx != 0:
			dx = y.val - fx
			return max (ld (dx / fx), best_acc)
		return 0.0

	def __repr__ (self):
		return "%-8s %s %d %5d %.3f %s %s" % (self.lib, self.func, self.id, self.ticks,
				float (self.acc()), self.x, self.y)

def print_table_header_and_cookies (libs, funcs, gain_ref="f7"):
	print ("'lib' 'fun' 'id' 'ticks' 'acc' 'x' 'fx'")
	print ("# Cookie[-1]:  specific order of libs / namespaces.")
	print ('"c(%s)"' % ("'" + "','".join (libs) + "'"), "'dummy' -1 0 0 0 0")

	print ("# Cookie[-2]:  specific order of functions.")
	print ('"c(%s)"' % ("'" + "','".join (funcs) + "'"), "'dummy' -2 0 0 0 0")

	print ("# Cookie[-3]:  reference for gains.")
	print ("'%s' 'dummy' -3 0 0 0 0" % gain_ref)

pat_Fnum = r"\s+F(\d+)\((.*)\)"

pat_line = re.compile (r"([\w-]+)\s+(\w+)\s+(\d+)\s+(\d+)"
					   + pat_Fnum + pat_Fnum + "$")

prec2 = 80
bump_precision (prec2)
#get_pi (100)
NaN = Decimal("NaN")
funcs = {
	"sqrt": lambda x: NaN if x < 0 else x.sqrt(),
	"log":  lambda x: NaN if x < 0 else x.ln(),
	"exp":  lambda x: x.exp(),
	"sin":  lambda x: get_sin (x, prec2),
	"cos":  lambda x: get_cos (x, prec2),
	"tan":  lambda x: get_tan (x, prec2),
	"asin": lambda x: get_asin (x, prec2),
	"acos": lambda x: get_acos (x, prec2),
	"atan": lambda x: get_atan (x, prec2),
}

s_libs = []
s_funcs = []
results = []

for line in fin.readlines():
	# Ditch comments.
	hash = line.find ("#")
	if hash >= 0:
		line = line[:hash]
	line = line.strip()
	if line == "":
		continue

	m = pat_line.match (line)
	if not m:
		raise Exception ("Unrecognized line: ", line)

	r = Result (m)
	if not r.lib in s_libs:
		s_libs += [ r.lib ]
	if not r.func in s_funcs:
		s_funcs += [ r.func ]
	results += [ r ]

print_table_header_and_cookies (s_libs, s_funcs)

id = None
exact = None
for r in results:
	if not id or id != r.id:
		id = r.id
		if exact is None:
			print ("#")
		else:
			print ("# %30s %s, %s" % ("", exact.exact, cdef(exact.exact)))
			exact = None
	print (r)
	if r.n_bits == 56:
		exact = r

if exact is None:
	print ("#")
else:
	print ("# %30s %s, %s" % ("", exact.exact, cdef(exact.exact)))
