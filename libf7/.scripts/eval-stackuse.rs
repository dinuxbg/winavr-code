
x <- read.table ("stackuse.data", header=TRUE)
x <- x[order(x$id),]
x

n_vals = length(x[,1])
n_cols = length(x[1,])

nlabels <- rep(x$fun, n_cols - 2)

cols = gray.colors (n_vals, start=0.9, end=0.3, gamma=2.2)

# http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf

cols = c("#ccccff" #"#aaaaff"
        , "#8888ff", "#6666ff"
        , "#ffee88"
        , "#ffaaaa", "#ff8888", "#ff6666"
        #, "#bbffaa"
        , "#66ff66"
        , "#bb99ff", "#9966ff"
        , "white"
        #, "#60ff60", "#c0ffc0"
        #, "#ccccff", "#a0a0ff", "#bbbbff", "#7070ff"
        #, "#ffaaaa"
        )


cols <- cols[1:n_vals]

png (file = "stackuse.png", width=880, height=500, res=80)

barplot (height=as.matrix(x[,3:n_cols])
        , beside=TRUE
        , main="Stack usage in Bytes"
        #, sub="submain\nsubmain2"
        , col=cols
        , cex.names=1.2
        , ylab=c("Stack usage in B")
)

for (pos in 0:5)
    abline (h=50*pos, lty=3)

legend (x=max(x[,3:n_cols]), legend=x[,1]
        , col="black"#cols
        ,cex=1.2
        ,pt.bg=cols
        ,pt.cex=2.6
        ,pch=22 # or 15 for filled
)

dev.off()