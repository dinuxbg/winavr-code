from __future__ import print_function

__all__ = ["Poly"]

import math
from fractions import Fraction as Frac
from decimal import *
from numbers import Integral

from mymathutils import *

# n -> n-th Chebyshev polynomial of the 1st kind.
_Tn = {}

# n-> McLaurin
_asin = {}

class Poly (object):
	""" A Polynomial"""
	
	# Styles for toString
	TEX = "TeX"
	GNUPLOT = "GNUplot"
	DECIMAL = "Decimal"
	IEEE_double = (53,11)
	IEEE_float  = (24,8)

	def __init__ (self, ai):
		self.a = ai if type(ai) is list else [ai]

	def __len__ (self):
		return len (self.a)

	def get_deg (self):
		for k in range (len (self), 0, -1):
			if self.a[k-1] != 0:
				return k - 1
		return 0

	@staticmethod
	def monome (ex, ak=1):
		a = [0 for k in range (ex)]
		return Poly (a + [ak])
	
	deg = property (get_deg, None, None, """Degree >= 0 of the polynomial""")

	def trim (self):
		return Poly (self.a[0 : 1 + self.deg])

	def crop (self, k1=None, k2=None):
		""" Return a Poly where every coefficient outside [k1,k2] is 0."""
		if k1 is None:	k1 = 0
		if k2 is None:	k2 = self.deg

		if k1 > k2 or k1 < 0:
			raise ValueError (k1, k2)

		if k2 >= self.deg and len(self) == 1 + self.deg:
			return self
		return Poly (self.a[k1 : 1 + min (k2, self.deg)])

	def __getitem__ (self, k):
		""" Get k-th coefficient."""
		return self.a[k] if k < len(self.a) else 0

	def __call__ (self, x):
		""" Eval poly at x."""
		if type (x) == Decimal and self.type() is Frac:
			(pp, ff) = self.to_int()
			return pp (x) / ff

		y = x * 0
		one = x ** 0 if x != 0 else get_1 (x)
		for k in range (self.deg, -1, -1):
			y = self.a[k] * one + y * x
		return y

	def map (self, call):
		return Poly (map (call, self.a))

	def height (self):
		hh = 0
		for k in range (1 + self.deg):
			ak = self[k]
			hh = max (hh, ak if ak >= 0 else -ak)
		return hh

	def type (self):
		tt = long
		for k in range (len (self)):
			ak = self[k]
			if isinstance (ak, Integral):
				pass
			elif type (ak) is Frac:
				if tt is Decimal:
					return None
				tt = Frac
			elif type (ak) is Decimal:
				if tt is Frac:
					return None
				tt = Decimal
		return tt

	def fromScalar (self, x):
		return x if type(x) is Poly else Poly (x)
		
	def __add__ (self, q):
		q = self.fromScalar (q)
		a = [self[k] + q[k] for k in range (1 + max (self.deg, q.deg))]
		return Poly (a)

	def __radd__ (self, q):
		return self + q

	def __neg__ (self):
		a = [-self[k] for k in range (1 + self.deg)]
		return Poly (a)

	def __sub__ (self, q):
		q = self.fromScalar (q)
		a = [self[k] - q[k] for k in range (1 + max (self.deg, q.deg))]
		return Poly (a)

	def __rsub__ (self, q):
		q = self.fromScalar (q)
		a = [q[k] - self[k] for k in range (1 + max (self.deg, q.deg))]
		return Poly (a)

	def __mul__ (self, q):
		q = self.fromScalar (q)
		a = [0 for i in range (1 + self.deg + q.deg)]
		for i in range (1 + self.deg):
			for j in range (1 + q.deg):
				a[i + j] += self[i] * q[j]
		return Poly (a)

	def __rmul__ (self, q):
		return self * q

	def __pow__ (self, ex):
		y = Poly(1)
		y2 = self
		while True:
			if ex & 1:
				y *= y2
			ex >>= 1
			if ex == 0:
				break
			y2 *= y2
		return y

	def __eq__ (self, p):
		d = self.deg
		if isScalar (p):
			return 0 == d and p == self[0]
		if d != p.deg:
			return False
		for k in range (1 + d):
			if self[k] != p[k]:
				return False
		return True

	def __ne__ (self, p):
		return not (self == p)

	def support (self):
		" Number of non-0 coefficients."""
		return sum (1 for k in range (1 + self.deg) if self[k] != 0)

	def is_sum (self):
		# used by class XPoly
		return self.support() > 1

	def ord_x (self):
		""" Maximal power k such that x^k devides p without remainder."""
		if self == 0:
			return 0
		for k in range (1 + self.deg):
			if self[k] != 0:
				return k

	def msub (self, p, c, r):
		""" return self - p * c * x^r with the assertion that the highest
			order term will vanish."""
		#print "%s -= %s*x^%d * (p=%s)" % (self, c, r, p)
		ds = self.deg
		if ds - r < p.deg:
			raise ValueError (self)
		probe = self[ds] - c * p[ds - r]
		if self.type() in (Decimal, None) or p.type() in (Decimal, None):
			if abs (probe) > 0.001:
				print ("Poly.msub.self =", self)
				print ("Poly.msub.p    =", p)
				print ("Poly.msub.c    =", c)
				print ("Poly.msub.r    =", r)
				print ("Poly.msub.probe =", probe, type(probe))
				raise ValueError (self)
		else:
			if probe != 0:
				print ("Poly.msub.probe = ", probe, self)
				raise ValueError (self)

		# 0 ... r -1
		a = self.a[0 : r]
		#print "acopy=", a

		# r ... deg(self) - 1
		for k in range (r, ds):
			a += [self[k] - c * p[k - r]]
		#print "adone=", a
		return Poly (a)

	def coeff_gcd (self):
		gc = 0
		for k in range (1 + self.deg):
			gc = gcd (gc, self.a[k])
		return gc

	def to_int (self):
		""" Decompose p = (q, f) such that: p = q / f, f in Z\{0}, q in Z[x]."""
		f = self.coeff_gcd()
		if f == 0:
			return (1, Poly(0))
		if f * self[self.deg] < 0:
			f = -f
		a = []
		for k in range (1 + self.deg):
			ak = Frac (self[k] * f.denominator)
			assert ak.denominator == 1
			a += [ak.numerator]
		return (Poly (a), f.denominator)

	def __lshift__ (self, n):
		if n >= 0:
			return Poly ([0 for k in range(n)] + self.a)
		else:
			return Poly (self.a[-n:])

	def __rshift__ (self, n):
		return self << -n

	def stride (self, n, s):
		""" Starting at a_n, return a polynomial composed of the s-th
			coefficients."""
		assert n >= 0 and s > 0
		a = [self[n + k*s] for k in range(1+self.deg) if n + k*s <= self.deg]
		return Poly (a)

	_style_Frac = {
		TEX		: r"\frac{%s}{%s}",
		GNUPLOT	: "%s.0/%s",
		None	: "%s/%s"
	}

	_style_var = {
		TEX		: ("%s^%d",  "%s^{%d}"),
		GNUPLOT	: ("%s**%d", "%s**%d"),
		None	: ("%s^%d",  "%s^%d")
	}

	_style_monome = {
		TEX		: r"%s\,%s",
		GNUPLOT	: "%s*%s",
		None	: "%s%s"
	}

	def __repr__ (self):
		return self.toString()

	@staticmethod
	def scalarToString(s, style=None):
		if type(s) is Frac:
			zz = s.numerator
			nn = s.denominator
			if nn != 1:
				return Poly._style_Frac[style] % (zz,nn)
			s = zz
		return "%s" % s

	def toString (self, style=None, var='x'):
		s = ""
		if style == Poly.DECIMAL:
			# ??? must wrap each Decimal into [] to get repr.
			to_dec = lambda x : ("%s" % [Decimal(x)]) [1:-1]
			s = ', '.join (map (to_dec, self.a))
			return "Poly ([%s])" % s

		elif style in (Poly.IEEE_double, Poly.IEEE_float):
			(mant,expo) = style
			n_dig = 1 + ((mant + expo - 1) >> 2)
			to_ieee = lambda x : "0x%0*x" % (n_dig, raw_IEEE754 (x, mant, expo))
			s = ", ".join (map (to_ieee, self.a))
			return s if var is None else "uint64_t %s[] = { %s };" % (var, s)

		for k in range(1 + self.deg):
			ak = self[k]
			if ak == 0:
				continue

			# How a_k represents and adds to the string representation so far.
			if s == "":
				op = "%s"
				if isScalar(ak) and ak < 0:
					op, ak = "-%s", abs(ak)
				elif type(ak) is Poly and ak.is_sum():
					op = "(%s)"
			else:
				op = " + %s"
				if isScalar(ak) and ak < 0:
					op, ak = " - %s", abs(ak)
				elif type(ak) is Poly and ak.is_sum():
					op = " + (%s)"

			# How x^k represents
			xk = var
			if type(var) is Poly:
				xk = var.toString (style=style)
				xk = xk if not var.is_sum() else "(%s)" % xk
			fmt = Poly._style_var[style][0 if k < 10 else 1]
			xk = xk if k == 1 else fmt % (xk,k)
			sk = Poly.scalarToString (ak, style)

			# How a_k * x^k represents
			if   k == 0:	ak_xk = sk
			elif ak == 1:	ak_xk = xk
			else:			ak_xk = Poly._style_monome[style] % (sk,xk)
			
			s += op % ak_xk

		return s if s != "" else "0"

	def divmod (self, p):
		if p == 0:
			raise ValueError ("division by Poly 0")
		r = self
		#print "divmod %s, %s" % (r, p)
		dr = r.deg
		dp = p.deg
		dq = dr - dp
		if dq < 0:
			return (Poly (0), r)

		q = [0 for k in range (1 + dq)]

		for k in range (dq, -1, -1):
			if type(r[k + dp]) is Decimal or type(p[dp]) is Decimal:
				q[k] = r[k + dp] / p[dp]
			else:
				q[k] = Frac (r[k + dp]) / p[dp]
			#print "q[%d] = %s / %s = %s"  % (k, r[k + dp], p[dp], q[k])
			if q[k]:
				r = r.msub (p, q[k], k)

		q = Poly (q)
		probe = q * p + r - self
		if probe != 0:
			if probe.type() in (Frac, long):
				raise Exception ("bug in Poly.divmod: %s != 0" % probe)
		return (q, r)

	def __div__ (self, q):
		q = self.fromScalar (q)
		(q, _) = self.divmod (q)
		return q

	def deriv (self):
		a = [(k + 1) * self[k + 1] for k in range (self.deg)]
		return Poly (a)

	@staticmethod
	def ggT (p, q):
		#print "ggT(",p," ; ",q,")"
		while q != 0:
			((_, q), p) = (p.divmod (q), q)
			#(p, q) = (q, r)
			#print "ggT(",p," ; ",q,")",  "quot=",dummy
		return p

	@staticmethod
	def xggT (a, b, stopper = lambda x: False):
		# https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Pseudocode
		(r1, r0) = (b, a)
		(s1, s0) = (Poly(0), Poly(1))
		(t1, t0) = (Poly(1), Poly(0))

		while r1 != 0:
			if r1.type() is Decimal:
				if r1.height() < 1e-20:
					break
			#print "\nstopper: ", stopper (t1)
			if stopper (t1):
				break
			#print ("xggT.r:", (r1, r0))
			#print ("xggT.s:", (s1, s0))
			#print ("xggT.t:", (t1, t0))
			(q, dummy) = r0.divmod (r1)
			(r1, r0) = (r0 - q * r1, r1)
			(s1, s0) = (s0 - q * s1, s1)
			(t1, t0) = (t0 - q * t1, t1)
		#print (s0, t0)
		#print ("Probe:", s0*a + t0*b)
		#print ("ggT =", r0)
		if r0.type() is Decimal:
			probe = r0 - s0*a - t0*b
			#print ("xggT.probe: ", probe)
			#assert probe.height() < 0.001, "xggT failed"
			pass
		else:
			assert r0 == s0*a + t0*b, "xggT failed"
			(qa, ra) = a.divmod (r0)
			(qb, rb) = b.divmod (r0)
			assert r0 == s0*a + t0*b, "xggT failed"
			if r1 == 0:
				# Not halted by stopper().
				assert ra == 0 and rb == 0
				assert t1 == qa or t1 == -qa, str(t1) + " != +/- " + str(qa)
				assert s1 == qb or s1 == -qb, str(s1) + " != +/- " + str(qb)
		return (r0, s0, t0, t1, s1)

	def Pade (self, m, n):
		""" Compute the [m/n] Pade rational approximation."""
		# https://en.wikipedia.org/wiki/Pade_approximation#Computation
		stopper = lambda x: x.deg > n
		(rn, sn, tn, qs, qt) = Poly.xggT (Poly.monome(n + m + 1), self, stopper)
		#print (rn, tn)
		#print "rn.gcd", rn.coeff_gcd()
		#print "tn.gcd", tn.coeff_gcd()
		if rn.type() is Decimal or tn.type() is Decimal:
			return (rn, tn)
		mul_r = Frac (1, rn.coeff_gcd())
		mul_t = Frac (1, tn.coeff_gcd())
		lc = lcm (mul_r, mul_t)
		#print mul_r, mul_t, lc
		#print (rn * lc, tn * lc)
		return (rn * lc, tn * lc)

	@staticmethod
	def Tn (n):
		""" n-th Chebyshev polynomial of the 1st kind. """
		if not type(n) is int or n < 0:
			raise ValueError (n)
		
		if not n in _Tn.keys():
			if n == 0:
				t = Poly(1)
			elif n == 1:
				t = Poly([0,1])
			else:
				t1 = Poly.Tn(n-1)
				t2 = Poly.Tn(n-2)
				t = (2 * t1 << 1) - t2
			_Tn[n] = t
		return _Tn[n]

	@staticmethod
	def Tn_extrema_x (n, a=-1, b=1):
		""" Get the extrema of Chebyshev polynomial of the 1st kind, mapped
			from [-1,1] to the interval [a,b].  This is a list of length n+1
			with x[0] = a and x[n] = b. """
		x = []
		m = (b - a) / 2
		for k in range(n+1):
			# Assumption: no high precision is needed.
			xk = math.cos(math.pi * k/n)
			if type(a) is Decimal:
				xk = Decimal(xk)
			x += [ (1 - xk) * m + a ]
		return x

	@staticmethod
	def atan (n):
		""" McLaurin to degree n. """
		a = []
		for k in range (n+1):
			sign = -1 if k & 2 else 1
			a += [0 if k % 2 == 0 else Frac (sign, k)]
		return Poly (a)

	@staticmethod
	def asin (n):
		""" McLaurin to degree n. """
		if not n in _asin.keys():
			a = []
			for k in range (n+1):
				if k % 2 == 0:
					a += [0]
				else: 
					a += [Frac (fak (k, 2), k * k * fak (k-1, 2))]
			_asin[n] = Poly (a)
		return _asin[n]

	@staticmethod
	def exp (n):
		""" McLaurin up to degree n. """
		a = [Frac (1, fak (k)) for k in range (n+1)]
		return Poly (a)

	@staticmethod
	def sin (n):
		""" McLaurin up to degree n. """
		a = []
		for k in range (n+1):
			sign = -1 if k & 2 else 1
			a += [0 if k % 2 == 0 else Frac (sign, fak (k))]
		return Poly (a)

	@staticmethod
	def cos (n):
		""" McLaurin up to degree n. """
		a = []
		for k in range (n+1):
			sign = -1 if k & 2 else 1
			a += [0 if k % 2 == 1 else Frac (sign, fak (k))]
		return Poly (a)

	@staticmethod
	def sqrt (n):
		""" Taylor at 1. """
		a = []
		for k in range (n+1):
			if k == 0:
				a += [1]
				continue
			sign = 1 if k <= 1 or k & 1 == 1 else -1
			nn = fak (k) * 2 ** k
			zz = Frac (fak (2*k - 2), fak (k-1) * 2 ** (k-1))
			a += [sign * zz / nn]
		return Poly (a)

	@staticmethod
	def sqrt1 (n):
		a = a_sqrt (n)
		a[0] = 0
		return Poly (a)

	@staticmethod
	def atan_deriv_numerator (n):
		""" Numerator of d^n/dx^n atan (x)  for  n > 0  which is
			a polynomial of degree n - 1."""
		x2_1 = Poly ([1, 0, 1])
		p = Poly (1)
		for k in range (1, n):
			p = x2_1 * p.deriv() - Poly([0, 2*k]) * p
		return p

	@staticmethod
	def atan_expand_at (n, x):
		""" Expand atan() up de degree n at x.  The 0-th coefficient
			(which would be atan(x)) will be set to 0."""
		a = [0]
		for k in range (1, 1 + n):
			zz = Poly.atan_deriv_numerator (k)
			nn = (1 + x*x) ** k
			a += [Frac (zz(x), nn * fak (k))]
		return Poly (a)

	@staticmethod
	def func_a (n, x0 = 0):
		""" Function a(x) around x0 as in asin around 1 to degree n:
			acos (1 - x) = sqrt(2x) * a(x)
			asin (1 - x) = pi/2 - sqrt(2x) * a(x) """

		a = []
		for k in range (1 + n):
			zz = fak(2*k)
			nn = fak(k) ** 2 * (2*k+1) * 8**k
			if type(x0) is Decimal:
				a += [Decimal (zz) / nn]
			else:
				a += [Frac (zz, nn)]
		pp = Poly (a)
		if x0 == 0:
			return pp
		return pp (Poly([-x0,1]))

	def to_Decimal (self):
		a = []
		for k in range (1 + self.deg):
			ak = self[k]
			if type (ak) is Frac:
				a += [Decimal (ak.numerator) / ak.denominator]
			else:
				a += [ak]
		return Poly (a)
	
	def cdef (self):
		print ("// " + str (self))
		for k in range (1 + self.deg):
			if self[k]:
				print (cdef (self[k]))

	@staticmethod
	def dn_dxn_asin (n):
		""" (1-x^2)^(n-1/2) * d^n/dx^n arcsin(x)  which is a polynomial
			of degree n - 1 in Z[x], even for odd n, odd for even n. """
		res = Poly (0)
		kk = int((n - 1) / 2)
		qk = Poly (1)	# (1-x^2)^k
		for k in range (1 + kk):
			# zz is numerator for (1-x^2) ^ (n-1/2 - k)
			# "Empty" double-factorials evaluate to 1.
			zz = binom (n-1, 2*k)
			zz *= fak (2*k - 1, 2)
			zz *= fak (2*n - 3 - 2*k, 2)
			# zz * x^(n-1 - 2*k) is numerator for (1-x^2) ^ (n-1/2 - k),
			# therefore multiply with qk = (1-x^2)^k so that all terms of the
			# sum will have the same denominator  (1-x^2) ^ (n-1/2).
			res += Poly.monome (n-1 - 2*k, zz) * qk
			qk *= Poly ([1, 0, -1])
		return res

	@staticmethod
	def dn_dxn_func_a (n):
		""" Let a(x) = acos(1-x) / sqrt(2x).  Return a polynomial in Z[x] such
			that the n-th derivative of a(x) is:
				a0 + p(x) / ( (2x)**n * (2-x)**(n-1/2)) / sqrt(2)
			with 
				a0 = a(x) * (-1)**n * (2n-1)!! / (2x)**n
			"""
		res = Poly (0)
		q1 = Poly ([1,-1])	# 1 - x
		q2 = Poly ([2,-1])	# 2 - x
		for k in range (1, 1 + n):
			pk = Poly.dn_dxn_asin(k)
			# Factor from Leibnitz Rule.
			fk = binom (n, k)
			# Factor from n-th derivative of 1/sqrt(n)
			fk *= (-1) ** (n-k)
			fk *= fak (2*n - 2*k - 1, 2)
			# Factor due to 1-x in acos^(k) (1-x).  "1" because acos' = -asin'.
			fk *= (-1) ** (1+k)
			# Factor for common denominator
			fk *= 2 ** k
			fk *= q2 ** (n - k)
			res += fk * pk (q1)
		#print ("res=", res)
		return res

	@staticmethod
	def func_a_expand_at (n, x):
		""" Expand func_a() up de degree n at x.  The 0-th coefficient which
			would be
				a(x) = acos(1-x)/sqrt(2x) = asin(sqrt(x/2))/sqrt(x/2)
			will be set to ???."""

		# For now, x must be of the form  x = 2 - 2r^2  with  r in Q
		# so that  sqrt(2-x) = sqrt2 * r.
		x = Frac(x)
		r = exact_sqrt (1-x/2)
		if r < 0:
			raise ValueError ("x=%s is not 2 - 2*r^2  with r in Q" % x)
		print ("r=%s" % r)

		a = [0]

		# p_k(x) such that the k-th derivative of a(x) is:
		#		a0 + p_k(x) / ( (2x)**k * (2-x)**(k-1/2)) / sqrt(2)
		#	with 
		#		a0 = a(x) * (-1)**k * (2k-1)!! / (2x)**k
		#
		# Due to our special choice of x we have
		#		a0 + p_k(x) / ( (2x)**k * (2-x)**(k-1/2)) / sqrt(2)
		#     = a0 + p_k(x) * r * (4x-2x^2) ** -k
		
		# We compute and return 2 polynomials S(x) and T(x) such that the
		# desired expansion of func_a has the representation
		# 	S(x) + a(x0)*T(x)

		xx = 4*x - 2*x*x

		s = [0]
		t = [1]
		for k in range(1,1+n):
			p_k = Poly.dn_dxn_func_a (k)
			print ("p_%d =" % k, p_k)
			sk = p_k(x) * r / xx**k
			tk = (-1)**k * fak(2*k-1, 2) / (2*x)**k
			s += [sk]
			t += [tk]
		S = Poly(s)
		T = Poly(t)
		print ("S=", S)
		print ("T=", T)
		return (S, T)
