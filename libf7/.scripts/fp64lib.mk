.PHONY: all XXX

override MCU = avrxmega3

MMCU = $(strip $(MCU))

override CFLAGS := $(CFLAGS)

FP64FLAGS =
FP64FLAGS += $(CFLAGS)

all: libfp64-$(MMCU).a

libfp64-%.a :
	$(warning $(patsubst libfp64-%.a,%,$@))
	make clean-libfp64 libfp64.a FP64FLAGS=-mmcu=$(patsubst libfp64-%.a,%,$@)
	mv libfp64.a $@

XXX: $(patsubst %,libfp64-avr%.a, 5 51 6)
XXX: $(patsubst %,libfp64-avrxmega%.a, 2 3 4 5 6 7)

XCC = e:/WinAVR/8.0.1_2018-04-23/bin/avr-gcc

OBJDUMP = $(XCC)/../avr-objdump
OBJCOPY = $(XCC)/../avr-objcopy
OBJSIZE = $(XCC)/../avr-size
OBJNM   = $(XCC)/../avr-nm
	
FP64_ASM_PARTS += 10pown abs acos addsf3x atan2 cbrt ceil classify
FP64_ASM_PARTS += cmp cmpsd2 copysign cordic cosh debug disd divsf3x ds
FP64_ASM_PARTS += exp fdim fixxdfsi floor fma fmax fmod frexp fsplit3
FP64_ASM_PARTS += ftoa1 gesd2 getexp10 hypot inf inverse isBzero isfinite
FP64_ASM_PARTS += isinf isnan ldexp log log10 lrint lround lshift64 modf
FP64_ASM_PARTS += mul64AB mulsd3x nan negdf2 norm2 pow powser powsodd
FP64_ASM_PARTS += pretA pscA pscB round sd sidf signbit sin sqrt square
FP64_ASM_PARTS += strtod tanh tostring trunc zero

FP64_ASM_OBJECTS = $(patsubst %, fp64_asm_%.o, $(FP64_ASM_PARTS))

# Depends

$(FP64_ASM_OBJECTS) : asmdef.h fp64def.h

fp64_asm_%.o : fp64_%.sx
	$(XCC) $< $(FP64FLAGS) -c -o $@ \
		-DMLIB_SECTION=.text.libfp64.asm.$(patsubst fp64_asm_%.o,%,$@)

libfp64.a: \
		$(patsubst %, libfp64.a(%), $(FP64_ASM_OBJECTS))
	$(XCC)/../avr-gcc-ranlib $@

libfp64.a(%.o): %.o
	$(XCC)/../avr-gcc-ar cr $@ $<

.PHONY: clean clean-libfp64

clean: clean-libfp64
	rm -f $(wildcard libfp64-*.a)

clean-libfp64:
	rm -f $(wildcard libfp64.a $(FP64_ASM_OBJECTS))