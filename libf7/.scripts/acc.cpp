#include "libf7.h"
#include "acc.h"
#include "string.h"

#include <avr/pgmspace.h>

static int LL = 1;

#define DD(str, X)                              \
  do {                                          \
      if (LL)                                   \
      {                                         \
          LOG_PSTR (PSTR (str));                \
          LOG_PUSH_OFF; f7_dump (X);            \
          LOG_POP;                              \
      }                                         \
  } while (0)

extern "C" {
#if __has_include ("avr_f64.h")
#include "avr_f64.h"
#define HAVE_AVR_F64
#endif
}

#if __has_include ("fp64lib.h")
#include "fp64lib.h"
#define HAVE_FP64LIB
#endif

bool ja;

void perf_func_f7 (int id, const char *label,
                   void (*f) (f7_t*, const f7_t*), const f7_t *xx)
{
    f7_t yy;
    PERF_PLABEL (id, label);
    PERF_START (id);
    f (&yy, xx);
    PERF_STOP (id);
}

void print_ticks_line (const char *nmspc, const char *func, uint16_t id,
                       uint32_t ticks, int16_t expo, float xtra1, float xtra2)
{
    LOG_PFMT_STR (PSTR ("%s "), nmspc);
    LOG_STR (func);
    LOG_U16 (id);
    LOG_U32 (ticks);
    LOG_S16 (expo);
    LOG_FLOAT (xtra1);
    LOG_FLOAT (xtra2);
    LOG_STR ("\n");
}

void f7_rand (f7_t *rr, int expo_min, int expo_max)
{
    uint32_t x = avrtest_prand();

    f7_clr(rr);
    ((uint32_t*) rr)[0] = avrtest_prand();
    ((uint32_t*) rr)[1] = avrtest_prand();
    rr->flags = 0;

    rr->mant[F7_MANT_BYTES - 1] |= 0x80;

    uint16_t dex = expo_max - expo_min;
    if (dex == 0)
    {
        rr->expo = expo_min;
        return;
    }

    uint8_t lz = __builtin_clz (dex);
//    LOG_FMT_U8 ("lz = %d\n", lz);
    uint16_t mask = (2u << (15 - lz)) - 1;

    uint16_t r;
    do {
        r = avrtest_prand() & mask;
    } while (r > dex);

    rr->expo = expo_min + r;
}

#ifdef HAVE_AVR_F64

#define MK_WRAP64_1(F)                          \
    __attribute__((__unused__))                 \
    static float64_t wrap_ ## F (float64_t x)   \
    {                                           \
        return F (x);                           \
    }
MK_WRAP64_1 (f_arctan);
MK_WRAP64_1 (f_arcsin);
MK_WRAP64_1 (f_arccos);
MK_WRAP64_1 (f_tan);
MK_WRAP64_1 (f_sin);
MK_WRAP64_1 (f_cos);

#define f_atan wrap_f_arctan
#define f_asin wrap_f_arcsin
#define f_acos wrap_f_arccos

#undef f_sin
#define f_sin wrap_f_sin

#undef f_cos
#define f_cos wrap_f_cos

#undef f_tan
#define f_tan wrap_f_tan

#endif // HAVE_AVR_F64


void f7_dummy (f7_t *cc, const f7_t *aa)
{
    f7_copy (cc, aa);
}

uint64_t w_dummy (uint64_t x)
{
    return x;
}

#define MK_1WRAP(N)                     \
extern "C" uint64_t w_##N (uint64_t);   \
__attribute__((noinline,noclone))       \
uint64_t w_##N (uint64_t x)             \
{                                       \
    f7_t xx;                            \
    f7_set_double (&xx, x);             \
    f7_##N (&xx, &xx);                  \
    return f7_get_double (&xx);         \
}
MK_1WRAP (sqrt)
MK_1WRAP (exp)
MK_1WRAP (sin)
MK_1WRAP (cos)
MK_1WRAP (tan)
MK_1WRAP (atan)
MK_1WRAP (asin)
MK_1WRAP (acos)

extern "C" uint64_t w_log (uint64_t);
__attribute__((noinline,noclone))
uint64_t w_log (uint64_t x)             
{
    f7_t xx;
    
    if (ja)
    {
        __builtin_memcpy (&xx.mant, &x, 8);
        if (ja) DD ("wrap: rawmant = ", &xx);
    }
    
    f7_set_double (&xx, x);
    if (ja) DD ("wrap: xx = ", &xx);
    f7_log (&xx, &xx);
    if (ja) DD ("wrap: zz = ", &xx);
    return f7_get_double (&xx);
}

#define MK_2WRAP(N)                             \
extern "C" uint64_t w_##N (uint64_t, uint64_t); \
__attribute__((noinline,noclone))               \
uint64_t w_##N (uint64_t x, uint64_t y)         \
{                                               \
    f7_t xx, yy;                                \
    f7_set_double (&xx, x);                     \
    f7_set_double (&yy, y);                     \
    f7_##N (&xx, &xx, &yy);                     \
    return f7_get_double (&xx);                 \
}
MK_2WRAP (add)
MK_2WRAP (sub)
MK_2WRAP (mul)
MK_2WRAP (div)

enum
{
    F_FLOAT,
    F_F7,
    F_WRAP7,
    F_U64,
    F_FP64
} libid_t;

typedef struct
{
    uint8_t flags; // like f7_t
    uint64_t mant;
    int expo;
    uint8_t dig_mant;
} float_t;

typedef struct
{
    float x;
    uint64_t x64, w64, fp64;
    f7_t xx;
} x_t;

typedef struct
{
    uint8_t dig_exp, dig_mant;
    int max_exp, exp_bias;
} layout_t;

typedef struct
{
    uint8_t id;
    const char* name;
    const layout_t *layout;
} lib_t;

enum
{
    ID_none,
    ID_add, ID_sub, ID_mul, ID_div,
    ID_exp, ID_log, ID_pow,
    ID_sin,  ID_cos,  ID_tan,
    ID_asin, ID_acos, ID_atan,
    ID_sqrt, ID_square,
    ID_count
};

typedef struct
{
    const lib_t *lib;
    const char *name;
    uint8_t id;
    union
    {
        float (*f_float)(float);
        void (*f_f7)(f7_t*, const f7_t*);
        uint64_t (*f_u64)(uint64_t);
        uint64_t (*f_wrap7)(uint64_t);
        uint64_t (*f_fp64)(uint64_t);

        float (*f_2float)(float, float);
        void (*f_2f7)(f7_t*, const f7_t*, const f7_t*);
        uint64_t (*f_2u64)(uint64_t, uint64_t);
        uint64_t (*f_2wrap7)(uint64_t, uint64_t);
        uint64_t (*f_2fp64)(uint64_t, uint64_t);
    };
} f_t;

typedef struct
{
    const lib_t *lib[10];
    size_t n_lib;
    const f_t *f[10];
    const f_t *inv[10];
    size_t n_f;
    uint8_t n_args;
} fun_t;

#define FLT_DIG_EXP   8
#define FLT_DIG_MANT  (31 - FLT_DIG_EXP)
#define FLT_MAX_EXP   ((1 << FLT_DIG_EXP) - 1)
#define FLT_EXP_BIAS  (FLT_MAX_EXP >> 1)
static const layout_t lay_float = { FLT_DIG_EXP, FLT_DIG_MANT, FLT_MAX_EXP, FLT_EXP_BIAS };

#define DBL_DIG_EXP   11
#define DBL_DIG_MANT  (63 - DBL_DIG_EXP)
#define DBL_MAX_EXP   ((1 << DBL_DIG_EXP) - 1)
#define DBL_EXP_BIAS  (DBL_MAX_EXP >> 1)
static const layout_t lay_double = { DBL_DIG_EXP, DBL_DIG_MANT, DBL_MAX_EXP, DBL_EXP_BIAS };

static const layout_t lay_f7t = { 16, 56, INT16_MAX, 0 };

void out_table_header_and_cookies (const fun_t *fs, const char *libname_gain_ref)
{
    LOG_PSTR (PSTR ("'lib' 'fun' 'id' 'ticks' 'acc' 'x' 'fx'\n"));

    LOG_PSTR (PSTR ("# Cookie[-1]:  specific order of libs / namespaces.\n\"c("));
    const lib_t* const *lib = fs->lib;
    for (uint8_t i = 0; i < fs->n_lib; lib++)
    {
        if (*lib)
        {
            if (i++) avrtest_putchar (',');
            LOG_PFMT_STR (PSTR ("'%s'"), (*lib)->name);
        }
    }
    LOG_PSTR (PSTR (")\" 'dummy' -1 0 0 0 0\n"));

    LOG_PSTR (PSTR ("# Cookie[-2]:  specific order of functions.\n\"c("));
    for (uint8_t i = 0; i < fs->n_f; i++)
    {
        if (i) avrtest_putchar (',');
        LOG_PFMT_STR (PSTR ("'%s'"), fs->f[i]->name);
    }
    LOG_PSTR (PSTR (")\" 'dummy' -2 0 0 0 0\n"));

    if (libname_gain_ref)
        LOG_PFMT_STR (PSTR ("# Cookie[-3]:  reference for gains.\n"
                            "'%s' 'dummy' -3 0 0 0 0\n"), libname_gain_ref);
}


void out_result (const char *space, const char *fun, uint16_t id,
                 uint32_t ticks, float bits_acc, float x, float y)
{
    LOG_FMT_STR ("%-12s ", space);
    LOG_FMT_STR ("%-4s ", fun);
    LOG_U16 (id);
    LOG_FMT_U32 ("% 6u ", ticks);
    //if (expo == INT8_MIN) LOG_STR ("NA "); else LOG_S16 (expo);
    LOG_FMT_FLOAT ("%g ", bits_acc);
    LOG_FMT_FLOAT ("%g ", x);
    LOG_FMT_FLOAT ("%g", y);
    LOG_STR ("\n");
}

#include <assert.h>
#include <math.h>

static float accbits_f7_t (const f7_t *xx, const f7_t *zz, int8_t best = -F7_MANT_BITS - 1)
{
    // Assumption: If NaN then this is the desired result.
    uint8_t x_class = f7_classify (xx);
    uint8_t z_class = f7_classify (zz);
//    DD ("xx", xx);
//      DD ("zz", zz);
    
    if (f7_class_nan (x_class | z_class))
        return best;

    uint8_t n_inf = f7_class_inf (x_class) + f7_class_inf (z_class);
    
    if (n_inf == 2 && ! f7_class_sign (x_class ^ z_class))
        return best;

    if (n_inf)
        return INT8_MAX;

    f7_t dd;
    int x_expo = f7_is_zero (xx) ? INT16_MIN : xx->expo;
    int z_expo = f7_is_zero (zz) ? INT16_MIN : zz->expo;
    int expo = x_expo > z_expo ? x_expo : z_expo;
    f7_sub (&dd, xx, zz);

    float ld = fabsf (f7_get_float (&dd));
    float lx = fabsf (f7_get_float (xx));
    if (ld == 0 || lx == 0)
        return best;

    ld = log (ld) / M_LN2;
    lx = log (lx) / M_LN2;
//    printf ("# ld = %f, dd.expo = %d\n", ld - lx, dd.expo - expo);
    //DD ("dd", &dd);
    return ld - lx; //dd.expo - expo;
}

static float accbits_float (float x, float z)
{
    f7_t xx, zz;
    f7_set_float (&xx, x);
    f7_set_float (&zz, z);
    return accbits_f7_t (&xx, &zz, -24 - 1);
}

static float accbits_u64 (uint64_t x, uint64_t z)
{
    f7_t xx, zz;
    f7_set_double (&xx, x);
    f7_set_double (&zz, z);
    return accbits_f7_t (&xx, &zz, -53 - 1);
}

__attribute__((noinline,noclone))
void exec_fs (const fun_t *fs, uint8_t j, x_t *vz, const x_t *vx, const x_t *vy)
{
    static uint16_t group;
    uint32_t cyc[fs->n_lib];
    int expo = vx->xx.expo;
    x_t vtest;
    
    if (fs->n_args == 1) assert (vy == NULL);
    if (fs->n_args == 2) assert (vy != NULL);

    group++;
//ja = group == 150;
//if (group != 9) return;
//DD ("vx", &vx->xx);
//DD ("vy", &vy->xx);
//if (!ja) return;
//LOG_ON;
    const f_t * const f0   = fs->f[j];
    const f_t * const inv0 = fs->inv[j];
    
    assert (j < fs->n_f);

    for (uint8_t lib = 0; lib < fs->n_lib; lib++)
    {
        const f_t *f   = f0   + lib;
        const f_t *inv = inv0 + lib;
        const uint8_t lid = f->lib->id;
        
        switch (lid)
        {
            default:
                continue;
            
            case F_FLOAT:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->x = f->f_float (vx->x);
                if (fs->n_args == 2) vz->x = f->f_2float (vx->x, vy->x);

                cyc[lid] = avrtest_cycles();
                break;
            }

            case F_F7:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) f->f_f7 (&vz->xx, &vx->xx);
                if (fs->n_args == 2) f->f_2f7 (&vz->xx, &vx->xx, &vy->xx);
                cyc[lid] = avrtest_cycles();
if (ja)DD("vx->xx", & vx->xx);
if (ja)DD("vy->xx", & vy->xx);
if (ja)DD("vz->xx", & vz->xx);
                break;
            }

            case F_U64:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->x64 = f->f_u64 (vx->x64);
                if (fs->n_args == 2) vz->x64 = f->f_2u64 (vx->x64, vy->x64);
                cyc[lid] = avrtest_cycles();
                break;
            }

            case F_FP64:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->fp64 = f->f_fp64 (vx->x64);
                if (fs->n_args == 2) vz->fp64 = f->f_2fp64 (vx->x64, vy->x64);
                cyc[lid] = avrtest_cycles();
                break;
            }

            case F_WRAP7:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->w64 = f->f_wrap7 (vx->x64);
                if (fs->n_args == 2) vz->w64 = f->f_2wrap7 (vx->x64, vy->x64);
                cyc[lid] = avrtest_cycles();
if (ja)LOG_FMT_U64 ("vx->xx = %0x%lx\n", vx->w64);
if (ja)LOG_FMT_U64 ("vy->xx = %0x%lx\n", vy->w64);
if (ja)LOG_FMT_U64 ("vz->xx = %0x%lx\n", vz->w64);
                break;
            }
        } // switch

        float bits = INT8_MAX;
        if (fs->n_args == 1 && inv->f_wrap7 == f->f_wrap7)
            bits = -20;
        else if (fs->n_args == 1 && inv->f_wrap7 != f->f_wrap7)
        {
            if (F_FLOAT == lid)
                bits = accbits_float (vx->x, vtest.x = inv->f_float (vz->x));

            if (F_F7 == lid)
                bits = accbits_f7_t (&vx->xx, (inv->f_f7 (&vtest.xx, &vz->xx), &vtest.xx));

            if (F_U64 == lid)
                bits = accbits_u64 (vx->x64, vtest.x64 = inv->f_u64 (vz->x64));

            if (F_WRAP7 == lid)
                bits = accbits_u64 (vx->x64, vtest.w64 = inv->f_wrap7 (vz->w64));

            if (F_FP64 == lid)
                bits = accbits_u64 (vx->x64, vtest.fp64 = inv->f_fp64 (vz->fp64));
        }

        if (fs->n_args == 2)
        {
            if (F_FLOAT == lid)
                bits = accbits_float (vx->x, vtest.x = inv->f_2float (vz->x, vy->x));

            if (F_F7 == lid)
                bits = accbits_f7_t (&vx->xx, (inv->f_2f7 (&vtest.xx, &vz->xx, &vy->xx), &vtest.xx));

            if (F_U64 == lid)
                bits = accbits_u64 (vx->x64, vtest.x64 = inv->f_2u64 (vz->x64, vy->x64));

            if (F_WRAP7 == lid)
                bits = accbits_u64 (vx->x64, vtest.w64 = inv->f_2wrap7 (vz->w64, vy->x64));

            if (F_FP64 == lid)
                bits = accbits_u64 (vx->x64, vtest.fp64 = inv->f_2fp64 (vz->fp64, vy->x64));
        }

        out_result (fs->lib[lid]->name, fs->f[j]->name, group,
                    cyc[lid], bits, vx->x, vz->x);
    } // for
}

extern float zexp (float) __asm ("exp");
extern float zlog (float) __asm ("log");
extern float zsqrt (float) __asm ("sqrt");
extern float zatan (float) __asm ("atan");
extern float zasin (float) __asm ("asin");
extern float zacos (float) __asm ("acos");
extern float ztan (float) __asm ("tan");
extern float zsin (float) __asm ("sin");
extern float zcos (float) __asm ("cos");

extern uint64_t f_mul (uint64_t, uint64_t) __asm ("f_mult");

float zsquare (float x)
{
    return x * x;
}

uint64_t f_square (uint64_t x)
{
    return f_mul (x, x);
}

uint64_t w_square (uint64_t x)
{
    return w_mul (x, x);
}

#define ARRAY_SIZE(X) (sizeof(X) / sizeof (*(X)))

//extern "C" void f7_atan (f7_t *cc, const f7_t *aa);

const fun_t* get_fs (uint8_t n_args, const f_t *fx, const f_t *invx, uint8_t n_fx)
{
    static fun_t fs;

    memset (&fs, 0, sizeof (fs));

    fs.n_args = n_args;
    
    uint8_t bits = 0;
    for (uint8_t i = 0; i < n_fx; i++)
    {
        const f_t *f = & fx[i];
        uint8_t b = bits;
        bits |= 1 << f->lib->id;
        if (b != bits)
        {
            fs.lib[f->lib->id] = f->lib;
            fs.n_lib++;
        }
    }

    assert (fs.n_lib < ARRAY_SIZE (fs.lib));
    fs.n_f   = n_fx / fs.n_lib;

    for (uint8_t i = 0; i < fs.n_f; i++)
    {
        assert (i < ARRAY_SIZE (fs.f));
        fs.f[i]   = & fx  [i * fs.n_lib];
        fs.inv[i] = & invx[i * fs.n_lib];
    }

    return & fs;
}

static const lib_t LIB_float = { F_FLOAT,   "avr-libc",  &lay_float };
static const lib_t LIB_f7    = { F_F7,      "f7_t",      &lay_f7t };
static const lib_t LIB_wrap7 = { F_WRAP7,   "f7",        &lay_double };
static const lib_t LIB_u64   = { F_U64,     "avr_f64",   &lay_double };
static const lib_t LIB_fp64  = { F_FP64,    "fp64",      &lay_double };


const fun_t* get_fs_f1 ()
{
#ifdef HAVE_FP64LIB
#define DEFF_FP64LIB(N)                                                 \
    { .lib = & LIB_fp64,  .name=#N, .id=ID_##N, .f_fp64  = fp64_##N },
#else
#define DEFF_FP64LIB(N) // empty
#endif

#ifdef HAVE_AVR_F64
#define DEFF_AVR_F64(N)                                                 \
    { .lib = & LIB_u64,   .name=#N, .id=ID_##N, .f_u64   = f_##N  },
#else
#define DEFF_AVR_F64(N) // empty
#endif

#define DEFF(N)                                                          \
        { .lib = & LIB_float, .name=#N, .id=ID_##N, .f_float = z##N   }, \
        { .lib = & LIB_f7,    .name=#N, .id=ID_##N, .f_f7    = f7_##N }, \
        { .lib = & LIB_wrap7, .name=#N, .id=ID_##N, .f_wrap7 = w_##N  }, \
        DEFF_AVR_F64(N)                                                  \
        DEFF_FP64LIB(N)
    static const f_t f1[] = 
    {
        DEFF (sqrt)
        DEFF (log)
        DEFF (exp)
        DEFF (sin)
        DEFF (cos)
        DEFF (tan)
        DEFF (asin)
        DEFF (acos)
        DEFF (atan)
    };

    static const f_t inv1[] = 
    {
/*        DEFF (square)
        DEFF (exp)
        DEFF (log)*/
        DEFF (sin)
        DEFF (cos)
//        DEFF (tan)
    };
    #undef DEFF
    #undef DEFF_FP64LIB
    #undef DEFF_AVR_F64

    return get_fs (1, f1, inv1, ARRAY_SIZE (f1));
}

const fun_t* get_fs_f2 ()
{
    extern float zadd (float, float) __asm ("__addsf3");
    extern float zsub (float, float) __asm ("__subsf3");
    extern float zmul (float, float) __asm ("__mulsf3");
    extern float zdiv (float, float) __asm ("__divsf3");
#ifdef HAVE_FP64LIB
#define DEFF_FP64LIB(N)                                                 \
    { .lib = & LIB_fp64,  .name=#N, .id=ID_##N, .f_2fp64  = fp64_##N },
#else
#define DEFF_FP64LIB(N) // empty
#endif

#ifdef HAVE_AVR_F64
#define DEFF_AVR_F64(N)                                                 \
    { .lib = & LIB_u64,   .name=#N, .id=ID_##N, .f_2u64   = f_##N },
#else
#define DEFF_AVR_F64(N) // empty
#endif

    #define DEFF(N)                                          \
        { .lib = & LIB_float, .name=#N, .id=ID_##N, .f_2float = z##N },  \
        { .lib = & LIB_f7,    .name=#N, .id=ID_##N, .f_2f7    = f7_##N },\
        { .lib = & LIB_wrap7, .name=#N, .id=ID_##N, .f_2wrap7 = w_##N }, \
            DEFF_FP64LIB (N)                                            \
            DEFF_AVR_F64 (N)                                        \
        
    static const f_t f2[] = 
    {
        DEFF (add)
        DEFF (mul)
        DEFF (div)
    };
    static const f_t inv2[] = 
    {
        DEFF (sub)
        DEFF (div)
        DEFF (mul)
    };
    #undef DEFF
    #undef DEFF_FP64LIB
    #undef DEFF_AVR_F64

    return get_fs (2, f2, inv2, ARRAY_SIZE (f2));
}

void test_fs_f1 (int n_tests)
{
    f7_t xx;
    x_t z, x;

    const fun_t *fs = get_fs_f1();

    out_table_header_and_cookies (fs, "f7");

    if (n_tests <= 0) n_tests = 20;

    for (int i = 0; i < n_tests; i++)
    {
        for (uint8_t j = 0; j < fs->n_f; j++)
        {
            uint8_t id = fs->f[j]->id;

            if (i == 0)
                f7_set_s16 (&x.xx, 0);
            else if (i == 1)
                f7_set_s16 (&x.xx, -1);
            else
            {
                f7_rand (& x.xx, -4, 5);
                x.xx.sign = 1 & avrtest_prand();
            }

            if (i > 1 && f7_signbit (&x.xx))
                if (id == ID_log || id == ID_sqrt) // f_f7 == f7_sqrt ||  f_f7 == f7_log)
                    f7_abs (&x.xx, &x.xx);

            if (i > 1 && id == ID_exp)
                {}

            if (id == ID_atan)
                if (x.xx.expo >= 2)
                    x.xx.expo &= 1;

            if (i > 1 && (id == ID_asin || id == ID_acos))
            {
                f7_t one;
                while (f7_cmp_abs (&x.xx, f7_set_u16 (&one, 1)) > 0)
                    x.xx.expo--;
            }

            x.x64 = f7_get_double (&x.xx);
            x.x = f7_get_float (&x.xx);

            exec_fs (fs, j, &z, &x, NULL);
        } // for f[]
    } // for test cases
}

void test_fs_f2 (int n_tests)
{
    f7_t xx, yy;
    x_t z, x, y;

    const fun_t *fs = get_fs_f2();

    out_table_header_and_cookies (fs, "f7");

    if (n_tests <= 0) n_tests = 20;

    for (int i = 0; i < n_tests; i++)
    {
        f7_rand (&xx, -30, 30);
        f7_rand (&yy, -30, 30);
        xx.sign = 1 & avrtest_prand();
        yy.sign = 1 & avrtest_prand();

        if (i == 0) { f7_set_s16 (&xx, 0); f7_set_s16 (&yy, 0); }
        if (i == 1) f7_set_s16 (&yy, 0);
        if (i == 2) f7_set_s16 (&yy, 0);

        x.xx = xx;
        x.x64 = f7_get_double (&xx);
        x.x = f7_get_float (&xx);

        y.xx = yy;
        y.x64 = f7_get_double (&yy);
        y.x = f7_get_float (&yy);

        for (uint8_t j = 0; j < fs->n_f; j++)
        {
            //void (*f_f7)(f7_t*, const f7_t*, const f7_t*) = fs->f[F_F7]->f_2f7;
            uint8_t id = fs->f[j]->id;

            const x_t *mx = &x;
            const x_t *my = &y;
            if (id == ID_add && y.xx.expo > x.xx.expo)
            {
                mx = &y;
                my = &x;
            }

            exec_fs (fs, j, &z, mx, my);
        } // for f[]
    } // for test cases
}


static void str_F64 (uint64_t x, char *str, bool);
static void str_F32 (float x, char *str, bool);
static void str_F7 (const f7_t *aa, char *str, bool);

void run_result (const lib_t *lib, const char *fname, int id, uint32_t cyc,
                 const x_t *x, const x_t *z)
{
    uint8_t c_style = true;
    static char str[40];
    const x_t *w[] = { x, z };
    printf ("%-9s %s %u % 6lu", lib->name, fname, id, cyc);
    
    for (uint8_t c_style = 0; c_style < 2; c_style++)
    {
        if (c_style == 1)
            LOG_STR (" #");
        for (uint8_t i = 0; i < 2; i++)
        {
            const x_t *v = w[i];
            if (lib->layout == &lay_f7t)
                str_F7 (&v->xx, str, c_style);
            else if (lib->layout == &lay_double)
                str_F64 (v->x64, str, c_style);
            else if (lib->layout == &lay_float)
                str_F32 (v->x, str, c_style);
            LOG_FMT_STR (" %s ", str);
        }
    }
    if (lib->layout == &lay_f7t)
        f7_put_CDEF ("x", & x->xx, stdout);
    LOG_STR ("\n");
}

__attribute__((noinline,noclone))
void run_fs (const fun_t *fs, uint8_t j, x_t *vz, const x_t *vx)
{
    static uint16_t group;
    uint32_t cyc;
    
    assert (fs->n_args == 1);

    group++;

//if(group != 19) return;
    const f_t * const f0 = fs->f[j];
    
    assert (j < fs->n_f);

    for (uint8_t lib = 0; lib < fs->n_lib; lib++)
    {
        const f_t *f = f0 + lib;
        const uint8_t lid = f->lib->id;
        
        switch (lid)
        {
            default:
                continue;
            
            case F_FLOAT:
            {
                avrtest_reset_cycles();
                vz->x = f->f_float (vx->x);
                cyc = avrtest_cycles();
                break;
            }

            case F_F7:
            {
                avrtest_reset_cycles();
                f->f_f7 (&vz->xx, &vx->xx);
                cyc = avrtest_cycles();
                break;
            }

            case F_U64:
            {
                avrtest_reset_cycles();
                vz->x64 = f->f_u64 (vx->x64);
                cyc = avrtest_cycles();
                break;
            }

            case F_FP64:
            {
                avrtest_reset_cycles();
                vz->x64 = f->f_fp64 (vx->x64);
                cyc = avrtest_cycles();
                break;
            }

            case F_WRAP7:
            {
                avrtest_reset_cycles();
                vz->x64 = f->f_wrap7 (vx->x64);
                cyc = avrtest_cycles();
                break;
            }
        } // switch

        run_result (f->lib, f->name, group, cyc, vx, vz);
    } // for
}

void run_fs_f1 (int n_tests)
{
    f7_t xx;
    x_t z, x;

    const fun_t *fs = get_fs_f1();

    //out_table_header_and_cookies (fs, "f7");

    if (n_tests < 2) n_tests = 2;

    for (int i = 0; i < n_tests; i++)
    {
        for (uint8_t j = 0; j < fs->n_f; j++)
        {
            LOG_STR("\n");
            uint8_t id = fs->f[j]->id;

            if (i == 0)
                f7_set_s16 (&x.xx, 0);
            else if (i == 1)
                f7_set_s16 (&x.xx, -1);
            else
            {
                f7_rand (& x.xx, -4, 5);
                x.xx.sign = 1 & avrtest_prand();
            }

            if (i > 1 && f7_signbit (&x.xx))
                if (id == ID_log || id == ID_sqrt) // f_f7 == f7_sqrt ||  f_f7 == f7_log)
                    f7_abs (&x.xx, &x.xx);

            if (i > 1 && id == ID_exp)
            {
                int16_t ex = 1 + ((i+2) >> 2);
                if (ex <= 8)
                    f7_set_1pow2 (&x.xx, (i & 2) ? ex : -ex, i & 1);
                if (i == 2)
                    f7_set_1pow2 (&x.xx, 20, 0);
            }

#define F7_CONST_DEF(NAME, FLAGS, M6, M5, M4, M3, M2, M1, M0, EXPO) \
    (f7_t) { .flags = FLAGS, .mant = { M0, M1, M2, M3, M4, M5, M6 }, .expo = EXPO }

            if (i > 1 && id == ID_log)
            {
                if (i == 2)
                    x.xx = F7_CONST_DEF (xx, 0, 0xb5,0x00,0x00,0x00,0x00,0x00,0x00, 0);
                if (i == 3)
                    x.xx = F7_CONST_DEF (xx, 0, 0xb4,0xff,0xff,0xff,0xff,0xff,0xff, 0);
                if (i == 4)
                    x.xx = F7_CONST_DEF (X, 0,	0xb5,0x04,0xf3,0x33,0xf9,0xde,0x65, 0);
            }

            if (id == ID_atan)
            {
                if (x.xx.expo >= 3)
                    x.xx.expo &= 1;
                if (i == 2)
                    x.xx = F7_CONST_DEF (big_atan, 0, 0x88,0x4e,0x46,0x93,0x3c,0x66,0x80, 2);
            }

            if (id == ID_sin || id == ID_cos)
            {
                if (i >= 2 && i <= 3)
                {
                    f7_const (&x.xx, pi);
                    x.xx.expo--;
                    x.xx.mant[0] = 0;
                    x.xx.mant[1] = 0;
                    x.xx.mant[2] = 0;
                    x.xx.mant[3] += (i & 1) ? 1 : -1;
                }
                else if (i >= 4 && i <= 6)
                {
                    f7_set_u16 (&x.xx, 355);
                    x.xx.expo -= i - 4;
                    x.xx.flags = i & 1;
                }
                else if (i == 7)
                    f7_set_u16 (&x.xx, 22);
                else if (i == 8)
                {
                    f7_const (&x.xx, pi);
                    x.xx.mant[0] = 0;
                    x.xx.mant[1] = 0;
                    x.xx.mant[2] = 0;
                    x.xx.mant[3] = 0;
                    x.xx.mant[4] ++;
                }
            }

            if (i > 1 && (id == ID_asin || id == ID_acos))
            {
                f7_t one;
                while (f7_cmp_abs (&x.xx, f7_set_u16 (&one, 1)) > 0)
                    x.xx.expo--;

                if (i >= 2 && i <= 3)
                {
                    f7_set_u16 (&x.xx, 255);
                    x.xx.expo -= 8;
                    x.xx.sign = i & 1;
                }
                else if (i >= 4 && i <= 5)
                    f7_set_1pow2 (&x.xx, -1, i & 1);
                else if (i == 6)
                    x.xx = F7_CONST_DEF (big_asin_fp64, 0, 0xdd,0x7e,0xe6,0xf4,0x15,0x28,0xe8, -1);
                else if (i == 7)
                    x.xx = F7_CONST_DEF (big_acos_fp64, 0, 0x85,0xc0,0x22,0x0e,0x2f,0xda,0xd8, -1);
            }
#undef F7_CONST_DEF

            x.x64 = f7_get_double (&x.xx);
            x.x = f7_get_float (&x.xx);

            run_fs (fs, j, &z, &x);
        } // for f[]
    } // for test cases
}

//F7_CONST_DEF (big_acos,	0,	0x85,0xc0,0x22,0x0e,0x2f,0xda,0xd8,	-1)
//F7_CONST_DEF (big_asin,	0,	0xdd,0x7e,0xe6,0xf4,0x15,0x28,0xe8,	-1)
//F7_CONST_DEF (big_atan,	0,	0x88,0x4e,0x46,0x93,0x3c,0x66,0x80,	2)

void hunt_delta (uint16_t n)
{
    x_t x, y;

    f7_t d_func, x_func, d;
    f7_clr (&d_func);
/*    
    do {
        f7_rand (&x.xx, -5, 7);
        x.xx.mant[0] &= 0xf8;
        x.x64 = f7_get_double (&x.xx);
        f7_atan (&y.xx, &x.xx);
        y.x64 = f_atan (x.x64);
        f7_set_double (&d, y.x64);
        f7_Isub (&d, &y.xx);
        d.sign = 0;
        
        if (f7_gt (&d, &d_func))
        {
            f7_copy (&d_func, &d);
            f7_copy (&x_func, &x.xx);
            LOG_FMT_FLOAT ("BIG func D=%g:", f7_get_float (&d));
            f7_dump (&x.xx);
            f7_put_CDEF ("big_atan", &x.xx, stdout);
        }
        
    } while (--n);
*/
}


void test ()
{
    f7_t xx, ff, exx, enxx, nn;
    avrtest_reset_all();
    PERF_START (1);
    f7_exp (&ff, &xx);
    PERF_STOP (1);
    uint32_t ticks = avrtest_cycles();
    LOG_FMT_U32 ("Cycles = %u\n", ticks);
    print_ticks_line ("libf7", "fun", 1, ticks, xx.expo, 1.23, 0);
    print_ticks_line ("libf7", "fun", 2, ticks- 1000, xx.expo, 1.23, 0);
    print_ticks_line ("libf7", "fun", 3, ticks- 3000, xx.expo, 1.23, 0);
//    PERF_DUMP_ALL;
}

static char hexdigit (uint8_t x)
{
    return x < 10 ? '0' + x : x - 10 + 'a';
}

char* toHex (uint64_t x, char *str)
{
    *str++ = '0';
    *str++ = 'x';
    char *s = str;
    do
    {
        *str++ = hexdigit (x & 0xf);
        x >>= 4;
    } while (x);
    *str = '\0';
    strrev (s);
    return str;
}


static inline __attribute__((always_inline))
void get_floating (float_t*, uint64_t, const layout_t*);
static void get_floating_F7 (float_t*, const f7_t*);
static void str_floating (float_t*, char*, bool);

void str_F64 (uint64_t x, char *str, bool c_style)
{
    float_t f;
    get_floating (&f, x, &lay_double);
    str_floating (&f, str, c_style);
}

void str_F32 (float x, char *str, bool c_style)
{
    float_t f;
    uint32_t u;
    __builtin_memcpy (&u, &x, 4);
    get_floating (&f, u, &lay_float);
    str_floating (&f, str, c_style);
}

void str_F7 (const f7_t *aa, char *str, bool c_style)
{
    float_t f;
    get_floating_F7 (&f, aa);
    str_floating (&f, str, c_style);
}

void str_floating (float_t *f, char *str, bool c_style)
{
    if (c_style)
    {
        if (f7_class_nan (f->flags))
            strcpy (str, "NaN");
        else if (f7_class_inf (f->flags))
            strcpy (str, (F7_FLAG_sign & f->flags) ? "-Inf" : "+Inf");
        else
        {
            if (f7_class_sign (f->flags))
                *str++ = '-';
            str = toHex (f->mant, str);
            *str++ = 'p';
            itoa (f->expo, str, 10);
        }
        return;
    }

    *str++ = 'F';
    utoa (f->dig_mant, str, 10);
    str += strlen (str);
    *str++ = '(';
    if (f7_class_nan (f->flags))
        strcpy (str, "NaN");
    else if (f7_class_inf (f->flags))
        strcpy (str, (F7_FLAG_sign & f->flags) ? "mInf" : "pInf");
    else
    {
        *str++ = (1 & f->flags) + '0';
        *str++ = ',';
        str = toHex (f->mant, str);
        *str++ = ',';
        itoa (f->expo, str, 10);
    }
    strcat (str, ")");
}

void get_floating (float_t *f, uint64_t x, const layout_t *lay)
{
    f->dig_mant = 1 + lay->dig_mant;
    f->flags = 1 & (x >> (lay->dig_exp + lay->dig_mant));
    f->expo = x >> lay->dig_mant;
    f->expo &= lay->max_exp;
    f->mant = x & ((1ull << lay->dig_mant) - 1);
    if (f->expo == lay->max_exp)
    {
        f->flags = f->mant ? F7_FLAG_nan : (f->flags | F7_FLAG_inf);
        return;
    }
    else if (f->expo == 0)
    {
        if (f->mant == 0)
            return;
        // Sub-normal.
        f->expo++;
    }
    else
    {
        // Normal.
        f->mant |= 1ull << lay->dig_mant;
    }

    // Align so that the string representation allows for easy comparison
    // across different types (float, double, f7_t).
    if (lay->dig_mant == 52)
    {
        f->mant <<= 4;
        f->expo -= 4;
    }
    if (lay->dig_mant == 23)
    {
        f->mant <<= 33;
        f->expo -= 33;
    }
    f->expo -= lay->exp_bias + lay->dig_mant;
}

void get_floating_F7 (float_t *f, const f7_t *aa)
{
    __builtin_memset (f, 0, sizeof (float_t));
    f->dig_mant = F7_MANT_BITS;
    f->flags = f7_classify (aa);
    if (!f7_class_number (f->flags))
        return;
    if (f7_class_zero (f->flags))
    {
        f->flags &= F7_FLAG_sign;
        return;
    }
    f->expo = aa->expo;
    __builtin_memcpy (&f->mant, aa->mant, 8);
    f->mant &= 0xffffffffffffff;
    // Align so that the string representation allows for easy comparison
    // across different types (float, double, f7_t).
    {
        f->mant <<= 1;
        f->expo--;
    }
    f->expo -= F7_MANT_BITS - 1;
}

void run ()
{
/*
    f7_double x = 0.5;
    f7_double y = 0.1;
    printf ("x = %f\n", (double) asin (x+y));
*/
    run_fs_f1 (10);
}

int main()
{
    run();
}
