from __future__ import print_function

__all__ = ['MiniMax']

import math
import sys

from poly import Poly
from decimal import Decimal
from matrix import *


def _flush():
	sys.stdout.flush()

def _abs (x):
	return x.copy_abs() if type(x) is Decimal else abs(x)

class Group (object):
	""" Value / delta pairs for which we search maximum delta."""
	
	ID = 0

	def __init__ (self, xy, sign, f, a, b):
		if not type(xy) is list:
			raise TypeError (xy)
		if not sign in (-1,1):
			raise ValueError (sign)
		
		for (_,y) in xy:
			if sign == -1 and y > 0:
				raise ValueError ((x,y),xy)
			if sign == 1 and y < 0:
				raise ValueError ((x,y),xy)

		Group.ID += 1
		self.id = Group.ID
		self.f = f
		self.a = a
		self.b = b
		self.sign = sign
		self.themax = None
		if len(xy) == 2:
			(x0,y0),(x2,y2) = xy[0],xy[1]
			x1 = (x0 + x2) / 2
			y1 = f(x1)
			self.xy = [(x0,y0), (x1,y1), (x2,y2)]
		else:
			self.xy = xy
		if sign == -1:
			for (_,y) in xy: assert y <= 0
			self.xy = [ (x,_abs(y)) for (x,y) in xy ]
		else:
			for (_,y) in xy: assert y >= 0

	def __repr__ (self):
		return "id=%d, #=%d, sign=%s: max=%s, %s" % (self.id, len(self.xy),
				self.sign, self.max, self.xy)

	def get_max (self):
		""" Return a pair of triples (k, x[k], y[k]) such that y[k] is
			minimal / maximal amongst all ys. """
		if self.themax is None:
			for k in range(len(self.xy)):
				(x,y) = self.xy[k]
				if self.themax is None or y > self.themax[2]:
					self.themax = (k, x, y)
		return self.themax

	max = property (lambda self: self.get_max()[2], None, None, "")

	def improve_max (self, dx=1e-8):
		""" Try to find an even greater maximum.
			dx:  The new x-coordinate must be at least dx away from
				 existing x's. """

		(k,x1,y1) = self.get_max()
		self.themax = None
		if k > 0 and k < len(self.xy) - 1 and len(self.xy) >= 3:
			#print ("improve %s: %s -> %s" % (k, x1, y1))
			(x0,y0) = self.xy[k-1]
			(x2,y2) = self.xy[k+1]
			assert x0 < x1 and x1 < x2
			#print ("...: %s -> %s" % (x0, y0))
			#print ("...: %s -> %s" % (x2, y2))
			A = Matrix.powers (2, [x0,x1,x2])
			#print ("Apowers=", A)
			# Find a parabola q() through the 3 points Pn = (xn, yn), n = 0..2.
			q = A.solve (Vector([y0,y1,y2]), way=Matrix.USE_QR)
			q = Poly(q._a)
			#print ("q=", q)
			# Solve q' = 0 to find the extremum of q.
			dq = q.deriv()
			xm = -dq[0] / dq[1]
			y = self.f (xm)
			ym = _abs (y)
			#print ("xm=", xm, ym)
			if (self.sign == -1 and y > 0) or (self.sign == 1 and y < 0):
				print ("Strange things happen to sign=%d y:", self.sign, y, y0, y1, y2)
			elif ym > y1:
				if _abs(xm-x1) < dx:
					if _abs(xm-x0) < dx or _abs(xm-x2) < dx:
						print ("Too close to (%s,%s,%s): xm=%s" % (x0,x1,x2, xm))
					elif _abs(xm-x0) > dx and _abs(xm-x2) > dx:
						print ("Replacing %s: xm=%s" % (x1, xm))
						self.xy[k] = (xm,ym)
				elif xm < x0 or xm > x2:
					print ("Strange things happen to x: ", xm, x0, x1, x2)
				else:
					#print ("Improved by %s, dx=%s" % (ym - y1, xm-x1))
					self.xy.insert (k+1 if xm > x1 else k, (xm,ym))
					#print (self)
					return True
		#print ("")
		return False

import operator

class MiniMaxGroups (object):
	""" Basically, a list of Group objects that in their entirety represent..."""
	def __init__ (self, n_groups, f, a, b, steps=280):
		"""
		"""
		(self.a,self.b) = (a,b)
		self.f = f
		step = (self.b - self.a) / steps
		self.n_groups = n_groups
		self.groups = []

		sign = 0
		# Split x[] in such a way that their deltas have the same sign.
		xy = []
		for j in range(1 + steps):
			x = self.a + step * j
			y = self.f(x)
			#print ("delta(x%d=%s) =" % (j,x), y)

			y_sign = 1 if y > 0 else -1 if y < 0 else 0
			assert y_sign * y >= 0
			if y_sign == 0 or y_sign == sign or sign == 0:
				if y_sign != 0:
					sign = y_sign
			else:
				if len(xy):
					#print ("done group:", xy)
					self.addGroup (sign, xy)
					xy = []
				sign = y_sign
			xy += [ (x,y) ]

		if len(xy):
			#print ("done group:", xy)
			self.addGroup (sign, xy)
		
		if len(self.groups) != self.n_groups:
			raise ValueError (n_groups, len(self.groups))

	def addGroup (self, sign, xy):
		self.groups += [ Group (xy, sign, self.f, self.a, self.b) ]

	def quality (self):
		ma = reduce (lambda g,h : g if g.max > h.max else h, self.groups)
		mi = reduce (lambda g,h : g if g.max < h.max else h, self.groups)
		(s,p) = (0,1)
		for g in self.groups:
			s += g.max
			p *= g.max
		print ("...min=", mi.max)
		print ("...max=", ma.max)
		print ("...diff:  ", ma.max - mi.max)
		print ("...quot:  ", ma.max / mi.max)
		a_mean = s / self.n_groups
		g_mean = p ** (p**0 / self.n_groups)
		print ("...a-mean:", a_mean)
		print ("...g-mean:", g_mean)
		return (ma.max, a_mean, g_mean)


	def max_errors (self):
		for g in self.groups:
			#print ("group[max@%s: %s->%s]=" % g.max(),g)
			while g.improve_max (dx=1e-8):
				pass
		x_max_error = []
		deltas = []
		for g in self.groups:
			(k,x,y) = g.get_max()
			x_max_error += [x]
			deltas += [y]
			print ("max(#%d): %s" % (len(g.xy), y * g.sign))
		#print ("x[] = ", x_max_error)
		return (x_max_error, deltas)


def ww(x):
	return 1
	

def MiniMax (n, f, a, b, relative=True, T=float, x_max=None):

	best = None
	best_max = None

	for tries in range(7):
		if tries == 0:
			if x_max is None:
				# n+2 values
				x = Poly.Tn_extrema_x (n+1, a=a, b=b)
			else:
				x = map (lambda x: T(x), x_max)
		print ("x[] =", x)
		y = map (lambda x: f(x, n_tries=tries), x)
		print ("y[] =", y)

		one = get_1 (y[0])
		alt = one
		fx = Vector(y)

		E = []
		for fi in fx:
			E += [ (alt * fi) if relative else alt ]
			alt = -alt
		
		P = Matrix.powers (n, x)
		E = Matrix(cols=[Vector(E)])
		EP = E.add_columns (P)
		print ("E|P = %d x %d" % EP._dim)
		_flush()
		ep = EP.solve (fx, way=Matrix.USE_QR)
		print ("e,p =", ep)

		E = ep[0]
		p = Poly (ep[1:])
		print ("E,p =", E, " , ", p)
		print ("p(x) =", p.toString (Poly.GNUPLOT))
		print ("\tp =", p.toString(style=Poly.DECIMAL))
		#p.cdef()
		#print (p.toString(style=Poly.IEEE_double, var="p"))

		def delta (x):
			y = f(x,n_tries=tries)
			dy = y - p (x)
			return ww(x) * (dy if not relative else (dy / y))

		G = MiniMaxGroups (n+2, delta, a, b)
		(x,deltas) = G.max_errors()
		(max_d, a_mean, g_mean) = G.quality()

		print ("deltas:", deltas)
		best_max = max_d if best_max is None else min (max_d, best_max)

		if best is None or max_d < best[0]:
			best = (max_d, a_mean, g_mean, p, x)
			print ("p(x) =",p.toString(style=Poly.GNUPLOT))
			print ("\tp =", p.toString(style=Poly.DECIMAL))
			p.cdef()
			print (p.toString(style=Poly.IEEE_double, var="p"))
		else:
			print ("---No improvement!")

	return (best[3], best[4])


tries = 0
	

def MiniMaxRational (m, n, f, a, b, relative=True, T=float, x_max=None):
	if not relative:
		raise Execptin ("todo")

	E_old = 0 #T(-5E-12)
	q_old = Poly(1)

	best = None
	best_max = None

	#E_fix = T(-2.8e-12)
	E_fix = None

	global tries
	for tries in range(4):
		if tries == 0:
			if x_max is None:
				# m+n+2 values
				x = Poly.Tn_extrema_x (m+n+1, a=a, b=b)
				#if T is Decimal:
				#	x = map (lambda x: (b*x).sqrt() if x > 0 else x, x)
				#else:
				#	x = map (lambda x: x**0.5 if x > 0 else x, x)
			else:
				x = map (lambda x: T(x), x_max)
		print ("x[] =", x)

		y = map (lambda x: f(x, n_tries=tries), x)
		print ("y[] =", y)

		one = get_1 (y[0])
		alt = one
		fx = Vector(y)

		# Try to solve (p(x)/q(x) - f(x)) / f(x) = (-1)^k * E  for x in x[]
		# which is m+n+2 equations for m+1 + n+1 + 1 unknowns.  Normalize
		# q so that q(0) = 1 reduces to m+n+2 unknowns.  Let fk = f * (-1)^k
		# <=>  p - q * f  =  q * E * fk = r.s.
		# We split the right side into
		# r.s. = (t * q * E + (1-t) * q * E) * fk and then in one summand
		# we take q = q_old, in the other one E = E_old so that
		# r.s. = (t * q_old * E + (1-t) * q * E_old) * fk.
		# Now use that q(0) = 1 and with q' = q-1 we have:
		# <=>  p - q' * f - f = (t * q'_old * E + tE + (1-t) * q' * E_old + (1-t)*E_old) * fk
		# <~>  p - q' * f - f = (t * q'_old * E +  E + (1-t) * q' * E_old) * fk
		# <=>  p - q' * (f + (1-t) * E_old * fk) - E * (1-t + t*q_old) * fk = f
		Q = Matrix.powers (n, x, from_expo=1)
		P = Matrix.powers (m, x)
		#print ("Q =", Q)
		if E_fix is None:
			E = []
			t = T(Decimal("0.0"))
			for k in range(len(x)):
				alt = -alt
				fk = fx[k] * alt
				E += [ Vector ([-fk * (1-t + t*q_old(x[k]))]) ]
				Q[k] *= -(fx[k] + (1-t) * E_old * fk)
			E = Matrix(rows=E)
		#print ("E =", E)
			EQP = E.add_columns (Q).add_columns (P)
			print ("E|Q-1|P = %d x %d" % EQP._dim)
			_flush()
			eqp = EQP.solve (fx, way=Matrix.USE_QR)
			print ("E^t=",E.t)
			E = eqp[0]
			q = (Poly (eqp[1:1+n]) << 1) + 1
			p = Poly (eqp[1+n:])
		else:
			print ("E_fix=", E_fix)
			B = []
			for k in range(len(x)):
				alt = -alt
				fk = fx[k] * (1 + alt * E_fix)
				B += [ fk ]
				Q[k] *= -fk
			print ("Q-1 =", Q)
			QP = Q.add_columns (P)
			B = Vector(B)
			print ("A=Q-1|P = %d x %d" % QP._dim)
			print ("A=Q-1|P =", QP)
			AtA = QP.t * QP
			print ("A^t * A =", AtA)
			print ("b=", B)
			AtB = QP.t * B
			print ("A^t * b =", AtB)
			_flush()
			qp = AtA.solve (AtB, way=Matrix.USE_QR)
			print ("qp =", qp)
			q = (Poly (qp[0:n]) << 1) + 1
			p = Poly (qp[n:])
			E = E_fix

		def delta (x):
			global tries
			y = f(x, n_tries=tries)
			dy = y - p (x) / q (x)
			
			return dy if not relative else (dy / y)

		try:
			G = MiniMaxGroups (n+m+2, delta, a, b)
		except ValueError as e:
			print ("p(x) =",p.toString(style=Poly.GNUPLOT))
			print ("q(x) =",q.toString(style=Poly.GNUPLOT))
			print ("\tp =",p.toString(style=Poly.DECIMAL))
			print ("\tq =",q.toString(style=Poly.DECIMAL))
			raise e
			#return (p,q,x)
		(x,deltas) = G.max_errors()
		(max_d, a_mean, g_mean) = G.quality()

		best_max = max_d if best_max is None else min (max_d, best_max)
		
		#if E_old == 0: E_old = max_d
		#if max_d < E_old: E_old = T(max_d)# = min (E_old
		#E_old = T (g_mean + best_max) / 2 # dd
		#E_old = T("0.9") * best_max
		E_old = E
		if E_old * E < 0:
			E_old = -E_old

		print ("E_old :=",E_old)
		q_old = q
		#q_old = Poly(1)
		
		if best is None or max_d < best[0]:
			if not best is None:
				d = best[0] - max_d
				print ("+++ Improved %s = %s%%" % (d, 100*d / best[0]))
			best = (max_d, a_mean, g_mean, p, q, x)
			print ("p(x) =",p.toString(style=Poly.GNUPLOT))
			print ("q(x) =",q.toString(style=Poly.GNUPLOT))
			print ("\tp =", p.toString(style=Poly.DECIMAL))
			print ("\tq =", q.toString(style=Poly.DECIMAL))
			p.cdef()
			q.cdef()
			print (p.toString(style=Poly.IEEE_double, var="p"))
			print (q.toString(style=Poly.IEEE_double, var="q"))
		else:
			print ("---No improvement!")

		# print ("deltas:", deltas)
	return (best[3], best[4], best[5])
