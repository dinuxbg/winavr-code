import math
import re
from fractions import Fraction as Frac
from fractions import gcd as gcd
import decimal
from decimal import Decimal as Decimal
from numbers import Integral

def fak (n, s = 1):
	""" s = 1  -->  factorial
		s = 2  -->  double factorial  etc. """
	if s == 1:
		return math.factorial (n)
	# if n < 0 we return the empty product (1).
	f = 1
	for k in xrange (n, 0, -s):
		f *= k
	return f

def binom (n, k):
	if k > n or n < 0 or k < 0:
		raise ValueError (n, k)
	zz = fak (n)
	nn = fak (k) * fak (n-k)
	rr = Frac (zz, nn)
	if rr.denominator != 1:
		raise ValueError (n, k)
	return rr.numerator


def ld (x):
	""" log_2 (|x|) """
	if x == 0:
		return -10000
	return math.log (abs (float (x))) / math.log(2)

def isScalar (x):
	return isinstance (x, Integral) or type(x) in (Frac, Decimal)

def lcm (a, b):
	return a * b / gcd (a, b)

def exact_sqrt (n):
	if type(n) is Frac:
		a = exact_sqrt (n.numerator)
		b = exact_sqrt (n.denominator)
		return -1 if a < 0 or b < 0 else Frac(a,b)
	q = int(math.sqrt(n))
	if n == q**2: return q
	if n == (q+1)**2: return q+1
	if n == (q-1)**2: return q-1
	return -1

def get_1 (obj):
	if type(obj) is Decimal:
		return Decimal(1)
	return obj ** 0

def cdef (x, n_bytes = 7):
	""" Return a string representation of x that can be included by
		libf7's C/C++ code. It represents a f7_t object as F7_CONST_DEF like:
		... """

	if type(x) is Decimal and x.is_nan():
		s = "0," * n_bytes + " 0)"
		flags = 1 << 2
		return "F7_CONST_DEF (NaN, 0x%02x,\t" % flags + s

	name = "X"
	pm = "p" if x >= 0 else "m"

	if type(x) == Frac:
		zz = x.numerator
		nn = x.denominator
		name = "%s%s" % (pm, abs (zz))
		if nn != 1:
			name += "_%s" % nn
		x = Decimal (zz) / nn
	elif isinstance (x, Integral):
		name = "%s%d" % (pm, abs (x))
		x = Decimal(x)
	else:
		x = Decimal(x)

	ax = x.copy_abs()
	if ax.is_infinite():
		s = "0," * n_bytes + " 0)"
		flags = 0x80 | (0 if x >= 0 else 1)
		return "F7_CONST_DEF (%sInf, 0x%02x,\t" % (pm, flags) + s

	if ax == 0:
		s = "0," * n_bytes + " 0)"
		return "F7_CONST_DEF (Zero, 0x0,\t" + s
	expo = -1
	while ax < Decimal ("0.5"):
		expo -= 1
		ax *= 2
	while ax > 1:
		expo += 1
		ax /= 2
	s = ""
	#bx = ax
	#bx *= 256
	#print ("ax=",bx)
	#print ("ax=",(bx).to_integral_value (decimal.ROUND_FLOOR))
	ax += Decimal ("2") ** -(1 + 8 * n_bytes)
	if ax >= 1:
		expo += 1
		ax /= 2
	for i in range (n_bytes):
		ax *= 256
		ix = ax.to_integral_value (decimal.ROUND_FLOOR)
		ax -= ix
		if i == 7:
			s += "/**/"
		s = s + "0x%02x," % ix
	s += " %d)" % expo
	return "F7_CONST_DEF (%s, %d,\t" % (name, 0 if x >= 0 else 1) + s

def long_from_hex (str):
	str = str.replace ("0x", "")
	str = re.sub (r"\s+", "", str)
	return long (str, base=16)

def long_from_bin (str):
	str = re.sub (r"\s+", "", str)
	return long (str, base=2)

def dec_from_f7 (str):
	xx = r"\s*0x([a-fA-F0-9]{1,2})\s*,\s*"
	dd = r"(-?\d+)"
	pat = re.compile (r"F7_CONST_DEF\s*\(.+,\s*([01])\s*," + (xx * 7) + dd + r"\s*\)")
	m = pat.match (str.strip())
	if not m:
		raise ValueError ("unrecignized f7", str);
	mant = Decimal(0)
	for i in range(2, 2+7):
		mant = 256 * mant + long(m.group(i), base=16)
	expo = int(m.group(9)) - 55
	if expo > 0:
		mant *= 2 ** expo
	else:
		mant /= 2 ** -expo
	return -mant if m.group(1) == "1" else mant

def raw_IEEE754 (x, bits_mant, bits_expo):
	""" Return binary representation (as int) of the
		IEEE-754 equivalent of x.  bits_mant includes the leading 1. """

	prec10 = 5 + int (bits_mant / ld(10))
	decimal.getcontext().prec = max (decimal.getcontext().prec, prec10)
	x = Decimal(x)

	max_expo = 2 ** bits_expo - 1
	bias_expo = max_expo >> 1

	n_bits = 1 + bits_expo + bits_mant - 1
	n_nibbles = (3 + n_bits) >> 2
	fmt = "0x%%0%dx" % n_nibbles

	if x == 0:
		return 0

	sign = 0
	if x.is_nan():
		# Nan is not unique.  avr-libc uses 0x80... as mantissa and
		# so does libf7.
		expo_biased = max_expo
		mant = 1 << (bits_mant - 2)
	elif x < 0:
		sign = 1
		x = x.copy_negate()
	#print (DecimalTuplex))
	#print dir (Decimal)
	if x.is_infinite():
		expo_biased = max_expo
		mant = 0
	elif not x.is_nan():
		two = Decimal(2)
		one = Decimal(1)
		expo = int(x.ln() / two.ln())
		x *= two ** -expo
		if x >= two:
			expo += 1
			x /= two
		elif x < one:
			expo -= 1
			x *= two
		#print (x, expo)
		# Round (and normalize again).
		x += two ** -bits_mant
		if x >= two:
			expo += 1
			x /= two

		if x < 1 or x >= 2:
			raise Exception ("fatal", x)
		mant = (x * two ** (bits_mant-1)).to_integral_value(decimal.ROUND_DOWN)
		mant = int (mant)
		expo_biased = expo + bias_expo
		mask = (1 << (bits_mant - 1)) - 1
		if expo_biased < 1:
			# Subnormal
			# expo_biased = 0 encodes expo_biased of 1, hence add 1 to shift offset.
			mant >>= 1 - expo_biased
			expo_biased = 0
			if 0 != (mant & ~mask):
				raise Exception ("~mask must not overlap mant")
		elif expo_biased >= max_expo:
			# Overflow -> Inf
			expo_biased = max_expo
			mant = 0
		else:
			# Remove redundant "1." from mantissa.
			if mask + 1 != (mant & ~mask):
				raise Exception ("mask+1 should overlap mant in exactly 1 bit")
			mant &= mant
	dval = mant
	dval |= expo_biased << (bits_mant - 1)
	dval |= sign << (bits_mant - 1 + bits_expo)
	return dval

def raw_double (x):
	return raw_IEEE754 (x, bits_mant=53, bits_expo=11)

def raw_float (x):
	return raw_IEEE754 (x, bits_mant=24, bits_expo=8)

def make_lookup (x, y, fmt):
	d = {
		"xd" : raw_double (x),
		"yd" : raw_double (y),
		"xf" : raw_float (x),
		"yf" : raw_float (y),
		"xc" : cdef(x),
		"yc" : cdef(y)
	}
	print fmt % d
	
import re

def revert_cdef ():
	""" Only used once in re-factoring mant bytes of F7_CONST_DEF. """
	fin = open ("libf7-const.def")
	ex_id = "([0-9a-zA-Z_]+)"
	ex_num = "(-?[0-9a-fA-Fx]+)"
	#ex_hex = "([0-9x]+)"
	ex_sep = ",\\s*"
	ex = "F7_CONST_DEF \\(" + ex_id + (9 * (ex_sep + ex_num)) + "\\)"
	#print "ex =", ex
	pat = re.compile (ex)
	for line in fin.readlines():
		line = line.strip ('\n')
		m = pat.match (line)
		if not m:
			if line.startswith ("F7_CONST_DEF"):
				raise Exception (line)
			print line
		else:
			s = "F7_CONST_DEF (" + m.group(1) + ",\t" + m.group(2) + ",\t"
			for k in range (7):
				s += m.group (9 - k) + ","
			s += "\t" + m.group (10) + ")"
			print (s)
	fin.close()
