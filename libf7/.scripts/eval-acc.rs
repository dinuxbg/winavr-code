# rscript --vanilla eval-test.rs

bits2bytes <- function (n) { max (1, (n+7) %/% 8) }

gmean <- function (x) { exp (mean (log(x))) }

bytestat <- function (x, col)
{
    b1 = x[(x$Bytes==1),][col];
    b2 = x[(x$Bytes==2),][col];
    b3 = x[(x$Bytes==3),][col];
    b4 = x[(x$Bytes==4),][col];
    c (x[col], b1, b2, b3, b4)
}

D <- function (x, y)
{
    print (paste ("---", x, ": "))
    print (y)
}

###########################################

# For auto line-wrap position in printing.
options (width=130)
# options (show.eror.locations = TRUE) is void?

func = "test-f7"

file_name = paste (func, ".data", sep="")
plot_name = paste (func, ".png", sep="")

x <- read.table (file_name, header=TRUE)

# In order to supply a specific order for levels(x$lib).
# Can be set by Cookie[-1].
nspaces = NA

# In order to supply a specific order for levels(x$fun).
# Can be set by Cookie[-2].
fnames = NA

# Like NA or "avr-libc", gains are relative to this lib.
# Can be set by Cookie[-3].
ticks_reference = "f7"

while (x$id[1] < 0) {
    D (paste ("Cookie [", x$id[1], "]: x[1,1] ="), x[1,1])
    # Need as.character() to transform from a "levels" to a plain string.
    cookie <- as.character (x[1,1])
    if (x$id[1] != -3)
        cookie <- eval (parse (text = cookie))
    switch (-x$id[1],
            cookie -> nspaces,
            cookie -> fnames,
            cookie -> ticks_reference
    )
    # Delete the row that shipped the cookie.
    x <- x[-1, ]
}

# Ditch now unused factors (from cookies).
x$lib <- factor (x$lib)
x$fun <- factor (x$fun)

if (length (nspaces) == 1 && is.na (nspaces))
    nspaces = levels (x$lib)
"nspaces:"
nspaces


if (length (fnames) == 1 && is.na (fnames))
    fnames = levels (x$fun)
"fnames:"
fnames

"ticks_reference:"
ticks_reference

# Make sure columns are named as expected
colnames(x)[1] = "lib"		# Namespace for function like "avr-libc"
colnames(x)[2] = "fun"		# Function like "exp".
colnames(x)[3] = "id"		# Same for same x (= avrtest_prand()).
colnames(x)[4] = "ticks"	# Cycles for fun(x)
#colnames(x)[5] = "expo"		
#colnames(x)[6] = "x"
#colnames(x)[7] = "fx"

gains_relative_p = 0

log10_of_ticks = 0

log2_of_gains = 1


if (log10_of_ticks == 1) {
    col_ticks = "log10.ticks";
    x$log10.ticks <- log (x$ticks) / log (10)
    fun_suffix = "log10 (ticks)"
} else {
    col_ticks = "ticks";
    fun_suffix = "ticks"
}


if (!is.na (ticks_reference)) {
    ref_suffix = ticks_reference
} else {
    ref_suffix = "average"
}

if (log2_of_gains == 1) {
    col_gains = "log2.gain.rel"
    gain_suffix = paste ("log_2 (ticks / ", ref_suffix, ")")
} else if (gains_relative_p == 1) {
    col_gains = "gain.rel"
    gain_suffix = paste ("ticks / ", ref_suffix)
} else {
    col_gains = "gain.abs"
    gain_suffix = paste ("ticks - ", ref_suffix)
}

# subset: lines not matching (NA) are still present.
# filter: lines not matching (NA) are dropped.
# filter (data, cond1, cond2, ..)   === cond1 & cond 2 & ...

#x$fun == "log"


ident <- function (x) { x }

for (id in levels (factor (x$id))) {
    tickref.mean  <- sapply (x[x$id == id, ]["ticks"], mean)
    tickref.gmean <- sapply (x[x$id == id, ]["ticks"], gmean)

    if (!is.na (ticks_reference))
        # FIXME: This is just one value, why we have to wrap it in dummy function?
        tickref.lib <- sapply (x[x$id == id & x$lib == ticks_reference, ]["ticks"], ident)

    for (lib in nspaces) {
        x$ref.gmean[x$id == id & x$lib == lib] <- tickref.gmean
        x$ref.mean [x$id == id & x$lib == lib] <- tickref.mean
        if (exists ("tickref.lib")) {
            x$ref.gmean[x$id == id & x$lib == lib] <- tickref.lib
            x$ref.mean [x$id == id & x$lib == lib] <- tickref.lib
        }
    }
}

x$gain.abs <- x$ticks - x$ref.mean

x$gain.rel <- x$ticks / x$ref.gmean

x$log2.gain.rel <- log (x$gain.rel) / log(2)

###########################################


lim_ticks <- c (min (x[col_ticks]), max (x[col_ticks]))
#lim_ticks

lim_gains <- c (max (-5, min (x[col_gains])), max (x[col_gains]))
#lim_gains

lim_accs <- c (min (x$acc), max (x$acc))
"lim_accs:"
lim_accs


#x$Gain     <- x$New - x$Old
#x$"Gain %" <- 100 * x$Gain / x$Old


n_namespaces = length (nspaces)

# Set labels below table ("libf7", "avr-libc", ...)
xlabels = nspaces

#hist (x$x)
#Sys.sleep(1)


n_funcs = length (fnames)
n_samples = length (factor (x$id))
n_libs = length (nspaces)

#tick_max = max (x$ticks)
#tick_min = min (x$ticks)

red1  = rep ("#ff8888", times=n_libs)
red   = rep ("#ffbbbb", times=n_libs)
green1 = rep ("#88ff88", times=n_libs)
green =  rep ("#bbffbb", times=n_libs)
green0 =  rep ("#ddffdd", times=n_libs)
blue  = c("#8888ff", rep ("#bbbbff", times=n_libs))
blue2 = c("#aa88ff", rep ("#ccbbff", times=n_libs))
yello= rep ("#ffff88", times=n_libs)
lila0 = rep ("#ccbbff", times=n_libs)
lila1 = rep ("#bb99ff", times=n_libs)
lila2 = rep ("#9966ff", times=n_libs)

mycolors = list (sqrt=yello, exp=red1, log=red, add=blue, div=red, mul=green,
                 asin=lila0, acos=lila1, atan=lila2,
                 sin=green0, cos=green, tan=green1)


png (file = plot_name, width=n_funcs * (n_libs * 55 + 40), height=3*350, res=80)
#png (file = plot_name, width=800 , height=400, res=80)

par (mfcol = c(3, n_funcs), oma=c(0, 0, 5, 0), mar=c(3.1, 3.1, 2.1, 1.1))

# We want the grouping in a specific order, maype passed by a cookie.
x$lib <- factor(x$lib, levels=xlabels)

#lattice::histogram (~ acc | lib * fun , data=x, # equal.widths=TRUE,
#    #breaks=5, 
#    type="percent",
#    col="#ddffdd", xlab="foo")


for (fname in fnames) {

    print (fname)

    # Filter the desired `fname' like "sin".
    xfun <- x[x$fun == fname,]
    
    gain_label = paste (fname, ": ", gain_suffix);
    acc_label  = paste (fname, ": log_2 (accuracy)");

    tlabel = paste (fname, ": ", fun_suffix);

    # No need for "names = xlabels".

    boxplot (xfun[[col_ticks]] ~ xfun$lib, main = tlabel, ylab="Ticks",
        col = mycolors[[fname]],
        cex.axis=1.6, cex.main=1.6,
        ylim = lim_ticks)
        #abline (h=colMeans(tt), lty=3)

    boxplot (xfun[[col_gains]] ~ xfun$lib,
        main = gain_label, ylab="Gains",
        col = mycolors[[fname]],
        cex.axis=1.6, cex.main=1.6,
        ylim = lim_gains)

    boxplot (xfun$acc ~ xfun$lib,
        main = acc_label,
        range=0,
        col = mycolors[[fname]],
        cex.axis=1.6, cex.main=1.6,
        ylim = lim_accs)

        abline (h=-c(56,53,24)-1, lty=3)
}

warnings()
#title (main="titleAll", line=3, outer=TRUE, sub="fdhjks fhdjks fhdjks fhdjks fh")

mtext (side=3, line=0, outer=TRUE, cex=1.10, paste (sep="",
        "Measurement based on ", n_samples, " samples taken for AVR ATmega4808.",
        "  The 2nd row shows\n",
        "log_2 of execution times relative to the times consumed by ",
        ticks_reference, ".\n",
        "Going 1 unit up doubles the number of needed cycles, ",
        "going 2 down means 1/4 of cycles etc."))

dev.off()

q()
#########################################################

#x[x$func == "fun"]

#x1 = subset (x, func=="fun")
#x2 = subset (x, func=="xfun")

#sort (x[2],unique=foo)

q()

x$Gain     <- x$New - x$Old
x$"Gain %" <- 100 * x$Gain / x$Old
x$Bytes    <- factor (sapply (x$bits, bits2bytes))

###########################################

xlabels = c("1-4 Bytes", "1 Byte", "2 Bytes", "3 Bytes", "4 Bytes")

titleOld = paste (func, ": AVR-Libc", sep="")
titleNew = paste (func, ": Tweaked", sep="")
titleAll = paste (func, " Speed Gain Analysis", sep="")

red   = c("#ff8888", rep ("#ffbbbb", times=4))
green = c("#88ff88", rep ("#bbffbb", times=4))
blue  = c("#8888ff", rep ("#bbbbff", times=4))
blue2 = c("#aa88ff", rep ("#ccbbff", times=4))

lim <- c (min (x[1:2]), max (x[1:2]))

###########################################

png (file = plot_name, width=800, height=700, res=80)

par (mfrow = c(2,2), oma=c(0, 0, 5, 0), mar=c(3.1, 3.1, 2.1, 1.1))

boxplot (bytestat (x,1), main = titleOld, ylab="Ticks",
         col = red, ylim = lim, names = xlabels)
    abline (h=mean(x$Old), lty=3)

title (main=titleAll, line=3, outer=TRUE)
       
mtext (side=3, line=0, outer=TRUE, paste (sep="",
       "Measurement based on ", length (x$Old), " Samples ",
       "taken with AVR ATmega168.  AVR-Libc Version: 1.8.0 (SVN 2294)\n",
       "Leftmost box-and-whiskers refer to all samples.  ",
       "The 4 boxes at the right show statistics for n-byte values."))

boxplot (bytestat (x,2), main = titleNew, ylab="Ticks",
         col = green, ylim = lim, names = xlabels)
    abline (h=mean(x$New), lty=3)

boxplot (bytestat (x,6), main="Speed Gain in Ticks", ylab="Gained Ticks",
         col = blue, names = xlabels)
    abline (h=mean(x$Gain), lty=3)

#boxplot (bytestat (x,7), main="Speed Gain in %", ylab="Gained %",
#         col = blue2, names = xlabels)
#    abline (h=-gmean(-x$"Gain %"), lty=3)

dev.off()
