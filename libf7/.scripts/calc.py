from __future__ import print_function

__all__ = ["get_pi", "get_asin", "get_acos", "get_atan", "get_sin", "get_cos",
		   "get_exp", "get_exp1_x", "get_exp1x_x2", "get_tan",
		   "get_asin_x", "get_asin_sqrt_sqrt", "get_sin_sqrt_sqrt", "get_atan_sqrt_sqrt"]

__all__ += [ "bump_precision" ]

from mymathutils import ld
from poly import Poly
import math, decimal
from decimal import Decimal

_one = Decimal(1)

_pi = (0, 0)

def bump_precision (prec2, margin=0):
	""" Bump decimal.getcontext().prec to prec2 binary figures."""
	prec10 = margin + int (1 + prec2 / ld (10))
	decimal.getcontext().prec = max (decimal.getcontext().prec, prec10)
	return decimal.getcontext().prec
	
def get_pi (n):
	""" Compute pi to n binary figures.
		This function might bump decimal.getcontext().prec. """
	if n < 0:
		raise ValueError (n)
	global _pi
	if _pi[0] >= n:
		return _pi[1]

	# As pi/4 = 4 * atan(1/5) - atan (1/239)
	deg_1_5 = n / ld (5)
	deg_1_239 = n / ld (239)
	deg = 1 | int (4 + deg_1_5 * 1.02)
	pp = Poly.atan (deg)
	#print "deg_1_5: ", deg_1_5
	#print "deg_1_239: ", deg_1_239
	bump_precision (n, 2)
	pi_n = 16 * pp (Decimal(1) / 5) - 4 * pp (Decimal(1) / 239)
	#print "pi(%d @2) = pi (%d @10):" % (n, prec10), pi_n
	_pi = (n, pi_n)
	return pi_n

def get_asin_mclaurin (x, n):
	""" Compute asin (x) to n binary figures for small |x|."""
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	bump_precision (n, 2)
	deg_p = int (3 - n / (ld (x) - 1) * 1.01)
	p = Poly.asin (deg_p)
	return p (x)

def get_asin_x_mclaurin (x, n):
	""" Compute asin (x) / x to n binary figures for small |x|."""
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	bump_precision (n, 2)
	deg_p = 1 + int (3 - n / (ld (x) - 1) * 1.01)
	p = Poly.asin (deg_p) >> 1
	return p (x)

def get_asin_sqrt_sqrt_mclaurin (x, n):
	""" Compute asin (sqrt(x)) / sqrt(x) to n binary figures for small |x|."""
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	bump_precision (n, 2)
	deg_p = int (3 - n / (ld (x) - 1) * 1.01)
	p = Poly.asin (1 + 2 * deg_p).stride (1, 2)
	return p (x)

def get_acos (x, n):
	""" Compute acos (x) to n binary figures."""
	""" acos (1 - x) = sqrt (2x) * a(x) """
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	if x.copy_abs() < 0.3:
		return get_pi (n) / 2 - get_asin_mclaurin (x, n)
	x1 = Decimal(1) - x.copy_abs()

	bump_precision (n, 2)
	deg_a = int (3 - n / (ld (x1) - 1) * 1.01)
	pa = Poly.func_a (deg_a)
	y = (2 * x1).sqrt() * pa (x1)
	return y if x > 0 else get_pi (n) - y

def get_asin (x, n):
	""" Compute asin (x) to n binary figures."""
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	if x.copy_abs() < 0.3:
		return get_asin_mclaurin (x, n)
	else:
		return get_pi(n)/2 - get_acos (x, n)

def get_asin_x (x, n):
	""" Compute asin (x) / x to n binary figures."""
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	if x < 0.3:
		return get_asin_x_mclaurin (x, n)
	else:
		return (get_pi(n)/2 - get_acos (x, n)) / x

def get_asin_sqrt_sqrt (x, n):
	""" Compute asin (sqrt(x)) / sqrt(x) to n binary figures."""
	if x.is_nan() or x > 1 or x < -1:
		return Decimal("NaN")
	if abs(x) < 0.3:
		return get_asin_sqrt_sqrt_mclaurin (x, n)
	else:
		s = x.sqrt()
		return get_asin (s, n) / s


def get_exp (x, n):
	""" Compute exp (x) to n binary figures for "small" |x|."""
	if x.is_nan():
		return Decimal("NaN")
	bump_precision (n, 5)
	eps = Decimal (2) ** - (n + 5)
	if eps == 0:
		raise Exception ("bug")
	n = 1
	s = 1 + x
	add = x
	while n < 5 or add.copy_abs() > eps:
		n += 1
		add *= x / n
		s += add
	return s

def get_exp1x_x2 (x, n):
	""" Compute (e^x - 1 - x) / x^2  to n binary figures for "small" |x|."""
	if x.is_nan():
		return Decimal("NaN")
	bump_precision (n, 5)
	eps = Decimal (2) ** - (n + 5)
	if eps == 0:
		raise Exception ("bug")
	n = 2
	s = _one / 2
	add = _one
	while n < 5 or add.copy_abs() > eps:
		n += 1
		add *= x / n
		s += add
	return s

def get_exp1_x (x, n):
	""" Compute (e^x - 1) / x  to n binary figures for "small" |x|."""
	if x.is_nan():
		return Decimal("NaN")
	bump_precision (n, 5)
	eps = Decimal (2) ** - (n + 5)
	if eps == 0:
		raise Exception ("bug")
	n = 1
	s = _one
	add = _one
	while n < 5 or add.copy_abs() > eps:
		n += 1
		add *= x / n
		s += add
	return s

def get_sin (x, n, prec_adjust=True):
	""" Compute sin (x) to n binary figures."""
	if x.is_nan():
		return Decimal("NaN")

	bump_precision (n, 10)

	extra10 = 0
	if prec_adjust:
		# Excess precision needed if |sin(x)| is small so that relative error
		# is as desired
		sin_x = math.sin(float(x))
		if sin_x != 0:
			extra   = 2 + int (-ld (sin_x))
			extra10 = 2 + int (-ld (sin_x) / ld (10))
			if extra > 0:
				n += extra
			if extra10 > 0:
				decimal.getcontext().prec += extra10

	pi = get_pi (10+n)
	x = x.remainder_near (2 * pi)
	if x < 0:
		y = get_sin (x.copy_negate(), n, prec_adjust=False).copy_negate()
		if extra10 > 0:
			decimal.getcontext().prec -= extra10
		return y

	neg = x > pi
	if neg:
		x -= pi
	if x > pi/2:
		x = pi - x

	eps = Decimal (2) ** - (n + 10)
	if eps == 0:
		raise Exception ("bug")
	n, s, add, x2 = 1, x, x, -x*x
	while n < 5 or add.copy_abs() > eps:
		n += 2
		add *= x2 / (n * (n-1))
		s += add

	if extra10 > 0:
		decimal.getcontext().prec -= extra10

	return s if not neg else s.copy_negate()

def get_sin_sqrt_sqrt (x, n):
	""" Compute sin (sqrt(x)) / sqrt(x) to n binary figures."""
	if x.is_nan():
		return Decimal("NaN")
	bump_precision (n, 5)
	eps = Decimal (2) ** - (n + 5)
	if eps == 0:
		raise Exception ("bug")
	n, s, add =  1, 1, 1
	while n < 5 or add.copy_abs() > eps:
		n += 2
		add *= -x / (n * (n-1))
		s += add

	return s

def get_cos (x, n):
	""" Compute cos (x) to n binary figures."""
	if x.is_nan():
		return Decimal("NaN")
	return get_sin (get_pi(10+n) / 2 - x, n)

def get_tan (x, n):
	""" Compute tan (x) to n binary figures."""
	if x.is_nan():
		return Decimal("NaN")
	return get_sin(x,n) / get_cos(x,n)


def get_atan (x, n, allow_neg = False):
	""" Compute atan (x) to n binary figures."""

	if x.is_nan():
		return Decimal("NaN")
	if x < 0 and not allow_neg:
		return -get_atan (-x, n)

	One = Decimal(1)
	pi = get_pi(3+n)

	if x.is_infinite():
		return pi/2
	elif x > One:
		return pi/2 - get_atan (One/x, 2+n)

	bump_precision (n, 3)

	w3 = Decimal(3).sqrt()
	if x > 2 - w3:
		xx = (x*w3 - 1) / (w3 + x)
		return pi/6  + get_atan (xx, n, True)

	deg_p = 1 | int (3 - n / ld (x) * 1.01)
	y, x2 = 0, x*x

	for k in xrange (deg_p, 0, -2):
		ak = One / k
		#print "k:", k, (-ak if k & 2 else ak)
		y = x2 * y + (-ak if k & 2 else ak)
	y *= x
	return y


def get_atan_sqrt_sqrt (x, n, allow_neg = False):
	""" Compute atan (sqrt(x)) / sqrt(x) to n binary figures."""

	if x.is_nan():
		return Decimal("NaN")

	One = Decimal(1)
	margin = Decimal ("0.015")
	if abs(x) > margin + 2*One - (3*One).sqrt():
		z = x.sqrt()
		return get_atan(z, n) / z

	bump_precision (n, 3)

	deg_p = 1 | int (3 - n / ld (x) * 1.01)
	p = Poly.atan (deg_p)
	p = p.stride (1,2)
	return p(x)
