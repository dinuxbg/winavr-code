from __future__ import print_function
import math, sys

from poly import Poly
from calc import *
import decimal
from decimal import Decimal

from approx import MiniMax, MiniMaxRational, MiniMaxGroups

def _flush():
	sys.stdout.flush()

def main():
	q = None
	T = float
	T = Decimal

	def f (x, n_tries):

		if 1 == 1:
			#return math.atan(x)
			#if abs(x) < 1e-8: return 1 + x
			#return math.exp(x) # T(get_exp1_x (x, bits))
			if x < 1e-7:
				return 1 - x/3 + x*x/5 - x*x*x/7 + x*x*x*x/9
				#return 1 + x/12 + 3*x*x/160 + 5*x*x*x/896
			z = math.sqrt (x) #/2
			return math.atan(z) / z
			return T(get_asin_sqrt_sqrt (Decimal(x)/2, bits))
			if x < 1e-7:
				return 1 + x/12
			z = math.sqrt(x/2)
			return math.asin(z)/z
		
		x = math.sqrt(x)
		x *= pi
		if x == 0: return pi
		return pi * math.sin(x) / x

#	decimal.getcontext().prec = 35

	prec = decimal.getcontext().prec
	bits = int (1 + prec * math.log(10) / math.log(2))
	print ("prec=%d, bits=%d" % (prec, bits))

	def fD (x, n_tries):
		return get_cos (x.sqrt(), bits)
		return get_sin_sqrt_sqrt (x, bits)
		s = x/2 #(_abs(x)/2).sqrt()
		return get_asin_sqrt_sqrt (s, bits)
		#return get_exp (x, bits)
		#if x == 0: return 0
		#if _abs(x) < 1e-10: return 1 + x/2
		#return (get_exp(x,bits)-1-x)/(x*x)
		#print ("prec=%d, bits=%d" % (prec, bits))
		return y

	#MiniMaxRational (5, 4, f if not T is Decimal else fD, a, b, T=T)

	margin = T(Decimal (1)/10**18)
	a = T(0)
	b = T(Decimal(2) - Decimal(3).sqrt())

	ff = f if not T is Decimal else fD

	b0 = b
	b *= b
	a = T(0)
	b = T(math.pi/4)
	b0 = b;
	b *= b
	margin = 0
	#b = T(1)/2
	#b = T(Decimal(2).ln() / 3)
	
	print ("x in [%s,%s]" % (a,b))
	#MiniMaxRational (3, 3, f if not T is Decimal else fD, T(a), T(b), T=T)

	xmax=None
	x_max = None
	#x_max = [Decimal('-1.0000E-18'), Decimal('0.100718592043018343813216909704754847346'), Decimal('0.202591777792460706665680792831739323642'), Decimal('0.302731712445547165886731101398505567250'), Decimal('0.402985929886271592146781828348823864481'), Decimal('0.502210147679718034835580963997324274016'), Decimal('0.597768842619371344222172503421716771354'), Decimal('0.689454538902175334906494858799563197837'), Decimal('0.774862934684505865626572087594819317070'), Decimal('0.849482081730234911984667584223513487415'), Decimal('0.912988815434177433296900704618379543663'), Decimal('0.960237314974221313202066186540768214055'), Decimal('0.9874999999999999999875'), Decimal('1.0000000000000000000000')]
	#x_max = [-0.0001, 0.0124025, 0.05542749330715237, 0.12202881069559164, 0.21048442926517294, 0.31566556539092583, 0.43206150368085616, 0.5528108195720403, 0.6708479567295962, 0.7791189660943383, 0.8710185337674918, 0.9410112138450442, 0.9875975, 1.0001]
	
	m,n = 7,0

	#x_max = []
	#for k in range(m+n+2):
	#	a = 0.0
	#	t = k / (m+n+1.0)
	#	t = (t + a*t*t) / (1 + a)
	#	x_max += [ 0.5 - math.cos (math.pi * t) / 2 ]
	x_max = None
	#x_max = [Decimal('-1.0000E-30'), Decimal('0.0124999999999999999999999999990125'), Decimal('0.04085658240037566419124737578901571776692485472765'), Decimal('0.09177920660824735826431586069728017956771613663512'), Decimal('0.1624999999999999999999999999991625'), Decimal('0.2518882356737308601178187423596184476560884477139'), Decimal('0.3575619466962995562134452308675601366374256346765'), Decimal('0.4755434024138459156186490106647530897273788966920'), Decimal('0.5998154117178691203547494269826944211030445585534'), Decimal('0.7222719220952000679635642876894144838941152845724'), Decimal('0.8331631438837968115910617147975702428494674707081'), Decimal('0.9221608331295827936522758278740571681022135577422'), Decimal('0.9799511872475692457458024602048481706624575900437'), Decimal('1.0000000000000000000000000000000000')]


	if 1 == 1:
		if n == 0:
			(p,xmax) = MiniMax (m, ff, T(a-margin), T(b+margin), T=T, x_max=x_max)
		else:
			(p,q,xmax) = MiniMaxRational (m,n, ff, T(a-margin), T(b+margin), T=T, x_max=x_max)
		print ("\n")
	else:
		
		# atan_s/s d_max = 5.808E-18, quot = 1.073
		#p = Poly([Decimal("1209.7470017580907217240715"), Decimal("3031.0745956115083044212807"), Decimal("2761.7198246138834959053784"), Decimal("1114.1290728455183546172942"), Decimal("192.5792014481559613474286"), Decimal("11.3221594116764655236245"), Decimal("0.09762721591717633036983")])
		#q = Poly([Decimal("1209.7470017580907287514197"), Decimal("3434.3235961975351716547069"), Decimal("3664.5449563283749893504796"), Decimal("1821.6003392918464941509225"), Decimal("423.0716464809047804524206"), Decimal("39.9178842486537981501999"), Decimal("1.0")])

		# asin_s/2 / s/2
		p = Poly ([Decimal('0.99999999999999999442491073135027586203'), Decimal('-1.0352340338921976278427312087167692142'), Decimal('0.35290206232981519813422591897720574012'), Decimal('-0.043334831706416857056123518013656946650'), Decimal('0.0012557428614630796315205218507940285622'), Decimal('0.0000084705471128435769021718764878041684288')])
		q = Poly ([Decimal('1'), Decimal('-1.1185673672255329236623716486696411533'), Decimal('0.42736600959872448854098334016758333519'), Decimal('-0.063555884849631716599421483898013782858'), Decimal('0.0028820878185134035637440105959294542908')])

		#p = p.map(float).map(T)
		#q = q.map(float).map(T)
		
		print ("p(x) =",p.toString(style=Poly.GNUPLOT))
		print ("q(x) =",q.toString(style=Poly.GNUPLOT))
		print ("\tp =",p.toString(style=Poly.DECIMAL))
		print ("\tq =",q.toString(style=Poly.DECIMAL))
		print (p.toString(style=Poly.IEEE_double,var='p'))
		print (q.toString(style=Poly.IEEE_double,var='q'))
		p.cdef()
		q.cdef()
		
		def fun(x):
			#x2 = x*x
			gx = p(x)/q(x)
			fx = get_asin_sqrt_sqrt (x/2, bits)
			return (gx - fx)/fx

		G = MiniMaxGroups (m+n+2, fun, Decimal(0), Decimal(0.5))
		(x,deltas) = G.max_errors()
		(max_d, a_mean, g_mean) = G.quality()
		print ("\tx_max =", x);

	_flush()

	if not q is None:
		q.map (T)
	p.map (T)

	def fplot (x):
		return T(get_asin (x, bits))

	def eval (pq, x):
		return x * pq (2*x*x)

	def fplot2 (x):
		return get_acos (1-x, bits)

	def eval2 (pq, x):
		return (2*x).sqrt() * pq (x)

	def fplotE (x):
		return T(get_exp (Decimal(x), bits))

	#plotfile ("gplot-acos.data", a, b, samples, fplot2, p, q, T=T, eval_pq=eval2)

	def fplot3 (x):
		return T(get_atan (Decimal(x), bits))

	def g(x):
		x = T(x)
		y = p(x)
		return y if q is None else (y / q(x))

	def deltaSIN (x):
		fx = get_sin (x, bits)
		if x == 0: return 0
		x = T(x)
		gx = x*g(x*x)
		fx,gx = T(fx),T(gx)
		return float ((gx - fx) / fx)

	def deltaCOS (x):
		fx = get_cos (x, bits)
		if x == 0: return 0
		x = T(x)
		gx = g(x*x)
		fx,gx = T(fx),T(gx)
		return float ((gx - fx) / fx)

	def deltaASIN (x):
		fx = get_asin (x, bits)
		if x == 0: return 0
		x = T(x)
		gx = x*g(2*x*x)
		fx,gx = T(fx),T(gx)
		return float ((gx - fx) / fx)

	def deltaACOS (x):
		fx = get_acos (x, bits)
		if x == 0 or x == 1: return 0
		x = 1-T(x)
		gx = (2*x).sqrt() * g(x)
		fx,gx = T(fx),T(gx)
		return float ((gx - fx) / fx)

	def deltaATAN (x):
		fx = get_atan (Decimal(x), bits)
		if x == 0 or x == 1: return 0
		x = T(x)
		gx = x*g(x*x)
		fx,gx = T(fx),T(gx)
		return float ((gx - fx) / fx)

	print ("x_max =", xmax)
	print ("p(x) =",p.toString(style=Poly.GNUPLOT))
	if not q is None:
		print ("q(x) =",q.toString(style=Poly.GNUPLOT))

	print ("\tp =", p.toString(style=Poly.DECIMAL))
	if not q is None:
		print ("\tq =", q.toString(style=Poly.DECIMAL))

	p.cdef()
	if not q is None:
		q.cdef()

	samples = 97

	plot_file ("gplot-atan.data", a, b0, samples, deltaCOS, T=T)

	suff = ("%d" % m) if n == 0 else ("%d_%d" % (m,n))
	plot_xmax ("xmax_%s.data" % suff, xmax)


def plot_xmax (fname, xmax):
	if xmax is None:
		return

	if not fname.endswith (".data"):
		raise ValueError (fname)
	print ("write %s ..." % fname)
	fxmax = open (fname, "w")
	for k in range(len(xmax)):
		fxmax.write ("%s %s\n" % (k, xmax[k])) 
	fxmax.close()


def plot_file (fname, lo, hi, num, func, T=float, xmax=None):

	decimal.getcontext().prec = 35

	prec = decimal.getcontext().prec
	bits = int (1 + prec * math.log(10) / math.log(2))
	print ("prec=%d, bits=%d" % (prec, bits))

	(lo,hi) = (T(lo),T(hi))
	step = (hi - lo) / num

	print ("plot [%s : %s] to %s" % (lo,hi,fname))

	if not fname.endswith (".data"):
		raise ValueError (fname)

	fout = open (fname, "w")
	for k in range(1+num):
		x = lo + k * step
		k += 1
		if 1 == 1:
		#try:
			fx = func(x)
			print (x, fx)
			fout.write ("%s %s\n" % (x, fx))
		#except Exception as e:
			#raise e
			pass
	
	fout.close()

from mymathutils import *

if __name__ == "__main__":
	#plotfile()
	#main()
	decimal.getcontext().prec = 30
	x = Decimal(300)
	x = dec_from_f7 ("F7_CONST_DEF (sqrt2,    0,  0xb5,0x04,0xf3,0x33,0xf9,0xde,0x65, 0)")
	print ("x = %s = " % x, cdef(x))
	print ("ln(%s) = " % "x", cdef(x.ln()))

	x = dec_from_f7 ("F7_CONST_DEF (sqrt2,    0,  0xb5,0x00,0x00,0x00,0x00,0x00,0x00, 0)")
	print ("x = %s = " % x, cdef(x))
	print ("ln(%s) = " % "x", cdef(x.ln()))

	x = dec_from_f7 ("F7_CONST_DEF (sqrt2,    0,  0xb5,0x00,0x00,0x00,0x00,0x01,0x00, 0)")
	print ("x = %s = " % x, cdef(x))
	print ("ln(%s) = " % "x", cdef(x.ln()))

	x = dec_from_f7 ("F7_CONST_DEF (sqrt2,    0,  0xb4,0xff,0xff,0xff,0xff,0xff,0xff, 0)")
	print ("x = %s = " % x, cdef(x))
	print ("ln(%s) = " % "x", cdef(x.ln()))
	#y = Decimal(2)
	#print ("ln2 = ", cdef(y.ln(), 14))
	#print ("d_ln2 = ", cdef(y.ln() - ln2_round, 14))
