from __future__ import print_function

from copy import copy
from collections import Iterable
from decimal import Decimal
from mymathutils import get_1
from itertools import imap
from operator import mul as operator_mul

import sys

def _flush (s="\n"):
	return
	print (s,end="")
	sys.stdout.flush()

class Vector (object):
	def __init__ (self, *args):
		if len(args) == 1 and type(args[0]) is list:
			self._a = args[0]
		elif len(args) == 1 and isinstance (args[0], Iterable):
			self._a = [a for a in args[0]]
		else:
			self._a = list(args)
		self._dim = len (self._a)

	@staticmethod
	def zero (n, zero=0):
		return Vector ([zero] * n)

	@staticmethod
	def e_k (n, k, one=1):
		"""k-th canonical unit vector of dimension n. """
		e = Vector.zero (n, one * 0)
		e[k] = one
		return e

	def __getitem__ (self, k):
		return self._a[k]

	def __setitem__ (self, k, val):
		self._a[k] = val

	def __call__ (self):
		return range(self.dim)

	a   = property (lambda self: self._a, None, None, """""")
	dim = property (lambda self: self._dim, None, None, """""")

	def map (self, fun, in_place=False):
		#a = [ fun(self[k]) for k in self() ]
		if in_place:
			for k in self():
				self._a[k] = fun (self.a[k])
		else:
			return Vector(map (fun, self._a))

	def __repr__ (self):
		s = "["
		for x in self:
			s += " " + str(x)
		return s + " ]" #str (self.a)

	def fits (self, v):
		if self.dim != v.dim:
			raise ValueError (self, v)

	def __add__ (self, v):
		self.fits (v)
		return Vector ([self[k] + v[k] for k in self()])

	def __neg__ (self):
		return Vector ([-self[k] for k in self()])

	def __sub__ (self, v):
		self.fits (v)
		return Vector ([self[k] - v[k] for k in self()])

	def __mul__ (self, v):
		if type(v) is Vector:
			self.fits (v)
			return sum (self[k] * v[k] for k in self())
		elif isinstance (v, Iterable):
			return sum (imap (operator_mul, self, v))
		else:
			return Vector (self[k] * v for k in self())

	def __rmul__ (self, v):
		return self * v

	def __pow__ (self, v):
		""" Outer product, a Matrix """
		if not type(v) is Vector:
			raise ValueError (v)
		rows = [self[k] * v for k in self()]
		return Matrix (rows=rows)


import math

class Matrix (object):
	""" A 2-dimensional matrix represented as list of row Vector's. """
	def __init__ (self, dim=None, rows=None, cols=None, zero=0):
		if not dim is None:
			if not type(dim) is tuple or len(dim) != 2:
				raise ValueError (dim)
			if not type(dim[0]) is int or dim[0] < 1:
				raise ValueError (dim)
			if not type(dim[1]) is int or dim[1] < 1:
				raise ValueError (dim)

		self._dim = dim

		if rows is None and cols is None:
			# Null matrix
			self._rows = [Vector.zero(self.n_cols) for k in range(self.n_rows)]
		elif rows is not None and cols is not None:
			raise ValueError ("setting both rows and cols")
		elif rows is not None:
			if self._dim is None:
				self._dim = (len(rows), rows[0].dim)
			else:
				if self.n_rows != len (rows):	raise ValueError ()
				if self.n_cols != rows[0].dim:	raise ValueError ()
			self._rows = []
			#print (rows)
			for r in range(len(rows)):
				if not type(rows[r]) is Vector:
					raise ValueError (rows)
				self._rows += [rows[r]]
				self._rows[0].fits (self._rows[r])
		elif cols is not None:
			M = Matrix (rows=cols).t
			self._rows = M._rows
			self._dim = M._dim
		else:
			raise Exception ("todo")

	def transpose (self):
		rows = [ Vector(self.col(k)) for k in range(self.n_cols) ]
		return Matrix (rows=rows)

	n_rows = property (lambda self: self._dim[0], None, None, "")
	n_cols = property (lambda self: self._dim[1], None, None, "")
	t = property (transpose, None, None, "")

	def __getitem__ (self, pos):
		if type(pos) is int:
			return self._rows[pos]
		if not type(pos) is tuple or len(pos) != 2:
			raise ValueError (pos)
		row = self._rows[pos[0]]
		return row[pos[1]]

	def __setitem__ (self, pos, val):
		if type(pos) is int:
			self._rows[pos] = val
			return
		if not type(pos) is tuple or len(pos) != 2:
			raise ValueError (pos)
		row = self._rows[pos[0]]
		row[pos[1]] = val

	def col (self, c, start=0):
		for r in range(start, self.n_rows):
			yield self[r,c]

	def map (self, fun, in_place=False):
		if in_place:
			for row in self._rows:
				row.map (fun, in_place)
		else:
			rows = [ row.map (fun, in_place) for row in self._rows ]
			return Matrix (self._dim, rows=rows)
		
	def __repr__ (self):
		s = " %dx%d " % self._dim
		for r in self:
			s += str(r) #+ ("0x%x" % id(r))
			s += "\n"
		return s

	def fits_add (self, m):
		if self._dim != m._dim:
			raise ValueError (self._dim, m._dim)

	def fits_mul (self, m):
		if type(m) is Vector:
			if self.n_cols != m.dim:
				raise ValueError (self._dim, m)
			return self.n_rows

		if self.n_cols != m.n_rows:
			raise ValueError (self._dim, m._dim)
		return (self.n_rows, m.n_cols)

	def __neg__ (self):
		rows = [ -self[r] for r in range(self.n_rows) ]
		return Matrix (dim=self._dim, rows=rows)

	def __add__ (self, m):
		self.fits_add (m)
		rows = [ (self[r] + m[r]) for r in range(self.n_rows) ]
		return Matrix (dim=self._dim, rows=rows)

	def __sub__ (self, m):
		self.fits_add (m)
		rows = [ (self[r] - m[r]) for r in range(self.n_rows) ]
		return Matrix (dim=self._dim,rows=rows)

	def __mul__ (self, m):
		if type(m) is Matrix:
			dim = self.fits_mul (m)
			rows = []
			for r in range(self.n_rows):
				row = []
				for c in range(m.n_cols):
					row += [ sum (self[r,i] * m[i,c] for i in range(self.n_cols)) ]
				rows += [Vector (row)]
			return Matrix (dim=dim, rows=rows)
		if type(m) is Vector:
			self.fits_mul (m)
		a = [ r * m for r in self ]
		return Vector(a) if type(m) is Vector else Matrix(dim=self._dim, rows=a)

	def __pow__ (self, ex):
		if ex < 0:
			raise Exception ("todo", ex)
		m = None
		m2 = self
		while True:
			if ex & 1:
				m = m2 if m is None else (m * m2)
			ex >>= 1
			if ex == 0:
				return m if not m is None else Matrix.id (self._dim)
			m2 *= m2

	def __rmul__ (self, m):
		return self * m

	def add_columns (self, m):
		if self.n_rows != m.n_rows:
			raise ValueError (self._dim, m._dim)
		rows = []
		n_cols = self.n_cols + m.n_cols
		for r in range(self.n_rows):
			row = self[r]._a + m[r]._a
			rows += [ Vector(row) ]
			#print ("row=", row)
		return Matrix (dim=(self.n_rows, n_cols), rows=rows)

	@staticmethod
	def id (dim, zero=0, one=1):
		if type(dim) is int:
			dim = (dim,dim)
		m = Matrix (dim, zero=zero)
		for k in range(m.n_rows):
			m[k,k] = one
		return m

	@staticmethod
	def powers (n, xs, first_column=None, from_expo=0):
		""" Return a matrix with row x^0, x^1, x^2, ... x^n for each x. """
		one = get_1 (xs[0])
		rows = []
		expo = range (max(1,from_expo), 1+n)
		for r in range(len(xs)):
			first = [] if first_column is None else [ first_column[r] ]
			if from_expo == 0: first += [ one ]
			row = first + [ (xs[r] ** k) for k in expo ]
			rows += [ Vector(row) ]
		return Matrix (rows=rows)


	def QR_mul_minor (self, q, f):
		""" Left-multiply square matrix self with a (smaller) square matrix q.
			q is (implicitly) extended as follows:
			* q is located south-east in a square matrix with the same
			  dimension like self.
			* North and west of q is a block of 0.
			* Noth-west of q is f*identity with dim_id = dim_self - dim_q.
			
			This means: The upper dim_id rows of self are unchanged.
		"""
		dim_id = self.n_rows - q.n_rows

		rows = self._rows[0:dim_id]
		if f != 1:
			rows = [ row * f for row in rows ]

		r_cols = range(self.n_cols)
		for r in range (q.n_rows):
			row = [ q[r] * self.col(c, start=dim_id) for c in r_cols ]
			#print ("row=",row)
			#_flush(".")
			rows += [ Vector(row) ]
			#print ("rows=",rows)
		return Matrix (rows=rows)

	def QR_minor (self, k, normalize):
		""" Run one QR step (Householder) in the submatrix starting at [k,k]."""

		ak = Vector (self.col(k, start=k))
		dim = ak.dim
		akak = ak * ak
		sign = -1 if ak[0] >= 0 else 1

		if type(akak) is Decimal:
			norm_ak = akak.sqrt()
		else:
			norm_ak = math.sqrt (akak)
		ek = Vector.e_k (dim, 0)
		#print ("e%d_%d=" % (dim, 0), ek)
		u = ak - sign * norm_ak * ek
		#print ("akek = ", ak*ek)
		#print ("uk=",u)
		uu = u*u
		if normalize:
			if uu == 0:
				raise ZeroDivisionError(u)
			one = get_1 (uu)
			Qk = Matrix.id (dim) - (2 * one / uu * u) ** u
			uu = 1
		else:
			# Instead of normalizing u, we track the denominator for Qk.
			Qk = Matrix.id (dim,one=uu) - (2 * u) ** u
		#print ("Qk=",7*Qk)
		#Qk *= 1./(u*u)
		#print ("Qk*self=", self.QR_mul_minor(Qk))
		return (uu, Qk)

	def QR_cleanup_R (self, k0):
		""" After the QR-step that shall produce zeros below [k0,k0], set the
			elements below the diagonal from [0,0] to [k0,h0] to zero by hand.
		"""
		#_flush("{")
		for k in range(k0+1):
			for j in range(k+1, self.n_rows):
				#print ("R[%d,%d] %s ="%(j,k,self[j,k]==0), self[j,k])
				self[j,k] = 0
		#_flush("}")

	def QR (self, normalize=True):
		""" Return (Q,R,f) the Q R decomposition of square matrix A=self,
			i.e.  A = Q * R where
			* Q is orthogonal, i.e. Q^t * Q = Id.
			* R is an upper trianlgle
			If normalize is False, then return matrices Q' and R' such that
			Q = Q'/f
			R = R'/f
			If normalize os Trur, f will be 1. """

		_flush ("\nQR:")
			
		(f,Q) = self.QR_minor (0, normalize)
		_flush ("[")
		R = Q * self
		_flush ("]")
		R.QR_cleanup_R (0)
		#print ("Q=",Q)
		#print ("R=",R)
		#print ("f0=",f)
		
		_flush ("0.")

		for k in range(1, self.n_rows-1):
			(fk,Qk) = R.QR_minor (k, normalize)
			#_flush ("(")
			Q = Q.QR_mul_minor (Qk,fk)
			#_flush (">")
			#print ("Q=",Q)
			#_flush ("(")
			R = R.QR_mul_minor (Qk,fk)
			#_flush (")")
			#print ("R=",R)
			R.QR_cleanup_R (k)
			#print ("R=",R)
			#print ("f%d="%k,fk)
			f *= fk
			#print ("prod_%d f="%k,f)
			_flush ("%d." % k)
		Q = Q.t
		#ff = one / f
		return (Q,R,f)

	USE_QR = "useQR"
	USE_UPPER_TRIANGLE = "useUpperTriangle"

	def solve (self, b, way=None):
		""" Solve the linear system A * x = b where A = self. """

		if way == Matrix.USE_QR:
			(Q,R,f) = self.QR()
			#print ((Q,R,f,b))
			if f != 1:
				raise Exception ("todo")
			# A = Q * R with Q orthogonal and R upper triangle:
			# A * x = b  <=>  Q * R * x = b  <=>  R * x = Q^t * b
			b = Q.t * b
			#print ("Qt=",Q.t)
			x = R.solve (b, way=Matrix.USE_UPPER_TRIANGLE)
			_flush("done\n")
			return x

		elif way == Matrix.USE_UPPER_TRIANGLE:
			#print ("upper=",self, b)
			dim = self.n_rows
			x = [ b[dim-1] / self[dim-1,dim-1] ]
			#print ("x =", x)
			for k in range (dim-2, -1, -1):
				# The portion of the row right of [k,k]
				row_tail = Vector (self[k]._a[k+1:])
				# the portion of b below b[k]
				#b_tail = Vector (b._a[k+1:])
				#print (k,row_tail,x)
				xk = (b[k] - row_tail * Vector(x)) / self[k,k]
				x = [xk] + x
			#print ("x=",x)
			return Vector(x)

		else:
			raise Exception ("todo")

from poly import Poly

def main():
	def todec (x):
		return Decimal(x)

	m = Matrix (rows=[Vector([1,2,3]),Vector([4,5,6]),Vector([7,8,-10])])
	b = Vector([3,4,5])
	x = m.solve (b, Matrix.USE_QR)
	print ("x!=",x)
	print ("m*x!=",m*x)
	return
	A = m
	(Q,R,f)= m.QR(normalize=True)
	print ("f!=",f)
	print ("Q!=",Q)
	print ("Q!*Q^t=",Q*Q.t)
	print ("R!=",R)
	R.QR_cleanup_R(R.n_rows)
	print ("R!=",R)
	print ("Q!*R! - m =",Q*R - m)
	t = Poly.Tn(4)
	print (t)
	print (t(Decimal(4)))
	
if __name__ == '__main__':
	main()
