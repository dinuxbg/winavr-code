import string
import os
import re
import sys
import subprocess
from sys import exit as exit
from collections import Iterable

CC = "avr-gcc"

common_opt  = " -mrelax -Wl,--gc-sections -fno-ident"
common_opt += " -nostartfiles -Wl,--defsym,main=0"
common_opt += " -Os -ffunction-sections -fdata-sections -mcall-prologues -mstrict-X"

def L (text):
	return string.join (text.splitlines(), "\n")

def n_args (fun):
	args2 = ("f_add",  "f_sub",  "f_mult", "f_div",  "f_pow",
			 "f7_add", "f7_sub", "f7_mul", "f7_div", "f7_pow",
			 "fp64_add", "fp64_sub", "fp64_mul", "fp64_div", "fp64_pow")
	return 2 if fun in args2 else 1

def run (cmd, src=None):
	print "cmd =", cmd
	cmd = re.split (r'\s+', cmd)
	pipe = subprocess.PIPE
	proc = subprocess.Popen (cmd, stdin=pipe, stdout=pipe, stderr=pipe, bufsize=10000)
	(out,err) = proc.communicate (src)
	
	if len (err) > 20:
		print "err=<<<", L(err), ">>>"
	#print "out=<<<", L(out), ">>>"
	#print "err=<<<", L(err), ">>>"
	return (out,err)

white = "\\s+"
num = "(\\d+)"
hex = "[a-f0-9]+"
pat_size = re.compile (4 * (white + num) + white + hex + white + "(.*)$")

def get_size (elf):
	(out,err) = run ("avr-size " + elf)
	print "out=<<<%s>>>" % L(out)

	for line in out.splitlines():
		#print "l=", line
		m = re.match (pat_size, line)
		if m and m.group (5) == elf:
			print "size =", int (m.group (1))
			return int (m.group (1))
	assert False, "no size-line found"


def decorate (syms, tpl):
	two = 2 == tpl.count ("%s")
	if type (syms) in (str, string):
		syms = [syms]
	text = ""
	for s in syms:
		if len (s.strip()) > 0:
			text += tpl % ((s,s) if two else s)
	return text

no_foreign = decorate (("abort", "__do_copy_data", "__do_clear_bss"), \
					   " -Wl,-u,%s,--defsym,%s=0")

def get_size_f7 (fun, mcu):
	""" Determine size of function[s] fun in an otherwise empty program."""

	opt = decorate (fun, " -Wl,-u,%s")
	opt += " -L.. -lf7 -I.."
	
	# __do_copy_data due to acc_sin/cos etc. will go away in the future.
	opt += no_foreign
	opt += common_opt

	cmd = CC + " empty.c -mmcu=" + mcu + opt
	(out,err) = run (cmd)

	return get_size ("a.out")

w7_code = "\t__attribute__((__used__)) uint64_t wrap_%s"
w7_code1 = w7_code + """ (uint64_t x)
	{
		f7_t xx;
		f7_set_double (&xx, x);
		%s (&xx, &xx);
		return f7_get_double (&xx);
	}\n"""
w7_code2 = w7_code + """ (uint64_t x, uint64_t y)
	{
		f7_t xx, yy;
		f7_set_double (&xx, x);
		f7_set_double (&yy, y);
		%s (&xx, &xx, &xx);
		return f7_get_double (&xx);
	}\n"""

def get_size_wrap7 (fun, mcu):
	""" Determine size of function[s] fun in an otherwise empty program."""

	# We have to wrap ALL of the f7_t functions.
	
	print "wrappers for: ", fun
	wrap1 = filter (lambda x: 1 == n_args (x),  fun)
	wrap2 = filter (lambda x: 2 == n_args (x),  fun)
	code = '\t#include "libf7.h"\n'
	code += decorate (wrap1, w7_code1)
	code += decorate (wrap2, w7_code2)
	print "wrapper code =\n", code

	opt = decorate (fun, " -Wl,-u,wrap_%s")
	opt += " -L.. -lf7 -I.."

	# __do_copy_data due to acc_sin/cos etc. will go away in the future.
	opt += no_foreign
	opt += common_opt

	cmd = CC + " -x c - -xnone -mmcu=" + mcu + opt
	(out,err) = run (cmd, code)

	return get_size ("a.out")


def get_size_f64 (fun, mcu):
	""" Determine size of function[s] fun in an otherwise empty program."""

	wrappers = []
	real_fun = []
	for f in ([fun] if type (fun) in (str, string) else fun):
		# The following are just macros and need wrappers
		if f in ("f_pow", "f_sin", "f_cos", "f_tan",
				 "f_arcsin", "f_arccos", "f_arctan"):
			real_fun += ["wrap_" + f]
			wrappers += [f]
		else:
			real_fun += [f]

	code = None
	if len (wrappers) > 0:
		print "wrappers for: ", wrappers
		print "real_fun: ", real_fun
		code = "\t__attribute__((__used__)) float64_t wrap_%s"
		code1 = code + " (float64_t x) { return %s (x); }\n"
		code2 = code + " (float64_t x, float64_t y) { return %s (x, y); }\n"
		wrap1 = filter (lambda x: 1 == n_args (x),  wrappers)
		wrap2 = filter (lambda x: 2 == n_args (x),  wrappers)
		code = '\t#include "avr_f64.h"\n'
		code += decorate (wrap1, code1)
		code += decorate (wrap2, code2)
		print "wrapper code =\n", code
	
	opt = decorate (real_fun, " -Wl,-u,%s")
	opt += no_foreign
	opt += common_opt

	cmd = CC + " -x c - -xnone avr_f64.o -mmcu=" + mcu + opt
	(out,err) = run (cmd, code)

	return get_size ("a.out")

def get_size_fp64lib (fun, mcu):
	""" Determine size of function[s] fun in an otherwise empty program."""

	# For now, no need for any wrappers in fp64lib.

	opt = decorate (fun, " -Wl,-u,%s")
	opt += no_foreign
	opt += common_opt

	cmd = CC + " empty.c -mmcu=" + mcu + opt
	cmd += " -Lfp64lib-1.0.6 -lfp64-atmega128"
	(out,err) = run (cmd, None)

	return get_size ("a.out")


fun_f64 = {
	"mul": "f_mult", "add": "f_add", "sub": "f_sub", "div": "f_div", 
	"sqrt": "f_sqrt",
	"sin": "f_sin", "cos": "f_cos", "tan": "f_tan",
	"asin": "f_arcsin", "acos": "f_arccos", "atan": "f_arctan",
	"log": "f_log", "exp": "f_exp", "pow": "f_pow",
}

fun_f7 = {
	"mul": "f7_mul", "add": "f7_add", "sub": "f7_sub", "div": "f7_div", 
	"sqrt": "f7_sqrt",
	"sin": "f7_sin", "cos": "f7_cos", "tan": "f7_tan",
	"asin": "f7_asin", "acos": "f7_acos",
	"log": "f7_log", "exp": "f7_exp", "pow": "f7_pow",
	"atan": "f7_atan",
}

fun_fp64lib = {
	"mul": "fp64_mul", "add": "fp64_add", "sub": "fp64_sub", "div": "fp64_div", 
	"sqrt": "fp64_sqrt",
	"sin": "fp64_sin", "cos": "fp64_cos", "tan": "fp64_tan",
	"asin": "fp64_asin", "acos": "fp64_acos",
	"log": "fp64_log", "exp": "fp64_exp", "pow": "fp64_pow",
	"atan": "fp64_atan",
}

libs = (
	("f7_t", 					fun_f7, get_size_f7), 
	("f7_t in IEEE wrappers", 	fun_f7, get_size_wrap7), 
	("avr_f64.c",				fun_f64, get_size_f64),
	("fp64lib",					fun_fp64lib, get_size_fp64lib),
)

def get_size_line (funs, funs_label=None):
	""" Get sizes for all "lib"s in one text line."""
	if type (funs) in (str, string):
		funs = [funs]
	line = funs_label if funs_label else string.join (funs, " ")
	line = "'" +  line + "'"
	print "Get sizes for:",  funs

	for (lib, mapf, getsize) in libs:
		fs = []
		for f in funs:
			fs += [ mapf[f] ]
		try:
			os.remove ("a.out")
		except OSError:
			pass
		n_bytes = getsize (fs, "atmega128")
		line +=  " %s" % n_bytes
		print "fs", lib, ":", fs, ", size =", n_bytes
	return line
		

tests = (
	"add", "mul", "div", ("add", "mul", "div"),
	"sqrt",
	"log", "exp",
	"pow",
	"sin", ("sin", "cos", "sqrt"),
	"asin",
	"atan", ("sin", "asin", "sqrt"),
)

fsize = open ("size.data", "w", False)

lib_names = (lib for (lib, x, y) in libs)
header = "'fun' '" + string.join (lib_names, "' '") + "'"

fsize.write (header + "\n")

for t in tests:
	line = get_size_line (t)
	fsize.write (line + "\n")
	print ">>>", line

all = set()
for t in tests:
	if type (t) in (str, string):
		all.add (t)
	else:
		all |= set(t)

line = get_size_line (all, "all")
fsize.write (line + "\n")
print ">>>", line

fsize.close()

#exit (6)

#get_size_f64 (["", "f_sin"], "atmega4808")
