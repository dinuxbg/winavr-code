#include "libf7.h"
#include "acc.h"

#include <avr/pgmspace.h>

#define DD(str, X)              \
    do {                        \
        if (LL) { LOG_PSTR (PSTR (str));  LOG_PUSH_OFF; f7_dump (X); LOG_POP; } \
    } while (0)

extern "C" {
#if __has_include ("avr_f64.h")
#include "avr_f64.h"
#define HAVE_AVR_F64
#endif
}

#if __has_include ("../fp64lib-1.0.5/fp64lib.h")
#include "../fp64lib-1.0.5/fp64lib.h"
#define HAVE_FP64LIB
#endif

bool ja;

//extern void f7_atan (f7_t*, const f7_t*) __asm ("xxf7_atan");

/*
__attribute__((noinline, noclone))
void check_cmp (const f7_t *a, const f7_t *b, int8_t r)
{
    if (f7_cmp_unordered (a, b, 1) != r)
        __builtin_abort();

    if (f7_cmp_unordered (b, a, 1) != (int8_t) -r)
        __builtin_abort();
}

void test_cmp (void)
{
    int8_t z = INT8_MIN;
    int8_t m = -1;

    static f7_t pinf, ninf, nan, zero, one, two, mtwo;
    f7_set_inf (&pinf, 0);
    f7_set_inf (&ninf, 1);
    f7_set_nan (&nan);
    f7_set_s16 (&zero, 0);
    f7_set_s16 (&one, 1);
    f7_set_s16 (&two, 2);
    f7_set_s16 (&mtwo, -2);
    
    check_cmp (&nan, &nan,  z);
    check_cmp (&nan, &pinf, z);
    check_cmp (&nan, &ninf, z);
    check_cmp (&nan, &zero, z);
    check_cmp (&nan, &one, z);
    check_cmp (&nan, &two, z);
    check_cmp (&nan, &mtwo, z);

    check_cmp (&pinf, &pinf, 0);
    check_cmp (&pinf, &ninf, 1);
    check_cmp (&pinf, &zero, 1);
    check_cmp (&pinf, &one, 1);
    check_cmp (&pinf, &two, 1);
    check_cmp (&pinf, &mtwo, 1);

    check_cmp (&ninf, &ninf, 0);
    check_cmp (&ninf, &zero, m);
    check_cmp (&ninf, &one, m);
    check_cmp (&ninf, &two, m);
    check_cmp (&ninf, &mtwo, m);

    check_cmp (&zero, &zero, 0);
    check_cmp (&zero, &one, m);
    check_cmp (&zero, &two, m);
    check_cmp (&zero, &mtwo, 1);

    check_cmp (&one, &one, 0);
    check_cmp (&one, &two, m);
    check_cmp (&one, &mtwo, 1);

    check_cmp (&two, &two, 0);
    check_cmp (&two, &mtwo, 1);

    check_cmp (&mtwo, &mtwo, 0);
}


void check_add_flags (const f7_t *aa, const f7_t *bb, uint8_t on_add, uint8_t on_sub)
{
    f7_t cc;
    f7_add (&cc, aa, bb);
    uint8_t c_class = f7_classify (&cc);

    if ((c_class & on_add) != on_add)
        __builtin_abort();

    if (c_class & (uint8_t) ~on_add)
        __builtin_abort();

    f7_sub (&cc, aa, bb);

    c_class = f7_classify (&cc);

    if ((c_class & on_sub) != on_sub)
        __builtin_abort();

    if (c_class & (uint8_t) ~on_sub)
        __builtin_abort();
}

void test_addsub_flags (void)
{
    f7_t pinf, ninf, nan, zero, one, two;
    f7_set_inf (&pinf, 0);
    f7_set_inf (&ninf, 1);
    f7_set_nan (&nan);
    f7_set_s16 (&zero, 0);
    f7_set_s16 (&one, 1);
    f7_set_s16 (&two, 2);
    check_add_flags (&nan, &nan,  F7_FLAG_nan, F7_FLAG_nan);
    check_add_flags (&pinf, &nan, F7_FLAG_nan, F7_FLAG_nan);
    check_add_flags (&ninf, &nan, F7_FLAG_nan, F7_FLAG_nan);
    check_add_flags (&nan, &pinf, F7_FLAG_nan, F7_FLAG_nan);
    check_add_flags (&nan, &ninf, F7_FLAG_nan, F7_FLAG_nan);

    check_add_flags (&pinf, &ninf, F7_FLAG_nan,                F7_FLAG_inf);
    check_add_flags (&ninf, &pinf, F7_FLAG_nan,                F7_FLAG_inf | F7_FLAG_sign);
    check_add_flags (&pinf, &pinf, F7_FLAG_inf,                F7_FLAG_nan);
    check_add_flags (&ninf, &ninf, F7_FLAG_inf | F7_FLAG_sign, F7_FLAG_nan);
}

void test_eps (void)
{
    //f7 x = f7::value::eps;
    f7 x;
    
    dump ("eps = ", x);
    dump ("1 - eps = ", 1-x);
    dump ("eps * eps = ", x*x);
    dump ("sqrt (eps) = ", sqrt(x));
    dump ("sqrt(1 + 12 * (eps-1)", sqrt(1 + 12*(x-1)));
    f7 y = 1 + 1*(x-1);
    y <<= 53;
    dump (y = 1.0/3);
    dump (y - (f7) 1 /3);
    dump ( - (f7) 1 /3);
    dump ("floor (-y) = ", floor (-y));
    dump ("round (-y) = ", round (-y));
    dump ("ceil (-y) = ", ceil (-y));
}

void test_exp (void)
{
    f7_t xx, ff, exx, enxx, nn;
    
    f7_set_float (&ff, 1.23);
    f7_set_float (&xx, 0.001199);

    for (int i = 0; i < 1; i++)
    {
        DD ("\n\n xx = ", &xx);
        f7_exp (&exx, &xx);
        //LOG_SET (200);
        f7_exp (&enxx, f7_neg (&nn, &xx));

        f7_mul (&nn, &exx, &enxx);

        DD ("exp(x)  = ", &exx);
        DD ("exp(-x) = ", &enxx);
        DD ("e+ * e- = ", &nn);

        f7_mul (&xx, &xx, &ff);
    }
}
*/

void perf_func_f7 (int id, const char *label,
                   void (*f) (f7_t*, const f7_t*), const f7_t *xx)
{
    f7_t yy;
    PERF_PLABEL (id, label);
    PERF_START (id);
    f (&yy, xx);
    PERF_STOP (id);
}

void print_ticks_line (const char *nmspc, const char *func, uint16_t id,
                       uint32_t ticks, int16_t expo, float xtra1, float xtra2)
{
    LOG_PFMT_STR (PSTR ("%s "), nmspc);
    LOG_STR (func);
    LOG_U16 (id);
    LOG_U32 (ticks);
    LOG_S16 (expo);
    LOG_FLOAT (xtra1);
    LOG_FLOAT (xtra2);
    LOG_STR ("\n");
}

void f7_rand (f7_t *rr, int expo_min, int expo_max)
{
    uint32_t x = avrtest_prand();

    f7_clr(rr);
    ((uint32_t*) rr)[0] = avrtest_prand();
    ((uint32_t*) rr)[1] = avrtest_prand();
    rr->flags = 0;

    rr->mant[F7_MANT_BYTES - 1] |= 0x80;

    uint16_t dex = expo_max - expo_min;
    if (dex == 0)
    {
        rr->expo = expo_min;
        return;
    }

    uint8_t lz = __builtin_clz (dex);
//    LOG_FMT_U8 ("lz = %d\n", lz);
    uint16_t mask = (2u << (15 - lz)) - 1;

    uint16_t r;
    do {
        r = avrtest_prand() & mask;
    } while (r > dex);

    rr->expo = expo_min + r;
    
  //  f7_dump (rr);
}

void test_atan()
{
/*
    f7_div1 (&xx, f7_set_s16 (&xx, 5));

    f7_atan (&xx, &xx);
    f7_ldexp (&xx, &xx, 4);

    f7_div1 (&nn, f7_set_s16 (&nn, 239));
    f7_atan (&nn, &nn);
    f7_ldexp (&nn, &nn, 2);
    f7_sub (&nn, &xx, &nn);

    f7_put_CDEF ("pi", &nn, stdout);
    f7_dump (&nn);
    
    f7_set_u64 (&nn, 0x3243F6A8885A308D);
    f7_ldexp (&nn, &nn, -15*4);
    f7_put_CDEF ("pi", &nn, stdout);
    f7_dump (&nn);
exit(0);
*/

/*    
    f7_ldexp (cc, cc, 4);

    f7_div1 (&zz, f7_set_s16 (&zz, 239));

    f7_sub (cc, cc, &zz);

    
    f7_atan (&xx, f7_set_s16 (&nn, 1));
    f7_put_CDEF ("pi_2", &nn, stdout);

    for (int i = 0; i < 40; i++)
    {
        static char s[10];
        int n = 2*i+1;
        sprintf (s, "%s1_%d", i&1 ? "m" : "p", n);
        f7_set_s16 (&xx, n);
        xx.flags = i&1;
        f7_div1 (&xx, &xx);
        f7_put_CDEF (s, &xx, stdout);
    }
    exit(0);
*/
}


#define MK_WRAP64_1(F)                          \
    __attribute__((__unused__))                 \
    static float64_t wrap_ ## F (float64_t x)   \
    {                                           \
        return F (x);                           \
    }
MK_WRAP64_1 (f_arctan);
MK_WRAP64_1 (f_arcsin);
MK_WRAP64_1 (f_arccos);
MK_WRAP64_1 (f_tan);
MK_WRAP64_1 (f_sin);
MK_WRAP64_1 (f_cos);

void f7_dummy (f7_t *cc, const f7_t *aa)
{
    f7_copy (cc, aa);
}

uint64_t w_dummy (uint64_t x)
{
    return x;
}

//#define w_atan w_dummy
//#define f7_atan f7_dummy
#define f_atan wrap_f_arctan
#define f_asin wrap_f_arcsin
#define f_acos wrap_f_arccos

#undef f_sin
#define f_sin wrap_f_sin

#undef f_cos
#define f_cos wrap_f_cos

#undef f_tan
#define f_tan wrap_f_tan

//#define zatan atan


#define MK_1WRAP(N)                     \
extern "C" uint64_t w_##N (uint64_t);   \
__attribute__((noinline,noclone))       \
uint64_t w_##N (uint64_t x)             \
{                                       \
    f7_t xx;                            \
    f7_set_double (&xx, x);             \
    f7_##N (&xx, &xx);                  \
    return f7_get_double (&xx);         \
}
MK_1WRAP (sqrt)
MK_1WRAP (exp)
MK_1WRAP (sin)
MK_1WRAP (cos)
MK_1WRAP (tan)
MK_1WRAP (atan)
MK_1WRAP (asin)
MK_1WRAP (acos)

extern "C" uint64_t w_log (uint64_t);
__attribute__((noinline,noclone))
uint64_t w_log (uint64_t x)             
{
    f7_t xx;
    
    if (ja)
    {
        __builtin_memcpy (&xx.mant, &x, 8);
        if (ja) DD ("wrap: rawmant = ", &xx);
    }
    
    f7_set_double (&xx, x);
    if (ja) DD ("wrap: xx = ", &xx);
    f7_log (&xx, &xx);
    if (ja) DD ("wrap: zz = ", &xx);
    return f7_get_double (&xx);
}

#define MK_2WRAP(N)                             \
extern "C" uint64_t w_##N (uint64_t, uint64_t); \
__attribute__((noinline,noclone))               \
uint64_t w_##N (uint64_t x, uint64_t y)         \
{                                               \
    f7_t xx, yy;                                \
    f7_set_double (&xx, x);                     \
    f7_set_double (&yy, y);                     \
    f7_##N (&xx, &xx, &yy);                     \
    return f7_get_double (&xx);                 \
}
MK_2WRAP (add)
MK_2WRAP (sub)
MK_2WRAP (mul)
MK_2WRAP (div)

enum
{
    F_FLOAT,
    F_F7,
    F_WRAP7,
    F_U64,
    F_FP64
} libid_t;

typedef struct
{
    uint8_t flags; // like f7_t
    uint64_t mant;
    int expo;
    uint8_t dig_mant;
} float_t;

typedef struct
{
    float x;
    uint64_t x64, w64, fp64;
    f7_t xx;
} x_t;

typedef struct
{
    uint8_t dig_exp, dig_mant;
    int max_exp, exp_bias;
} layout_t;

typedef struct
{
    uint8_t id;
    const char* name;
    const layout_t *layout;
} lib_t;

enum
{
    ID_none,
    ID_add, ID_sub, ID_mul, ID_div,
    ID_exp, ID_log, ID_pow,
    ID_sin,  ID_cos,  ID_tan,
    ID_asin, ID_acos, ID_atan,
    ID_sqrt, ID_square,
    ID_count
};

typedef struct
{
    const lib_t *lib;
    const char *name;
    uint8_t id;
    union
    {
        float (*f_float)(float);
        void (*f_f7)(f7_t*, const f7_t*);
        uint64_t (*f_u64)(uint64_t);
        uint64_t (*f_wrap7)(uint64_t);
        uint64_t (*f_fp64)(uint64_t);

        float (*f_2float)(float, float);
        void (*f_2f7)(f7_t*, const f7_t*, const f7_t*);
        uint64_t (*f_2u64)(uint64_t, uint64_t);
        uint64_t (*f_2wrap7)(uint64_t, uint64_t);
        uint64_t (*f_2fp64)(uint64_t, uint64_t);
    };
} f_t;

typedef struct
{
    const lib_t *lib[10];
    size_t n_lib;
    const f_t *f[10];
    const f_t *inv[10];
    size_t n_f;
    uint8_t n_args;
} fun_t;

#define FLT_DIG_EXP   8
#define FLT_DIG_MANT  (31 - FLT_DIG_EXP)
#define FLT_MAX_EXP   ((1 << FLT_DIG_EXP) - 1)
#define FLT_EXP_BIAS  (FLT_MAX_EXP >> 1)
static const layout_t lay_float = { FLT_DIG_EXP, FLT_DIG_MANT, FLT_MAX_EXP, FLT_EXP_BIAS };

#define DBL_DIG_EXP   11
#define DBL_DIG_MANT  (63 - DBL_DIG_EXP)
#define DBL_MAX_EXP   ((1 << DBL_DIG_EXP) - 1)
#define DBL_EXP_BIAS  (DBL_MAX_EXP >> 1)
static const layout_t lay_double = { DBL_DIG_EXP, DBL_DIG_MANT, DBL_MAX_EXP, DBL_EXP_BIAS };

static const layout_t lay_f7t = { 16, 56, INT16_MAX, 0 };

void out_table_header_and_cookies (const fun_t *fs, const char *libname_gain_ref)
{
    LOG_PSTR (PSTR ("'lib' 'fun' 'id' 'ticks' 'acc' 'x' 'fx'\n"));

    LOG_PSTR (PSTR ("# Cookie[-1]:  specific order of libs / namespaces.\n\"c("));
    const lib_t* const *lib = fs->lib;
    for (uint8_t i = 0; i < fs->n_lib; lib++)
    {
        if (*lib)
        {
            if (i++) avrtest_putchar (',');
            LOG_PFMT_STR (PSTR ("'%s'"), (*lib)->name);
        }
    }
    LOG_PSTR (PSTR (")\" 'dummy' -1 0 0 0 0\n"));

    LOG_PSTR (PSTR ("# Cookie[-2]:  specific order of functions.\n\"c("));
    for (uint8_t i = 0; i < fs->n_f; i++)
    {
        if (i) avrtest_putchar (',');
        LOG_PFMT_STR (PSTR ("'%s'"), fs->f[i]->name);
    }
    LOG_PSTR (PSTR (")\" 'dummy' -2 0 0 0 0\n"));

    if (libname_gain_ref)
        LOG_PFMT_STR (PSTR ("# Cookie[-3]:  reference for gains.\n"
                            "'%s' 'dummy' -3 0 0 0 0\n"), libname_gain_ref);
}


void out_result (const char *space, const char *fun, uint16_t id,
                 uint32_t ticks, float bits_acc, float x, float y)
{
    LOG_FMT_STR ("%-12s ", space);
    LOG_FMT_STR ("%-4s ", fun);
    LOG_U16 (id);
    LOG_FMT_U32 ("% 6u ", ticks);
    //if (expo == INT8_MIN) LOG_STR ("NA "); else LOG_S16 (expo);
    LOG_FMT_FLOAT ("%g ", bits_acc);
    LOG_FMT_FLOAT ("%g ", x);
    LOG_FMT_FLOAT ("%g", y);
    LOG_STR ("\n");
}

#include <assert.h>
#include <math.h>

static float accbits_f7_t (const f7_t *xx, const f7_t *zz, int8_t best = -F7_MANT_BITS - 1)
{
    // Assumption: If NaN then this is the desired result.
    uint8_t x_class = f7_classify (xx);
    uint8_t z_class = f7_classify (zz);
//    DD ("xx", xx);
//      DD ("zz", zz);
    
    if (f7_class_nan (x_class | z_class))
        return best;

    uint8_t n_inf = f7_class_inf (x_class) + f7_class_inf (z_class);
    
    if (n_inf == 2 && ! f7_class_sign (x_class ^ z_class))
        return best;

    if (n_inf)
        return INT8_MAX;

    f7_t dd;
    int x_expo = f7_is_zero (xx) ? INT16_MIN : xx->expo;
    int z_expo = f7_is_zero (zz) ? INT16_MIN : zz->expo;
    int expo = x_expo > z_expo ? x_expo : z_expo;
    f7_sub (&dd, xx, zz);

    float ld = fabsf (f7_get_float (&dd));
    float lx = fabsf (f7_get_float (xx));
    if (ld == 0 || lx == 0)
        return best;

    ld = log (ld) / M_LN2;
    lx = log (lx) / M_LN2;
//    printf ("# ld = %f, dd.expo = %d\n", ld - lx, dd.expo - expo);
    //DD ("dd", &dd);
    return ld - lx; //dd.expo - expo;
}

static float accbits_float (float x, float z)
{
    f7_t xx, zz;
    f7_set_float (&xx, x);
    f7_set_float (&zz, z);
    return accbits_f7_t (&xx, &zz, -24 - 1);
}

static float accbits_u64 (uint64_t x, uint64_t z)
{
    f7_t xx, zz;
    f7_set_double (&xx, x);
    f7_set_double (&zz, z);
    return accbits_f7_t (&xx, &zz, -53 - 1);
}

__attribute__((noinline,noclone))
void exec_fs (const fun_t *fs, uint8_t j, x_t *vz, const x_t *vx, const x_t *vy)
{
    static uint16_t group;
    uint32_t cyc[fs->n_lib];
    int expo = vx->xx.expo;
    x_t vtest;
    
    if (fs->n_args == 1) assert (vy == NULL);
    if (fs->n_args == 2) assert (vy != NULL);

    group++;
//ja = group == 150;
//if (group != 9) return;
//DD ("vx", &vx->xx);
//DD ("vy", &vy->xx);
//if (!ja) return;
//LOG_ON;
    const f_t * const f0   = fs->f[j];
    const f_t * const inv0 = fs->inv[j];
    
    assert (j < fs->n_f);

    for (uint8_t lib = 0; lib < fs->n_lib; lib++)
    {
        const f_t *f   = f0   + lib;
        const f_t *inv = inv0 + lib;
        const uint8_t lid = f->lib->id;
        
        switch (lid)
        {
            default:
                continue;
            
            case F_FLOAT:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->x = f->f_float (vx->x);
                if (fs->n_args == 2) vz->x = f->f_2float (vx->x, vy->x);

                cyc[lid] = avrtest_cycles();
                break;
            }

            case F_F7:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) f->f_f7 (&vz->xx, &vx->xx);
                if (fs->n_args == 2) f->f_2f7 (&vz->xx, &vx->xx, &vy->xx);
                cyc[lid] = avrtest_cycles();
if (ja)DD("vx->xx", & vx->xx);
if (ja)DD("vy->xx", & vy->xx);
if (ja)DD("vz->xx", & vz->xx);
                break;
            }

            case F_U64:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->x64 = f->f_u64 (vx->x64);
                if (fs->n_args == 2) vz->x64 = f->f_2u64 (vx->x64, vy->x64);
                cyc[lid] = avrtest_cycles();
                break;
            }

            case F_FP64:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->fp64 = f->f_fp64 (vx->x64);
                if (fs->n_args == 2) vz->fp64 = f->f_2fp64 (vx->x64, vy->x64);
                cyc[lid] = avrtest_cycles();
                break;
            }

            case F_WRAP7:
            {
                avrtest_reset_cycles();
                if (fs->n_args == 1) vz->w64 = f->f_wrap7 (vx->x64);
                if (fs->n_args == 2) vz->w64 = f->f_2wrap7 (vx->x64, vy->x64);
                cyc[lid] = avrtest_cycles();
if (ja)LOG_FMT_U64 ("vx->xx = %0x%lx\n", vx->w64);
if (ja)LOG_FMT_U64 ("vy->xx = %0x%lx\n", vy->w64);
if (ja)LOG_FMT_U64 ("vz->xx = %0x%lx\n", vz->w64);
                break;
            }
        } // switch

        float bits = INT8_MAX;
        if (fs->n_args == 1 && inv->f_wrap7 == f->f_wrap7)
            bits = -20;
        else if (fs->n_args == 1 && inv->f_wrap7 != f->f_wrap7)
        {
            if (F_FLOAT == lid)
                bits = accbits_float (vx->x, vtest.x = inv->f_float (vz->x));

            if (F_F7 == lid)
                bits = accbits_f7_t (&vx->xx, (inv->f_f7 (&vtest.xx, &vz->xx), &vtest.xx));

            if (F_U64 == lid)
                bits = accbits_u64 (vx->x64, vtest.x64 = inv->f_u64 (vz->x64));

            if (F_WRAP7 == lid)
                bits = accbits_u64 (vx->x64, vtest.w64 = inv->f_wrap7 (vz->w64));

            if (F_FP64 == lid)
                bits = accbits_u64 (vx->x64, vtest.fp64 = inv->f_fp64 (vz->fp64));
        }

        if (fs->n_args == 2)
        {
            if (F_FLOAT == lid)
                bits = accbits_float (vx->x, vtest.x = inv->f_2float (vz->x, vy->x));

            if (F_F7 == lid)
                bits = accbits_f7_t (&vx->xx, (inv->f_2f7 (&vtest.xx, &vz->xx, &vy->xx), &vtest.xx));

            if (F_U64 == lid)
                bits = accbits_u64 (vx->x64, vtest.x64 = inv->f_2u64 (vz->x64, vy->x64));

            if (F_WRAP7 == lid)
                bits = accbits_u64 (vx->x64, vtest.w64 = inv->f_2wrap7 (vz->w64, vy->x64));

            if (F_FP64 == lid)
                bits = accbits_u64 (vx->x64, vtest.fp64 = inv->f_2fp64 (vz->fp64, vy->x64));
        }

        out_result (fs->lib[lid]->name, fs->f[j]->name, group,
                    cyc[lid], bits, vx->x, vz->x);
    } // for
}

extern float zexp (float) __asm ("exp");
extern float zlog (float) __asm ("log");
extern float zsqrt (float) __asm ("sqrt");
extern float zatan (float) __asm ("atan");
extern float zasin (float) __asm ("asin");
extern float zacos (float) __asm ("acos");
extern float ztan (float) __asm ("tan");
extern float zsin (float) __asm ("sin");
extern float zcos (float) __asm ("cos");

extern uint64_t f_mul (uint64_t, uint64_t) __asm ("f_mult");

float zsquare (float x)
{
    return x * x;
}

uint64_t f_square (uint64_t x)
{
    return f_mul (x, x);
}

uint64_t w_square (uint64_t x)
{
    return w_mul (x, x);
}

#define ARRAY_SIZE(X) (sizeof(X) / sizeof (*(X)))

//extern "C" void f7_atan (f7_t *cc, const f7_t *aa);

const fun_t* get_fs (uint8_t n_args, const f_t *fx, const f_t *invx, uint8_t n_fx)
{
    static fun_t fs;

    memset (&fs, 0, sizeof (fs));

    fs.n_args = n_args;
    
    uint8_t bits = 0;
    for (uint8_t i = 0; i < n_fx; i++)
    {
        const f_t *f = & fx[i];
        uint8_t b = bits;
        bits |= 1 << f->lib->id;
        if (b != bits)
        {
            fs.lib[f->lib->id] = f->lib;
            fs.n_lib++;
        }
    }

    assert (fs.n_lib < ARRAY_SIZE (fs.lib));
    fs.n_f   = n_fx / fs.n_lib;

    for (uint8_t i = 0; i < fs.n_f; i++)
    {
        assert (i < ARRAY_SIZE (fs.f));
        fs.f[i]   = & fx  [i * fs.n_lib];
        fs.inv[i] = & invx[i * fs.n_lib];
    }

    return & fs;
}

static const lib_t LIB_float = { F_FLOAT,   "avr-libc",  &lay_float };
static const lib_t LIB_f7    = { F_F7,      "f7_t",      &lay_f7t };
static const lib_t LIB_wrap7 = { F_WRAP7,   "f7",        &lay_double };
static const lib_t LIB_u64   = { F_U64,     "avr_f64",   &lay_double };
static const lib_t LIB_fp64  = { F_FP64,    "fp64",      &lay_double };


const fun_t* get_fs_f1 ()
{
    #define DEFF(N)                                                      \
        { .lib = & LIB_float, .name=#N, .id=ID_##N, .f_float = z##N   }, \
        { .lib = & LIB_f7,    .name=#N, .id=ID_##N, .f_f7    = f7_##N }, \
        { .lib = & LIB_wrap7, .name=#N, .id=ID_##N, .f_wrap7 = w_##N  }, \
        { .lib = & LIB_u64,   .name=#N, .id=ID_##N, .f_u64   = f_##N  }, \
        { .lib = & LIB_fp64,  .name=#N, .id=ID_##N, .f_fp64  = fp64_##N },
    static const f_t f1[] = 
    {
        DEFF (sqrt)
        DEFF (log)
        DEFF (exp)
        DEFF (sin)
        DEFF (cos)
        DEFF (tan)
        DEFF (asin)
        DEFF (acos)
        DEFF (atan)
    };

    static const f_t inv1[] = 
    {
/*        DEFF (square)
        DEFF (exp)
        DEFF (log)*/
        DEFF (sin)
        DEFF (cos)
//        DEFF (tan)
    };
    #undef DEFF

    return get_fs (1, f1, inv1, ARRAY_SIZE (f1));
}

const fun_t* get_fs_f2 ()
{
    extern float zadd (float, float) __asm ("__addsf3");
    extern float zsub (float, float) __asm ("__subsf3");
    extern float zmul (float, float) __asm ("__mulsf3");
    extern float zdiv (float, float) __asm ("__divsf3");

    #define DEFF(N)                                          \
        { .lib = & LIB_float, .name=#N, .id=ID_##N, .f_2float = z##N },  \
        { .lib = & LIB_f7,    .name=#N, .id=ID_##N, .f_2f7    = f7_##N },\
        { .lib = & LIB_wrap7, .name=#N, .id=ID_##N, .f_2wrap7 = w_##N }, \
        { .lib = & LIB_u64,   .name=#N, .id=ID_##N, .f_2u64   = f_##N }, \
        { .lib = & LIB_fp64,  .name=#N, .id=ID_##N, .f_2fp64  = fp64_##N },
    static const f_t f2[] = 
    {
        DEFF (add)
        DEFF (mul)
        DEFF (div)
    };
    static const f_t inv2[] = 
    {
        DEFF (sub)
        DEFF (div)
        DEFF (mul)
    };
    #undef DEFF

    return get_fs (2, f2, inv2, ARRAY_SIZE (f2));
}

void test_fs_f1 (int n_tests)
{
    f7_t xx;
    x_t z, x;

    const fun_t *fs = get_fs_f1();

    out_table_header_and_cookies (fs, "f7");

    if (n_tests <= 0) n_tests = 20;

    for (int i = 0; i < n_tests; i++)
    {
        for (uint8_t j = 0; j < fs->n_f; j++)
        {
            uint8_t id = fs->f[j]->id;

            if (i == 0)
                f7_set_s16 (&x.xx, 0);
            else if (i == 1)
                f7_set_s16 (&x.xx, -1);
            else
            {
                f7_rand (& x.xx, -4, 5);
                x.xx.sign = 1 & avrtest_prand();
            }

            if (i > 1 && f7_signbit (&x.xx))
                if (id == ID_log || id == ID_sqrt) // f_f7 == f7_sqrt ||  f_f7 == f7_log)
                    f7_abs (&x.xx, &x.xx);

            if (i > 1 && id == ID_exp)
                {}

            if (id == ID_atan)
                if (x.xx.expo >= 2)
                    x.xx.expo &= 1;

            if (i > 1 && (id == ID_asin || id == ID_acos))
            {
                f7_t one;
                while (f7_cmp_abs (&x.xx, f7_set_u16 (&one, 1)) > 0)
                    x.xx.expo--;
            }

            x.x64 = f7_get_double (&x.xx);
            x.x = f7_get_float (&x.xx);

            exec_fs (fs, j, &z, &x, NULL);
        } // for f[]
    } // for test cases
}

void test_fs_f2 (int n_tests)
{
    f7_t xx, yy;
    x_t z, x, y;

    const fun_t *fs = get_fs_f2();

    out_table_header_and_cookies (fs, "f7");

    if (n_tests <= 0) n_tests = 20;

    for (int i = 0; i < n_tests; i++)
    {
        f7_rand (&xx, -30, 30);
        f7_rand (&yy, -30, 30);
        xx.sign = 1 & avrtest_prand();
        yy.sign = 1 & avrtest_prand();

        if (i == 0) { f7_set_s16 (&xx, 0); f7_set_s16 (&yy, 0); }
        if (i == 1) f7_set_s16 (&yy, 0);
        if (i == 2) f7_set_s16 (&yy, 0);

        x.xx = xx;
        x.x64 = f7_get_double (&xx);
        x.x = f7_get_float (&xx);

        y.xx = yy;
        y.x64 = f7_get_double (&yy);
        y.x = f7_get_float (&yy);

        for (uint8_t j = 0; j < fs->n_f; j++)
        {
            //void (*f_f7)(f7_t*, const f7_t*, const f7_t*) = fs->f[F_F7]->f_2f7;
            uint8_t id = fs->f[j]->id;

            const x_t *mx = &x;
            const x_t *my = &y;
            if (id == ID_add && y.xx.expo > x.xx.expo)
            {
                mx = &y;
                my = &x;
            }

            exec_fs (fs, j, &z, mx, my);
        } // for f[]
    } // for test cases
}


static void str_F64 (uint64_t x, char *str, bool);
static void str_F32 (float x, char *str, bool);
static void str_F7 (const f7_t *aa, char *str, bool);

void run_result (const lib_t *lib, const char *fname, int id, uint32_t cyc,
                 const x_t *x, const x_t *z)
{
    uint8_t c_style = true;
    static char str[40];
    const x_t *w[] = { x, z };
    printf ("%-9s %s %u % 6lu", lib->name, fname, id, cyc);
    
    for (uint8_t c_style = 0; c_style < 2; c_style++)
    {
        if (c_style == 1)
            LOG_STR (" #");
        for (uint8_t i = 0; i < 2; i++)
        {
            const x_t *v = w[i];
            if (lib->layout == &lay_f7t)
                str_F7 (&v->xx, str, c_style);
            else if (lib->layout == &lay_double)
                str_F64 (v->x64, str, c_style);
            else if (lib->layout == &lay_float)
                str_F32 (v->x, str, c_style);
            LOG_FMT_STR (" %s ", str);
        }
    }
    if (lib->layout == &lay_f7t)
        f7_put_CDEF ("x", & x->xx, stdout);
    LOG_STR ("\n");
}

__attribute__((noinline,noclone))
void run_fs (const fun_t *fs, uint8_t j, x_t *vz, const x_t *vx)
{
    static uint16_t group;
    uint32_t cyc;
    
    assert (fs->n_args == 1);

    group++;

//if(group != 19) return;
    const f_t * const f0 = fs->f[j];
    
    assert (j < fs->n_f);

    for (uint8_t lib = 0; lib < fs->n_lib; lib++)
    {
        const f_t *f = f0 + lib;
        const uint8_t lid = f->lib->id;
        
        switch (lid)
        {
            default:
                continue;
            
            case F_FLOAT:
            {
                avrtest_reset_cycles();
                vz->x = f->f_float (vx->x);
                cyc = avrtest_cycles();
                break;
            }

            case F_F7:
            {
                avrtest_reset_cycles();
                f->f_f7 (&vz->xx, &vx->xx);
                cyc = avrtest_cycles();
                break;
            }

            case F_U64:
            {
                avrtest_reset_cycles();
                vz->x64 = f->f_u64 (vx->x64);
                cyc = avrtest_cycles();
                break;
            }

            case F_FP64:
            {
                avrtest_reset_cycles();
                vz->x64 = f->f_fp64 (vx->x64);
                cyc = avrtest_cycles();
                break;
            }

            case F_WRAP7:
            {
                avrtest_reset_cycles();
                vz->x64 = f->f_wrap7 (vx->x64);
                cyc = avrtest_cycles();
                break;
            }
        } // switch

        run_result (f->lib, f->name, group, cyc, vx, vz);
    } // for
}

void run_fs_f1 (int n_tests)
{
    f7_t xx;
    x_t z, x;

    const fun_t *fs = get_fs_f1();

    //out_table_header_and_cookies (fs, "f7");

    if (n_tests < 2) n_tests = 2;

    for (int i = 0; i < n_tests; i++)
    {
        for (uint8_t j = 0; j < fs->n_f; j++)
        {
            LOG_STR("\n");
            uint8_t id = fs->f[j]->id;

            if (i == 0)
                f7_set_s16 (&x.xx, 0);
            else if (i == 1)
                f7_set_s16 (&x.xx, -1);
            else
            {
                f7_rand (& x.xx, -4, 5);
                x.xx.sign = 1 & avrtest_prand();
            }

            if (i > 1 && f7_signbit (&x.xx))
                if (id == ID_log || id == ID_sqrt) // f_f7 == f7_sqrt ||  f_f7 == f7_log)
                    f7_abs (&x.xx, &x.xx);

            if (i > 1 && id == ID_exp)
            {
                int8_t ex = i >> 2;
                if (ex <= 8)
                    f7_set_1pow2 (&x.xx, (i & 2) ? ex : -ex, i & 1);
            }

#define F7_CONST_DEF(NAME, FLAGS, M6, M5, M4, M3, M2, M1, M0, EXPO) \
    (f7_t) { .flags = FLAGS, .mant = { M0, M1, M2, M3, M4, M5, M6 }, .expo = EXPO }

            if (i > 1 && id == ID_log)
            {
                if (i == 2)
                    x.xx = F7_CONST_DEF (xx, 0, 0xb5,0x00,0x00,0x00,0x00,0x00,0x00, 0);
                if (i == 3)
                    x.xx = F7_CONST_DEF (xx, 0, 0xb4,0xff,0xff,0xff,0xff,0xff,0xff, 0);
                if (i == 4)
                    x.xx = F7_CONST_DEF (X, 0,	0xb5,0x04,0xf3,0x33,0xf9,0xde,0x65, 0);
            }

            if (id == ID_atan)
            {
                if (x.xx.expo >= 3)
                    x.xx.expo &= 1;
                if (i == 2)
                    x.xx = F7_CONST_DEF (big_atan, 0, 0x88,0x4e,0x46,0x93,0x3c,0x66,0x80, 2);
            }

            if (id == ID_sin || id == ID_cos)
            {
                if (i >= 2 && i <= 3)
                {
                    f7_const (&x.xx, pi);
                    x.xx.expo--;
                    x.xx.mant[0] = 0;
                    x.xx.mant[1] = 0;
                    x.xx.mant[2] = 0;
                    x.xx.mant[3] += (i & 1) ? 1 : -1;
                }
                else if (i >= 4 && i <= 6)
                {
                    f7_set_u16 (&x.xx, 355);
                    x.xx.expo -= i - 4;
                    x.xx.flags = i & 1;
                }
                else if (i == 7)
                    f7_set_u16 (&x.xx, 22);
                else if (i == 8)
                {
                    f7_const (&x.xx, pi);
                    x.xx.mant[0] = 0;
                    x.xx.mant[1] = 0;
                    x.xx.mant[2] = 0;
                    x.xx.mant[3] = 0;
                    x.xx.mant[4] ++;
                }
            }

            if (i > 1 && (id == ID_asin || id == ID_acos))
            {
                f7_t one;
                while (f7_cmp_abs (&x.xx, f7_set_u16 (&one, 1)) > 0)
                    x.xx.expo--;

                if (i >= 2 && i <= 3)
                {
                    f7_set_u16 (&x.xx, 255);
                    x.xx.expo -= 8;
                    x.xx.sign = i & 1;
                }
                else if (i >= 4 && i <= 5)
                    f7_set_1pow2 (&x.xx, -1, i & 1);
                else if (i == 6)
                    x.xx = F7_CONST_DEF (big_asin_fp64, 0, 0xdd,0x7e,0xe6,0xf4,0x15,0x28,0xe8, -1);
                else if (i == 7)
                    x.xx = F7_CONST_DEF (big_acos_fp64, 0, 0x85,0xc0,0x22,0x0e,0x2f,0xda,0xd8, -1);
            }
#undef F7_CONST_DEF

            x.x64 = f7_get_double (&x.xx);
            x.x = f7_get_float (&x.xx);

            run_fs (fs, j, &z, &x);
        } // for f[]
    } // for test cases
}

//F7_CONST_DEF (big_acos,	0,	0x85,0xc0,0x22,0x0e,0x2f,0xda,0xd8,	-1)
//F7_CONST_DEF (big_asin,	0,	0xdd,0x7e,0xe6,0xf4,0x15,0x28,0xe8,	-1)
//F7_CONST_DEF (big_atan,	0,	0x88,0x4e,0x46,0x93,0x3c,0x66,0x80,	2)

void hunt_delta (uint16_t n)
{
    x_t x, y;

    f7_t d_func, x_func, d;
    f7_clr (&d_func);
    
    do {
        f7_rand (&x.xx, -5, 7);
        x.xx.mant[0] &= 0xf8;
        x.x64 = f7_get_double (&x.xx);
        f7_atan (&y.xx, &x.xx);
        y.x64 = f_atan (x.x64);
        f7_set_double (&d, y.x64);
        f7_Isub (&d, &y.xx);
        d.sign = 0;
        
        if (f7_gt (&d, &d_func))
        {
            f7_copy (&d_func, &d);
            f7_copy (&x_func, &x.xx);
            LOG_FMT_FLOAT ("BIG func D=%g:", f7_get_float (&d));
            f7_dump (&x.xx);
            f7_put_CDEF ("big_atan", &x.xx, stdout);
        }
        
    } while (--n);
}

void run ()
{
/*
    f7_double x = 0.5;
    f7_double y = 0.1;
    printf ("x = %f\n", (double) asin (x+y));
*/
    run_fs_f1 (10);
}

void test ()
{
    f7_t xx, ff, exx, enxx, nn;
    //LOG_PSTR (PSTR ("namespace func id ticks expo extra1 extra2 \n"));
    test_fs_f1 (30);

exit (0);

    f7_set_float (&xx, 1.23);
    //f7_set_float (&xx, 0.001199);

    avrtest_reset_all();
    PERF_START (1);
    f7_exp (&ff, &xx);
    PERF_STOP (1);
    uint32_t ticks = avrtest_cycles();
    LOG_FMT_U32 ("Cycles = %u\n", ticks);
    print_ticks_line ("libf7", "fun", 1, ticks, xx.expo, 1.23, 0);
    print_ticks_line ("libf7", "fun", 2, ticks- 1000, xx.expo, 1.23, 0);
    print_ticks_line ("libf7", "fun", 3, ticks- 3000, xx.expo, 1.23, 0);
//    PERF_DUMP_ALL;
}

static char hexdigit (uint8_t x)
{
    return x < 10 ? '0' + x : x - 10 + 'a';
}

char* toHex (uint64_t x, char *str)
{
    *str++ = '0';
    *str++ = 'x';
    char *s = str;
    do
    {
        *str++ = hexdigit (x & 0xf);
        x >>= 4;
    } while (x);
    *str = '\0';
    strrev (s);
    return str;
}


static inline __attribute__((always_inline))
void get_floating (float_t*, uint64_t, const layout_t*);
static void get_floating_F7 (float_t*, const f7_t*);
static void str_floating (float_t*, char*, bool);

void str_F64 (uint64_t x, char *str, bool c_style)
{
    float_t f;
    get_floating (&f, x, &lay_double);
    str_floating (&f, str, c_style);
}

void str_F32 (float x, char *str, bool c_style)
{
    float_t f;
    uint32_t u;
    __builtin_memcpy (&u, &x, 4);
    get_floating (&f, u, &lay_float);
    str_floating (&f, str, c_style);
}

void str_F7 (const f7_t *aa, char *str, bool c_style)
{
    float_t f;
    get_floating_F7 (&f, aa);
    str_floating (&f, str, c_style);
}

void str_floating (float_t *f, char *str, bool c_style)
{
    if (c_style)
    {
        if (f7_class_nan (f->flags))
            strcpy (str, "NaN");
        else if (f7_class_inf (f->flags))
            strcpy (str, (F7_FLAG_sign & f->flags) ? "-Inf" : "+Inf");
        else
        {
            if (f7_class_sign (f->flags))
                *str++ = '-';
            str = toHex (f->mant, str);
            *str++ = 'p';
            itoa (f->expo, str, 10);
        }
        return;
    }

    *str++ = 'F';
    utoa (f->dig_mant, str, 10);
    str += strlen (str);
    *str++ = '(';
    if (f7_class_nan (f->flags))
        strcpy (str, "NaN");
    else if (f7_class_inf (f->flags))
        strcpy (str, (F7_FLAG_sign & f->flags) ? "mInf" : "pInf");
    else
    {
        *str++ = (1 & f->flags) + '0';
        *str++ = ',';
        str = toHex (f->mant, str);
        *str++ = ',';
        itoa (f->expo, str, 10);
    }
    strcat (str, ")");
}

void get_floating (float_t *f, uint64_t x, const layout_t *lay)
{
    f->dig_mant = 1 + lay->dig_mant;
    f->flags = 1 & (x >> (lay->dig_exp + lay->dig_mant));
    f->expo = x >> lay->dig_mant;
    f->expo &= lay->max_exp;
    f->mant = x & ((1ull << lay->dig_mant) - 1);
    if (f->expo == lay->max_exp)
    {
        f->flags = f->mant ? F7_FLAG_nan : (f->flags | F7_FLAG_inf);
        return;
    }
    else if (f->expo == 0)
    {
        if (f->mant == 0)
            return;
        // Sub-normal.
        f->expo++;
    }
    else
    {
        // Normal.
        f->mant |= 1ull << lay->dig_mant;
    }

    // Align so that the string representation allows for easy comparison
    // across different types (float, double, f7_t).
    if (lay->dig_mant == 52)
    {
        f->mant <<= 4;
        f->expo -= 4;
    }
    if (lay->dig_mant == 23)
    {
        f->mant <<= 33;
        f->expo -= 33;
    }
    f->expo -= lay->exp_bias + lay->dig_mant;
}

void get_floating_F7 (float_t *f, const f7_t *aa)
{
    __builtin_memset (f, 0, sizeof (float_t));
    f->dig_mant = F7_MANT_BITS;
    f->flags = f7_classify (aa);
    if (!f7_class_number (f->flags))
        return;
    if (f7_class_zero (f->flags))
    {
        f->flags &= F7_FLAG_sign;
        return;
    }
    f->expo = aa->expo;
    __builtin_memcpy (&f->mant, aa->mant, 8);
    f->mant &= 0xffffffffffffff;
    // Align so that the string representation allows for easy comparison
    // across different types (float, double, f7_t).
    {
        f->mant <<= 1;
        f->expo--;
    }
    f->expo -= F7_MANT_BITS - 1;
}

/*
void perf (void)
{
    uint64_t d64;
    char txt[100];
    f7_t a;
    for (int ex = 3; ex < 16; ex++)
    {
        f7_div1 (&a, f7_set_s16 (&a, ex));
        d64 = f7_get_double (&a);
        str_D64 (d64, txt);
        LOG_FMT_STR ("d64 = %s\n", txt);
        str_D32 (f7_get_float (&a), txt);
        LOG_FMT_STR ("d32 = %s\n", txt);
        str_F7 (&a, txt);
        LOG_FMT_STR ("f7  = %s\n", txt);
    }
}
*/

void perf_funcs_1 (void)
{
    f7_t xx, dd;
    
    f7_clr (&xx);
    f7_set_float (&dd, 1.7);
    
LL = 0;
DD ("perf_funcs_1 dd = ", &dd);

    for (int i = 0; i < 50; i++)
    {
        if (! f7_signbit (&xx))
        {
           perf_func_f7 (1, PSTR ("f7_sqrt"), f7_sqrt, &xx);
           perf_func_f7 (2, PSTR ("f7_log"), f7_log, &xx);
        }
        perf_func_f7 (3, PSTR ("f7_exp"), f7_exp, &xx);

        f7_add (&xx, &xx, &dd);
//LL=1;
DD ("perf_funcs_1 xx = ", &xx);
LL=0;
    }
    PERF_DUMP_ALL;
}


#include <stdio.h>
#include <math.h>

void show_artanh_bits_per_its (float x)
{
    if (x < 0) x = sqrt (2);
    float x1 = (x - 1);
    float x2 = x1 * x1 / x;
    x1 /= (x + 1);
    
    for (int n = 0; n  < 20; n++)
    {
        float x1_2n1 = pow (x1, 2*n+1);
        float Rn = x2 *  x1_2n1 / (4*n + 6);
        float addend = x1_2n1 / (n + .5);
        printf ("Rn(%d+1) = %e = %f bits, add = %f bits\n",
                n, Rn, log(Rn) / log(2), log (addend) / log(2));
    }
}

float ffak (uint8_t n)
{
    if (n < 2)
        return 1;
        
    float f = 2;

    for (uint8_t i = 3; i <= n; i++)
        f *= i;
    return f;
}

void show_exp_bits_per_its (float x)
{
    for (int n = 0; n  < 20; n++)
    {
        float fak = ffak (n);
        float xn = pow (x, n);
        float addend = xn / fak;
        float Rn = 2 * (xn * x) / (fak * (n+1));
        printf ("Rn(%d) = %e = %f bits, add = %f bits\n",
                n, Rn, log(Rn) / log(2), log (addend) / log(2));
    }
}

void xxtest()
{
    f7_double x = 1;
    f7_double y = 1.5;
    dump ("x = ", x);
    dump ("y = ", y);
    LOG_ON;
    if (y > 1.4f)
    {
    LOG_OFF;
        x = x + y;
    }
    LOG_OFF;
    dump ("x = ", x);
}
