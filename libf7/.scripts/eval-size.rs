# Core wrapping function for long strings.
wrapit <- function(x, len)
{ 
  sapply(x, function(y) paste (strwrap (y, len), collapse = "\n"), 
         USE.NAMES = FALSE)
}

# Call this function with a list or vector
wraplabels <- function(x, len)
{
    if (is.list(x))
    {
        lapply (x, wrapit, len)
    } else {
        wrapit (x, len)
    }
}

x <- read.table ("size.data", header=TRUE)
x

n_vals = length(x[,1])
n_cols = length(x[1,])

nlabels <- rep(x$fun, n_cols - 1)

wrapla <- nlabels # wraplabels (nlabels, 4)
wrapla


cols = gray.colors (n_vals, start=0.9, end=0.3, gamma=2.2)

# http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf

cols = c(#"azure1", "azure2", "azure3", "azure4"
        #"khaki1","khaki2","khaki3","khaki4"
        #,"chartreuse2","chartreuse3","chartreuse4"
        #,"bisque2","bisque3","bisque4"
        "#ccccff", "#aaaaff", "#8888ff", "#6666ff"
        , "#ffee88"
        , "#ffbbbb", "#ff8888", "#ff6666"
        , "#bbffaa", "#66ff66"
        , "#bb99ff", "#9966ff"
        , "#8044cc"
        , "white"
        #, "#60ff60", "#c0ffc0"
        #, "#ccccff", "#a0a0ff", "#bbbbff", "#7070ff"
        #, "#ffaaaa"
        )

png (file = "size.png", width=880, height=500, res=80)


barplot (height=as.matrix(x[,2:n_cols] / 1024)
        , beside=TRUE
        #, names.arg=wrapla
        #, xlab="f7_t                                                            avr_f64.c"
        , main=c("Code sizes in KiBytes",
                 "for otherwise empty programs (no startup code etc.)")
        #, sub="submain\nsubmain2"
        , col=cols[1:n_vals]
        , cex.names=1.2
        , ylab=c("Code size in KiB")
        #, las=2
        #, legend.text=x[,1]#c("legend", "legnd2")
        #, col=c("green", "red", "blue", "purple", "#ffff00")
)

for (pos in 0:12)
    abline (h=1*pos, lty=3)

legend (x=max(x[,2:n_cols]) / 1024, legend=x[,1]
        , col="black"#cols
        ,cex=1.07
        ,pt.bg=cols
        ,pt.cex=2.4
        ,pch=22 # or 15 for filled
)


#barplot (height = VADeaths)
#VADeaths

dev.off()