#include <stdio.h>
#include "libf7.h"

int main (void)
{
    f7_t x, s, c;
    f7_set_s16 (&x, 3.14 * 256);
    f7_ldexp (&x, &x, -8);

    f7_sin (&s, &x);
    f7_cos (&c, &x);
    f7_dump (&s);
    f7_dump (&c);
}
