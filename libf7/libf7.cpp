/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#include "libf7.h"

#if defined (F7MOD_wrappers_) || defined (IN_LIBF7_CLASS_H)

f7 f7::operator - () const
{
    f7 zz;
    f7_neg (& zz.mm, & mm);
    return zz;
}

f7 f7::dump () const
{
    f7_dump (&mm);
    return *this;
}

const f7& dump (const f7 &xx)
{
    xx.dump ();
    return xx;
}


#define F7_METH(OP, NAME)                       \
                                                \
    f7 f7::operator OP (const f7 &yy) const     \
    {                                           \
        f7 zz;                                  \
        f7_##NAME (& zz.mm, & mm, &yy.mm);      \
        return zz;                              \
    }                                           \
                                                \
    const f7& f7::operator OP##= (const f7 &yy) \
    {                                           \
        f7_##NAME (& mm, & mm, & yy.mm);        \
        return *this;                           \
    }                                           \
                                                \
    f7 f7::operator OP (int s16) const          \
    {                                           \
        f7 zz = s16;                            \
        return (*this) OP zz;                   \
    }                                           \
                                                \
    f7 operator OP (int s16, const f7 &yy)      \
    {                                           \
        f7 zz = s16;                            \
        f7_##NAME (& zz.mm, & zz.mm, &yy.mm);   \
        return zz;                              \
    }
    F7_METH (+, add)
    F7_METH (-, sub)
    F7_METH (*, mul)
    F7_METH (/, div)
#undef F7_METH


#define F7_METH(OP, NAME)                       \
                                                \
    bool f7::operator OP (const f7 &yy) const   \
    {                                           \
        return f7_##NAME (& mm, & yy.mm);       \
    }                                           \
                                                \
    bool f7::operator OP (int s16) const        \
    {                                           \
        f7 zz = s16;                            \
        return f7_##NAME (& mm, & zz.mm);       \
    }                                           \
                                                \
    bool operator OP (int s16, const f7 &yy)    \
    {                                           \
        f7 zz = s16;                            \
        return f7_##NAME (& zz.mm, &yy.mm);     \
    }
    F7_METH (> , gt)
    F7_METH (>=, ge)
    F7_METH (< , lt)
    F7_METH (<=, le)
    F7_METH (==, eq)
    F7_METH (!=, ne)
#undef F7_METH


#define F7_METH(FUN)                \
    f7 f7::FUN () const             \
    {                               \
        f7 zz;                      \
        f7_##FUN (& zz.mm, & mm);   \
        return zz;                  \
    }                               \
                                    \
    f7 FUN (const f7 &xx)           \
    {                               \
        return xx.FUN();            \
    }
    F7_METH (abs)
    F7_METH (exp)
    F7_METH (log)
    F7_METH (log10)
    F7_METH (log2)
    F7_METH (sqrt)
    F7_METH (ceil)
    F7_METH (floor)
    F7_METH (round)
    F7_METH (trunc)
    F7_METH (cotan)
    F7_METH (sin)
    F7_METH (cos)
    F7_METH (tan)
    F7_METH (asin)
    F7_METH (acos)
    F7_METH (atan)
    F7_METH (pow10)
    F7_METH (exp10)
#undef F7_METH

f7 f7::min (const f7 &ex) const
{
    f7 zz;
    f7_fmin (& zz.mm, & mm, & ex.mm);
    return zz;
}

f7 f7::max (const f7 &ex) const
{
    f7 zz;
    f7_fmax (& zz.mm, & mm, & ex.mm);
    return zz;
}

f7 f7::pow (const f7 &ex) const
{
    f7 zz;
    f7_pow (& zz.mm, & mm, & ex.mm);
    return zz;
}

f7 f7::atan2 (const f7 &ex) const
{
    f7 zz;
    f7_atan2 (& zz.mm, & mm, & ex.mm);
    return zz;
}

f7 f7::powi (int ii) const
{
    f7 zz;
    f7_powi (& zz.mm, & mm, ii);
    return zz;
}

f7 min (const f7 &bb, const f7 &ex)
{
    return bb.min (ex);
}

f7 max (const f7 &bb, const f7 &ex)
{
    return bb.max (ex);
}

f7 pow (const f7 &bb, const f7 &ex)
{
    return bb.pow (ex);
}

f7 atan2 (const f7 &bb, const f7 &ex)
{
    return bb.atan2 (ex);
}

f7 powi (const f7 &bb, int ii)
{
    return bb.powi (ii);
}

const f7& f7::operator <<= (int s)
{
    f7_ldexp (& mm, & mm, s);
    return *this;
}

const f7& f7::operator >>= (int s)
{
    f7_ldexp (& mm, & mm, -s);
    return *this;
}

f7 f7::operator << (int s) const
{
    f7 zz;
    f7_ldexp (& zz.mm, & mm, s);
    return zz;
}

f7 f7::operator >> (int s) const
{
    f7 zz;
    f7_ldexp (& zz.mm, & mm, -s);
    return zz;
}

#endif // F7MOD_wrappers_

#if !defined (IN_LIBF7_H)

#ifdef F7MOD_cxx_misc_
const f7& dump (const char *text, const f7 &xx)
{
#ifdef AVRTEST_H
    LOG_STR (text);
#else
    (void) text;
#endif
    xx.dump ();
    return xx;
}

const f7& dump_P (const char *text, const f7 &xx)
{
#ifdef AVRTEST_H
    LOG_PSTR (text);
#else
    (void) text;
#endif
    xx.dump ();
    return xx;
}

f7 absdiff (const f7 &x, const f7 &y)
{
    return abs (x - y);
}

#define MK_BINARY(T, OP)                                    \
    T f7_double::operator OP (const f7_double &yy) const    \
    {                                                       \
        return (T) ((f7) this OP (f7) &yy);                 \
    }
    MK_BINARY (f7_double, +)
    MK_BINARY (f7_double, -)
    MK_BINARY (f7_double, *)
    MK_BINARY (f7_double, /)
    MK_BINARY (bool, >)
    MK_BINARY (bool, >=)
    MK_BINARY (bool, <)
    MK_BINARY (bool, <=)
    MK_BINARY (bool, ==)
    MK_BINARY (bool, !=)
#undef MK_BINARY

#endif // F7MOD_cxx_misc_

#endif // ! in libf7.h
