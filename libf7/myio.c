/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#include <stdio.h>
#include "uart.h"

// Provide output and input function of proper prototypes to
// feed in FDEV_SETUP_STREAM below.

static int
myio_putchar (char c, FILE *f)
{
    // We use this function to set up stdio et al.,
    // thus we do not need f.  The following line prevents the
    // compiler from complaining about the unused parameter f.
    (void) f;

    // uart_putchar is just a wrapper for the output function,
    // so here we go for the output of char c in module uart.c:
    uart_putc (c);

#ifdef HOST_WINDOWS
    // For MS-Windows terminal: add "carriage return" after "newline"
    if (c == '\n')
        uart_putc ('\r');
#endif /* HOST_WINDOWS */

    return 0;
}

// Wrap uart_getc into proper prototyped function, too
static int
myio_getchar (FILE *f)
{
    // f is not needed, so once again we give gcc its pacifier
    (void) f;

    return uart_getc();
}

// Build up the FILE object for our UART communication
static FILE myio_stream =
    FDEV_SETUP_STREAM (myio_putchar, myio_getchar, _FDEV_SETUP_RW);

// The following function will be called automatically before main.
static void __attribute__((constructor,used))
myio_setup_stdio (void)
{
    // Initialize UART. For UART mode (8N1) and baudrate see there.
    uart_init();

    // Let the standard streams point to our UART-stream objcet.
    stdout = &myio_stream;
    stdin  = &myio_stream;
    stderr = &myio_stream;
}
