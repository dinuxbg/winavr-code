#include <stdio.h>
#include "libf7.h"

int main()
{
    f7 x = 1.57;

    printf ("x      = %f\n", (double) x);
    f7 s = sin(x);
    printf ("sin(x) = %e\n", (double) s);

    f7 c = cos(x);
    printf ("cos(x) = %e\n", (double) c);

    printf ("sin^2(x) + cos^2(x) = %e\n", (double) (s*s + c*c));

    f7 _355 = 355;
    s = _355.sin();
    s.dump();
    printf ("sin(355) = %e\n", (double) s);
}
