XCC	= avr-gcc
AR	= avr-gcc-ar
RANLIB	= avr-gcc-ranlib

# The prefix for most of the libf7 functions.  Will be used to #define F7_
# in the auto-created f7-renames.h.

F7_PREFIX = f7_

.PHONY: all

all: s-libf7

override CFLAGS += -mmcu=$(MCU) -W -Wall -Winline -Wextra -std=gnu99 -fno-lto -Os \
		  -fdata-sections -fno-reorder-blocks -mrelax -dp -save-temps \
		  -fno-tree-loop-optimize -fno-tree-loop-im -fno-move-loop-invariants

CXXFLAGS += $(filter-out -std=gnu99, $(CFLAGS)) -std=gnu++17

$(warning "AVRTEST_HOME=$(AVRTEST_HOME)")
ifneq (,$(AVRTEST_HOME))

f7_cpp_cxx_misc.o f7_c_%.o f7_asm_%.o : \
	CFLAGS += \
		-DWITH_AVRTEST -I $(AVRTEST_HOME) -include avrtest.h \
		-fverbose-asm
endif

include libf7-common.mk

F7_ASM_PARTS += $(patsubst %, D_%,  $(F7_ASM_WRAPS))

F7F += dump dump_mant put_C put_CDEF put_hex2

F7_C_PARTS += dump put_C

F7_CPP_PARTS += f7_wrappers cxx_misc

F7_USE_CONST += const log exp atan asin sincos

F7_C_OBJECTS   = $(patsubst %, f7_c_%.o,   $(F7_C_PARTS))
F7_CPP_OBJECTS = $(patsubst %, f7_cpp_%.o, $(F7_CPP_PARTS))
F7_ASM_OBJECTS = $(patsubst %, f7_asm_%.o, $(F7_ASM_PARTS))
F7_USE_CONST_O = $(patsubst %, f7_c_%.o, $(F7_USE_CONST))

# Depends

$(F7_C_OBJECTS) $(F7_CPP_OBJECTS) $(F7_ASM_OBJECTS) : libf7.h asm-defs.h

$(F7_C_OBJECTS) $(F7_CPP_OBJECTS) $(F7_ASM_OBJECTS) : f7-renames.h

s-libf7-h: f7-renames.h
	echo "" > $@

$(F7_USE_CONST_O) : libf7-const.def libf7-array.def libf7-constdef.h

# Extra options

f7_c_log.o f7_c_addsub.o f7_c_floor.o f7_c_ldexp.o f7_c_exp.o : CFLAGS += -mstrict-X

$(F7_C_OBJECTS) $(F7_CPP_OBJECTS) : CFLAGS += -fno-ident

$(F7_CPP_OBJECTS) \
$(patsubst %, f7_c_%.o, $(CALL_PROLOGUES)) \
	: CFLAGS += -mcall-prologues

F7FLAGS   += $(CFLAGS) -ffunction-sections -save-temps=obj

F7XXFLAGS += $(filter-out -std=gnu99, $(F7FLAGS)) -std=gnu++17

F7F += dump dump_mant put_C put_CDEF put_hex2

f7-renames.h: f7renames.sh libf7-common.mk
	./$< head $(F7_PREFIX) libf7.mk   > $@
	./$< c    $(F7_PREFIX) $(F7F)     >> $@
	./$< cst  $(F7_PREFIX) $(F7F_cst) >> $@
	./$< asm  $(F7_PREFIX) $(F7F_asm) >> $@
	./$< tail $(F7_PREFIX)            >> $@

F7_ASM_WRAPS_g_ddd += $(g_ddd)
F7_ASM_WRAPS_g_xdd_cmp += $(g_xdd_cmp)
F7_ASM_WRAPS_g_xd  += $(g_xd)
F7_ASM_WRAPS_g_dx  += $(g_dx)
F7_ASM_WRAPS_m_ddd += $(m_ddd)
F7_ASM_WRAPS_m_ddx += $(m_ddx)
F7_ASM_WRAPS_m_dd  += $(m_dd)
F7_ASM_WRAPS_m_xd  += $(m_xd)

f7-wraps.h: f7wraps.sh libf7.mk libf7-common.mk
	./f7wraps.sh header " " > $@
	./f7wraps.sh ddd_libgcc     $(F7_ASM_WRAPS_g_ddd)     >> $@
	./f7wraps.sh xdd_libgcc_cmp $(F7_ASM_WRAPS_g_xdd_cmp) >> $@
	./f7wraps.sh dx_libgcc      $(F7_ASM_WRAPS_g_dx)      >> $@
	./f7wraps.sh xd_libgcc      $(F7_ASM_WRAPS_g_xd)      >> $@
	./f7wraps.sh ddd_math       $(F7_ASM_WRAPS_m_ddd)  >> $@
	./f7wraps.sh ddx_math       $(F7_ASM_WRAPS_m_ddx)  >> $@
	./f7wraps.sh dd_math        $(F7_ASM_WRAPS_m_dd)   >> $@
	./f7wraps.sh xd_math        $(F7_ASM_WRAPS_m_xd)   >> $@

F7_ASM_WRAPS += $(F7_ASM_WRAPS_g_ddd)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_g_xdd_cmp)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_g_xd)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_g_dx)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_m_ddd)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_m_ddx)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_m_dd)
F7_ASM_WRAPS += $(F7_ASM_WRAPS_m_xd)

F7_ASM_PARTS += $(patsubst %, D_%, $(F7_ASM_WRAPS))

f7_c_%.o : libf7.c
	$(XCC) libf7.c $(F7FLAGS) -c -o $@ -D F7MOD_$*_

f7_cpp_%.o : libf7.cpp libf7-class.h
	$(XCC) $< $(F7XXFLAGS) -c -o $@ -D F7MOD_$*_

f7_asm_%.o : libf7-asm.sx f7-wraps.h
	$(XCC) $< $(F7FLAGS) -c -o $@ -D F7MOD_$*_

s-libf7: \
		$(patsubst %, libf7.a(%), $(F7_CPP_OBJECTS)) \
		$(patsubst %, libf7.a(%), $(F7_C_OBJECTS))   \
		$(patsubst %, libf7.a(%), $(F7_ASM_OBJECTS))
	$(RANLIB) libf7.a
	echo "x" > $@

libf7.a(%.o): %.o
	$(AR) cr $@ $<


.PHONY: clean-f7 clean

clean-f7:
	rm -f $(wildcard libf7.a $(F7_ASM_OBJECTS) $(F7_C_OBJECTS))
	rm -f $(wildcard $(F7_CPP_OBJECTS))
	rm -f $(wildcard s-libf7 s-libf7-h f7-renames.h f7-wraps.h)

clean: clean-f7
	rm -f $(wildcard *.i *.ii *.s *.su *.mi *.res)
	rm -f $(wildcard *.o *.opp *.elf *.elf.ltrans* *.hex)
	rm -f $(wildcard *.lst *.lss *.out *.map)
