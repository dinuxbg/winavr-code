/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#include <avr/io.h>

#include "uart.h"

#if !defined (UCSRA)

#define UCSRA UCSR0A
#define UCSRB UCSR0B
#define UCSRC UCSR0C
#define UDR   UDR0
#define UDRIE UDRIE0
#define RXEN  RXEN0
#define TXEN  TXEN0
#define RXC   RXC0
#define TXC   TXC0
#define TXCIE TXCIE0
#define RXCIE RXCIE0
#define UCSZ1 UCSZ01
#define UCSZ0 UCSZ00
#define UBRRL UBRR0L
#define UBRRH UBRR0H
#define U2X   U2X0
#define UDRE  UDRE0
#endif

#if !defined (U2X_BIT)
#define U2X_BIT 1
#endif // U2X_BIT

#if !defined (BAUDRATE)
#error Define BAUDRATE
#endif // BAUDRATE

#if !defined (F_CPU)
#error Define F_CPU
#endif // F_CPU

void uart_init (void)
{
    unsigned short ubrr = -.6 + F_CPU/((16L-8*U2X_BIT) * BAUDRATE);

    UBRRH = ubrr >> 8;
    UBRRL = ubrr;

    UCSRA = (U2X_BIT << U2X) | (1 << TXC);

    // Enable UART Reciever, Transmitter
    // Data mode 8N1, asynchronous

    UCSRB = (1 << RXEN) | (1 << TXEN);

#if defined (URSEL)
    UCSRC = (1 << UCSZ1) | (1 << UCSZ0) | (1 << URSEL);
#else
    UCSRC = (1 << UCSZ1) | (1 << UCSZ0);
#endif

    // Flush input buffer: remove rubbish that might occur after
    // (re)configuering the UART hardware
    do
        UDR;
    while (UCSRA & (1 << RXC));
}

// Get one char from UART module (blocking version)
char uart_getc (void)
{
    while (!(UCSRA & (1 << RXC)))
        ;
    return (char) UDR;
}

// Get int (0..255) from UART module or -1 (non-blocking version)
int uart_getc_nowait (void)
{
    return (UCSRA & (1 << RXC)) ? UDR : -1;
}

/////////////////////////////////////////////////////////////////

// Write one char to UART (non-buffered, blocking version)
void uart_putc (const char c)
{
    while (!(UCSRA & (1 << UDRE)))
        ;
    UDR = c;
}
