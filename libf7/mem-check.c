/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#include <avr/io.h> // RAMEND
#include "mem-check.h"

// Mask to init SRAM and check against
#define MASK 0xaa

// __heap_start is defined in the linker script
extern uint8_t memcheck_heap_start[] __asm ("__heap_start");

uint16_t
get_mem_unused (void)
{
    uint16_t unused = -1u;

    // Get end of static allocated RAM space (.data, .bss, .noinit, ...)
    const uint8_t *p = memcheck_heap_start;

    while (1)
    {
        unused++;
        // Mask written in memcheck_init_mem still intact?
        if (*p++ != MASK)
            return unused;
    }
}

// Init RAM space after static allocated RAM with MASK.
// Write accesses to the stack will overwrite this mask, so
// we can probe later on and detect memory usage.
// !!! Do never call this function         !!!
// !!! The Linker knows what to do with it !!!
static void __attribute__ ((naked, used, unused, section (".init3")))
memcheck_init_mem (void);

void memcheck_init_mem (void)
{
    // We use inline assembly to be independent of optimization flags.
    // Moreover, according to GCC documentation, the only code that
    // can be safely used in naked functions is inline assembly.
        __asm__ __volatile (
            "ldi r30, lo8 (%[start])"      "\n\t"
            "ldi r31, hi8 (%[start])"      "\n\t"
            "ldi r24, lo8 (%[mask])"       "\n\t"
            "ldi r25, hi8 (%[end])"        "\n\t"
            "0:"                           "\n\t"
            "st  Z+,  r24"                 "\n\t"
            "cpi r30, lo8 (%[end])"        "\n\t"
            "cpc r31, r25"                 "\n\t"
            "brlo 0b"
        :
        : [mask]  "i" (MASK),
          [end]   "i" (RAMEND+1),
          [start] "i" (memcheck_heap_start)
        : "memory", "r24", "r25", "r30", "r31"
        );
}
