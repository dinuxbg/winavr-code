/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#ifndef UART_H
#define UART_H

extern void uart_init (void);
extern void uart_putc (const char);
extern char uart_getc (void);
extern int uart_getc_nowait(void);

extern unsigned char volatile uart_transmits_p;

#ifdef UART_OUTFIFO
#include "fifo.h"
extern fifo_t uart_outfifo;
#endif // UART_OUTFIFO

#ifdef UART_INFIFO
#include "fifo.h"
extern fifo_t uart_infifo;
#endif // UART_INFIFO


#endif /* UART_H */
