/*  Copyright (C) 2019 Free Software Foundation, Inc.

    This file is part of LIBF7.

    LIBF7 is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 3, or (at your option) any later
    version.

    LIBF7 is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with LIBF7; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.  */

#include <stdlib.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

ISR (TIMER0_OVF_vect)
{
    wdt_reset();
}

// The following function will be called automatically before main.

static void __attribute__((constructor,used))
init_system (void)
{
    (void) MCUSR;
    MCUSR = 0;

    // Langsamer Watchdog
    wdt_reset();
    wdt_enable (WDTO_2S);

    // Timer initialisieren zur Watchdog-Bedienung

    // Normal Mode und PRESCALE = 1024
    TCCR0B = (1 << CS00) | (1 << CS01);

    // Overflow-Interrupt f�r Timer 0
    TIMSK0 = (1 << TOIE0);

    // Normal Mode und PRESCALE = 1
    TCCR1B = (1 << CS10);

    sei();
}

#include <stdio.h>

// Nach return von main.

void exit (int x)
{
    (void) x;

    while (1)
        wdt_reset();
}

void abort (void)
{
    while (1)
        wdt_reset();
}
